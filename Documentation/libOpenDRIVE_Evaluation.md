# Evaluation of libOpenDRIVE

## 1. API Design & Usability

libOpenDRIVE has the following capabilities:
- Parse an OpenDRIVE 1.4 map into a simple data structure (mapping id (string) to objects)
- Fetch simple information about openDRIVE objects
- Calculate a route using weights (weight=length) and a simple algorithm.
- Convert s-t-(h) road coordinates to x-y-z
- Convert roads and other objects to 3d meshes
- Most, if not all Objects still contain their XML representation

The API is very straight forward and mainly based on the identifier mappings.

Not much documentation exists. It feels close enough to OpenDRIVE to be able to defer most information from the context,
though.

## 2. Overview of the Data Structure & Feature Set

As stated above most objects still allow access to the underlying XML node. Here I will only give an overview about the
additional convenience fields.

- The `OpenDriveMap` contains all `Road`s and `Junction`s
- The `Road` object contains basic attributes, links, etc.
- Same with `Junction`s, `LaneSection`s.

=> In a nutshell, the data structure mirrors the openDRIVE xml.

- Surfaces (CRG) are not implemented but can probably be accessed via `XmlNode`s.

## 3. Licenses

```
libOpenDRIVE: Apache 2.0
|- earcut: ISC License (similar to MIT)
|- pugixml: MIT License
```

## 4. Effort to Upgrade

Most of the parsing is done inside the `OpenDriveMap` constructor which can be difficult to maintain when upgrading to a
newer version of openDRIVE or adding new features like (CRG) surfaces.