import numpy as np
import matplotlib
matplotlib.use('Agg') # no output on screen
from scipy.special import fresnel
import matplotlib.pyplot as plt

hdg = 0.666
x_offset = -0.257
y_offset = -0.28

# Create full spiral
t = np.linspace(-3.0, 3.0, 401)
ss, cc = fresnel(t / np.sqrt(np.pi / 2))
scaled_ss = np.sqrt(np.pi / 2) * ss
scaled_cc = np.sqrt(np.pi / 2) * cc
x = np.cos(hdg) * scaled_cc - np.sin(hdg) * scaled_ss + x_offset
y = np.sin(hdg) * scaled_cc + np.cos(hdg) * scaled_ss + y_offset
plt.plot(x, y, 'k', linewidth=2)

# Create spiral part
t = np.linspace(0.5, 1.5, 201)
ss, cc = fresnel(t / np.sqrt(np.pi / 2))
scaled_ss = np.sqrt(np.pi / 2) * ss
scaled_cc = np.sqrt(np.pi / 2) * cc
x = np.cos(hdg) * scaled_cc - np.sin(hdg) * scaled_ss + x_offset
y = np.sin(hdg) * scaled_cc + np.cos(hdg) * scaled_ss + y_offset
plt.plot(x, y, 'r', linewidth=2)

# Annotations
plt.annotate("Spiral Origin", xy=(x_offset, y_offset), xytext=(-1.5, -0.1), arrowprops=dict(facecolor='black', shrink=0.05, width=1, headwidth=5))
plt.annotate("Road Geometry\nOrigin", xy=(x[0], y[0]), xytext=(0.75, 0), arrowprops=dict(facecolor='black', shrink=0.05, width=1, headwidth=5))

plt.xlim(-2, 2)
plt.ylim(-2, 2)

ax = plt.gca()
ax.set_aspect('equal', adjustable='box')
plt.grid(True)
plt.savefig("spiral_naming_conventions.png")
