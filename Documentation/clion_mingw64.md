# Development Environment Setup - CLion + MSYS2/MINGW64

This setup was tested with CLion 2022.3 (Build #CL-223.7571.171)

1. Install the CLion Bazel Plugin.
   - `File -> Settings -> Plugins -> Search for 'Bazel' -> Install`

2. Install Bazel using Bazelisk.
   - Download the newest release version for Windows on the [Github Release Page](https://github.com/bazelbuild/bazelisk/releases/).
   - Rename the downloaded file to `bazel.exe`.
   - Add the path to the `bazel.exe` to your user environment variables.

3. Install the GCC compiler using a MSYS2/MINGW64 environment.
   - Download and Install [MSYS2](https://www.msys2.org/).
   - Download MINGW64 from [Github Releases](https://github.com/niXman/mingw-builds-binaries/releases). It is recommended to use UCRT instead of MSVCRT, but both should work. [Relevant Stackoverflow Question](https://stackoverflow.com/questions/67848972/differences-between-msvcrt-ucrt-and-vcruntime-libraries)
   - Extract MINGW64 into the MSYS2 folder, i.e. `${MSYS_2_PATH}/mingw64`.
   - Add the binary folder into your PATH variable, i.e. `${MSYS_2_PATH}/mingw64/bin`
   - Create a new user environment variable `BAZEL_SH` that points to the MSYS2 bash, i.e. `BAZEL_SH=${MSYS_2_PATH}/usr/bin/bash.exe`

4. Test your bazel setup by building buildifier.
   - Clone the buildtools repository.
   - Navigate into the `buildtools/buildifier` directory.
   - Run `bazel build --config=rls //buildifier --compiler=mingw-gcc`.
   - If there are no errors, you can successfully use the GCC compiler provided by the MINGW64 developer tools.
   - The output should tell you where the `buildifier.exe` has been created.

5. Setup user settings for bazel.
   - Create a file in the repository root: `user.bazelrc`.
   - Add the following line to the file: `build --compiler=mingw-gcc`.
   - The file is ignored by git so it has no influence on the CI or on any other developers setup.

6. Add Buildifier to CLion Bazel settings.
   - `File -> Settings -> Bazel Settings -> Buildifier Binary Location -> Enter Path to Buildifier.exe`

7. Setup Intellisense
   - Run `bazel run @hedron_compile_commands//:refresh_all`. This should create a file called `compile_commands.json`
   - Right-Click on `compile_commands.json` and select `Load Compilation Database`. This might take some time.
   - CLion might report some compilation errors, however the auto navigation and autocompletion have so far worked very well.
   - When adding new files or new libraries, re-running the hedron compile commands are necessary, but CLion picks up on any changes in the Compilation Databse automatically.
