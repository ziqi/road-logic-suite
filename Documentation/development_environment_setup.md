# Development Environment Setup - General

## Setup Instructions

### Install Bazel

- Install Bazel: https://bazel.build/install
- Install Bazel Command-Line Completion : https://bazel.build/install/completion
- Bazel formatter: https://github.com/bazelbuild/buildtools/blob/master/buildifier/README.md

### Setup Instructions for CLion + MSYS2/MINGW64

[CLion+Mingw64](./clion_mingw64.md)

### Containerized Setup

The devcontainer for build and test can be found in `.devcontainer`, which contains all required dependencies.

See more [VScode-devcontainer](https://code.visualstudio.com/docs/devcontainers/containers)

### Build the Docker dev image

A docker dev environment is proved with the `devcontainer`: `.devcontainer/Dockerfile`

Build docker dev image：

```bash
docker build -f .devcontainer/Dockerfile  -t  road-logic-suite-dev .
```

### Setup Instructions for VSCode

Follow the instructions to [Install Bazel](https://bazel.build/install).


Clone the project from repo
```bash
git clone git@gitlab.eclipse.org:eclipse/openpass/road-logic-suite.git
```
Rename the main project folder if it is not done, there may be problems with debugging and open the project again
```bash
road-logic-suite -> road_logic_suite
```

Add Submodules
```bash
git submodule init
git submodule update
```

now you can open the Project in VSCode

Install Bazel Extensions:
- Install Bazel extension via extension marketplace
    - install bazel Buildifier with sudo

After this Steps you can in VSCode terminal build and Test the project with:

```bash
bazel build --config=rls //RoadLogicSuite/Tests/...
bazel test --config=rls //RoadLogicSuite/Tests/...
```

Configure clangd/intellisense (all .cpp files have red swiggles)

Clangd:
- install clangd Extension
- Run this command:
```bash
bazel run @hedron_compile_commands//:refresh_all -- --config=rls
```
- this command generated: compile_commands.json
- go to clangd extensions settings
- add to Arguments for clangd server the following commands (only one command to push the button Add Item):
```bash
--header-insertion=never
--compile-commands-dir=${workspaceFolder}/
--query-driver=**
```

 reload VSCode [(CMD/CTRL+SHIFT+P)->reload Window]
## Build Test and Code-Checks

### Build and test

- Build and test in native linux system

```bash
# in debug mode
bazel build --config=rls  //RoadLogicSuite/...
bazel test --config=rls  //RoadLogicSuite/...  --test_output=all
# in release
bazel build --config=rls_release  //RoadLogicSuite/...
bazel test  --config=rls_release  //RoadLogicSuite/...  --test_output=all
```

- Using dev docker container

```bash
docker run --rm -v $PWD:/road_logic_suite -t  -i road-logic-suite-dev /bin/sh -c "cd road_logic_suite && bazel test --config=rls //RoadLogicSuite/..."
#Use `-v` to mount Bazel cache to the host can avoid rebuild from scratch.
# docker run --rm -v $PWD:/road_logic_suite -v ~/bazel_cache:/tmp/bazel_output -t -i road-logic-suite-dev /bin/sh -c "cd road_logic_suite && bazel test --config=rls //RoadLogicSuite/..."
```

### Pre-commit check and fix

A pre-commit script is provided in `check_and_fix/check_and_fix.sh`. Please find the file for detailed checks.

- Run in the native linux system:

```bash
./utils/ci/scripts/check_and_fix/check_and_fix.sh
```

- Run using dev docker container

```bash
docker run --rm -v $PWD:/road_logic_suite  -t -i road-logic-suite-dev /bin/sh -c "cd road_logic_suite && bash utils/ci/scripts/check_and_fix/check_and_fix.sh"
```

Clang-tidy checks is not included in the script:
- Run in the native linux system:

```bash
 bazel build --config=clang-tidy //RoadLogicSuite/...
```
- Run using dev docker container

```bash
docker run --rm -v $PWD:/road_logic_suite -v ~/.cache/bazel:/root/.cache/bazel -t -i road-logic-suite-dev /bin/sh -c "cd road_logic_suite &&  bazel build  --config=clang-tidy //RoadLogicSuite/..."
```

### Code-coverage

The code-coverage report can be found on the
page [code-coverage](https://symmetrical-pancake-j88pvrz.pages.github.io/bazel-coverage/index.html). This page is
updated with each merge to the `main` branch.

Follow the following instruction to generate code-coverage report locally. Report can be found
in `bazel-coverage/index.html`:

- Run in the native linux system:

```bash
./utils/ci/scripts/check_and_fix/code_coverage.sh
```

- Run using dev docker container

```bash
docker run --rm -v $PWD:/road_logic_suite -t -i road-logic-suite-dev /bin/sh -c "cd road_logic_suite && utils/ci/scripts/check_and_fix/code_coverage.sh"
```

## Using the doxygen documentation

Generate doxygen documentation locally:
* run `$ doxygen Documentation/Doxyfile.in`
* a folder `Documentation/Doxygen/documentation` will be created, containing all files
* open `Documentation/Doxygen/documentation` with a web browser

Documentation can be found can be found on the page [Documentation](https://symmetrical-pancake-j88pvrz.pages.github.io/documentation/index.html)

## Release and deployment

The Dockerfile located in `utils/Dockerfile` defines a system environment which should be used to build the RoadLogicSuite locally and also in the CI.

- The Jenkins job is [here](https://ci.eclipse.org/openpass/job/RoadLogicSuite).
- The release pages is located [here](https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite/-/releases).

### Create Release using build pipeline

Tag the relevant git commit in the main branch

- In GitHub: Project Overview -> Code -> Tags -> New Tags
- Use a tag name according to the format: v1.0.0, v1.0.1, v1.1.0, v2.0.0

Build artifact

- Now, in the Jenkins the just created tag should appear in the `Tag` view
- Start building the tag manually (permission needed). Tag commits will not started automatically. This is not a misconfiguration, this behaviour is confirmed by the OpenPass admin.

Create release page in Gitlab (permission needed)

- In GitHub: Project Overview -> Deploy -> Releases -> New Release
- Select the just created tag
- Add release notes
- Source code as zip and tar will be attached automatically
- Attach link to build zip/tar in Jenkins
- Add link to user documentation
- Add link to developer documentation

### Build RLS locally

- Make sure that Docker is installed locally
- Execute utils/build_rls.sh
- The build will be stored in the `artifacts` folder
