load("@//ThirdParty/googletest:googletest.bzl", "googletest")
load("@//ThirdParty/libOpenDRIVE:libOpenDrive.bzl", "lib_open_drive")
load("@//ThirdParty/units:units.bzl", "units_nhh")
load("@//ThirdParty/eigen:eigen.bzl", "eigen")
load("@//ThirdParty/rtree:rtree.bzl", "rtree")
load("@//ThirdParty/bazel_clang_tidy:bazel_clang_tidy.bzl", "bazel_clang_tidy")
load("@//ThirdParty/mantle_api:mantle_api.bzl", "mantle_api")
load("@//ThirdParty/map_sdk:map_sdk.bzl", "map_sdk")

def third_party_deps():
    """
    Load thrid party libiraries
    """
    lib_open_drive()
    units_nhh()
    googletest()
    eigen()
    rtree()
    bazel_clang_tidy()
    mantle_api()
    map_sdk()
