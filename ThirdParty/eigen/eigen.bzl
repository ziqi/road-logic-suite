"""
This module contains rule to pull the eigen library
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

def eigen():
    maybe(
        http_archive,
        name = "eigen",
        build_file = "//ThirdParty/eigen:eigen.BUILD",
        url = "https://gitlab.com/libeigen/eigen/-/archive/3.4/eigen-3.4.tar.gz",
        strip_prefix = "eigen-3.4",
    )
