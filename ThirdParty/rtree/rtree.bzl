"""
This module contains rule to pull the RTree library
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")


def rtree():
    maybe(
        http_archive,
        name = "rtree",
        build_file = "//ThirdParty/rtree:rtree.BUILD",
        url = "https://github.com/nushoin/RTree/archive/4bd551c7e2b2cf3fce9d4bf3d18f4c58d1efefc1.zip",
        strip_prefix = "RTree-4bd551c7e2b2cf3fce9d4bf3d18f4c58d1efefc1",
    )
