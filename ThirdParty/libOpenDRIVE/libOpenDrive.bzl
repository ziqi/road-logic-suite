"""
This module contains rule to pull libOpenDRIVE
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "0.5.0"

def lib_open_drive():
    maybe(
        http_archive,
        name = "libOpenDRIVE",
        build_file = "//ThirdParty/libOpenDRIVE:libOpenDrive.BUILD",
        url = "https://github.com/pageldev/libOpenDRIVE/archive/refs/tags/{tag}.tar.gz".format(tag = _TAG),
        sha256 = "f38c664a487021574383e414ed8827639cc06535c15dfe58ada1cc23fcd33e99",
        strip_prefix = "libOpenDRIVE-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
