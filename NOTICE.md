# Notices for Eclipse openpass

This content is produced and maintained by the Eclipse openpass project.

 * Project home: https://projects.eclipse.org/projects/automotive.openpass

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite

## Third-party Content

### bazel
- License: [Apache-2.0](https://github.com/bazelbuild/bazel/blob/master/LICENSE)
- Repository: https://github.com/bazelbuild/bazel
- Version: 5.1.1

### bazel_rules
- License: [Apache-2.0](https://github.com/bazelbuild/rules_pkg/blob/main/LICENSE)
- Repository: https://github.com/bazelbuild/rules_pkg
- Version: 0.8.0

### bazel_skylib
- License: [Apache-2.0](https://github.com/bazelbuild/bazel-skylib/blob/main/LICENSE)
- Repository: https://github.com/bazelbuild/bazel-skylib
- Version: 1.3.0


### Hedron's Compile Commands Extractor for Bazel
- License: [Hedron Vision Incorporated ](https://github.com/hedronvision/bazel-compile-commands-extractor/blob/main/LICENSE.md)
- Repository: https://github.com/hedronvision/bazel-compile-commands-extractor
- Version: ed994039 (commit)


### googletest (Google Test)
- License: [BSD 3-Clause "New" or "Revised" License](https://github.com/google/googletest/blob/main/LICENSE)
- Repository: https://github.com/google/googletest
- Version: 1.10.0

### clang_tidy (LLMV)
- License: [University of Illinois/NCSA Open Source License](!https://releases.llvm.org/11.0.0/LICENSE.TXT)
- Homepage: https://releases.llvm.org/
- Version: 11


### mantle_api (MantleAPI)
- License: [Eclipse Public License 2.0](https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api/-/blob/master/LICENSE.txt)
- Repository: https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/tags/v2.0.0

### map_sdk
- License: [Eclipse Public License 2.0](https://gitlab.eclipse.org/eclipse/openpass/map-sdk/-/blob/main/LICENSE.txt)
- Repository: https://gitlab.eclipse.org/eclipse/openpass/map-sdk
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/map-sdk/-/tags/v0.3.0


### libOpenDRIVE
- License: [Apache-2.0 License](https://github.com/grepthat/libOpenDRIVE/blob/master/LICENSE)
- Repository: https://github.com/grepthat/libOpenDRIVE
- Version: v0.5.0

### earcut
- Required for: libOpenDRIVE
- License: [ISC License](https://github.com/mapbox/earcut/blob/main/LICENSE)
- Repository: https://github.com/mapbox/earcut
- Version: TODO

### pugixml
- Required for: libOpenDRIVE
- License: [MIT License](https://github.com/zeux/pugixml/blob/master/LICENSE.md)
- Repository: https://github.com/zeux/pugixml
- Version: 1.10

### eigen
- Required for: root finding algorithms for implementing polynomial geometry coordinate conversions
- License:
  - [MPL License](https://www.mozilla.org/en-US/MPL/2.0/)
  - [MPL License FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)
- Repository: https://gitlab.com/libeigen/eigen
- Version: 3.4.0

### RTree
- Required for: CoordinateConvrter optimization for conversion from inertial coordinates
- License: MIT License
- Repository: https://github.com/nushoin/RTree
- Version: master branch (commit 4bd551c7e2b2cf3fce9d4bf3d18f4c58d1efefc1)

## Development build dependencies

### Units
- License: MIT License
- Repository: https://github.com/nholthaus/units
- Version: 2.3.1

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
