/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_LOGIC_SUITE_H
#define ROADLOGICSUITE_ROAD_LOGIC_SUITE_H

#include "map_converter_config.h"

#include <MantleAPI/Common/position.h>
#include <MapAPI/i_map_loader.h>

#include <memory>
#include <optional>
#include <string>

namespace road_logic_suite
{
namespace map_validation
{
class ValidationRunner;
}

class RoadLogicSuiteImpl;
class RoadLogicSuite : public map_api::IMapLoader
{
  public:
    /// @brief Construct a RoadLogicSuite object with default MapConverterConfig parameters.
    RoadLogicSuite();
    /// @brief Construct a RoadLogicSuite object with a specific MapConverterConfig set.
    /// @param converter_config A MapConverterConfig containing specific parameters.
    RoadLogicSuite(MapConverterConfig& converter_config);
    /// @brief Default desctructor.
    ~RoadLogicSuite();

    /// @brief Loads the given OpenDRIVE file and returns the MantleMAP.
    /// @param map_file The path to the OpenDRIVE file.
    /// @return The mantle_map.
    [[nodiscard]] std::unique_ptr<map_api::Map> LoadMap(const std::string& map_file) const override;

    /// @brief Get the map validator.
    /// @return The validation runner.
    [[nodiscard]] std::unique_ptr<map_validation::ValidationRunner> GetMapValidator() const;

    /// @brief Takes coordinates in the inertial coordinate system and converts it into lane coordinate system.
    /// If the input values are invalid (i.e. s out of range or road does not exist), then the method will return
    /// std::nullopt.
    /// @param query The x-y-z coordinate in inertial coordinates.
    /// @return When valid input: The lane coordinates as an mantle_api::OpenDriveLanePosition.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::OpenDriveLanePosition> ConvertInertialToLaneCoordinates(
        const mantle_api::Vec3<units::length::meter_t>& query) const;

    /// @brief Takes coordinates in a lane position and converts it to inertial coordinates (x-y-z). If the input
    /// values are invalid (i.e. s out of range or road does not exist), then the method will return std::nullopt.
    /// @param query The coordinates in a lane position.
    /// @return When valid input: The coordinates in the inertial coordinate system.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::Vec3<units::length::meter_t>> ConvertLaneToInertialCoordinates(
        const mantle_api::OpenDriveLanePosition& query) const;

    /// @brief Takes coordinates in a road position coordinate system and converts it to inertial coordinates (x-y-z).
    /// If the input values are invalid (i.e. s out of range or road does not exist), then the method will return
    /// std::nullopt.
    /// @param query The coordinates in a road position coordinate system.
    /// @return When valid input: The coordinates in the inertial coordinate system.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::Vec3<units::length::meter_t>> ConvertRoadPositionToInertialCoordinates(
        const mantle_api::OpenDriveRoadPosition& query) const;

    /// @brief Calculate the road position heading at a specific s coordinate.
    /// @param road_id The road id.
    /// @param s The s coordinate.
    /// @return The road position heading [rad].
    [[nodiscard]] std::optional<units::angle::radian_t> GetRoadPositionHeading(const std::string& road_id,
                                                                               const units::length::meter_t& s) const;

  private:
    std::unique_ptr<RoadLogicSuiteImpl> impl_{};
    MapConverterConfig converter_config_{};
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_ROAD_LOGIC_SUITE_H
