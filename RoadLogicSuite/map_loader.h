/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MAPLOADER_H
#define ROADLOGICSUITE_MAPLOADER_H

#include <MapAPI/i_map_loader.h>

#include <memory>

namespace road_logic_suite
{

class RoadLogicSuiteImpl;

/// @brief Implements the MapLoader interface.
/// @note Currently, please utilize the RoadLogicSuite interface to load the map, as this dedicated map loader
/// implementation does not store the internal data structure. This is essential for coordinate conversion purposes.
/// Once the Coordinate Conversion interface has been designed, they can be restructured accordingly.
class MapLoader : public map_api::IMapLoader
{
  public:
    MapLoader();
    ~MapLoader();
    MapLoader(const MapLoader&) = delete;
    MapLoader& operator=(const MapLoader&) = delete;
    MapLoader(MapLoader&&) = default;
    MapLoader& operator=(MapLoader&&) = delete;

    /// @brief Load a given map.
    /// @param map_file the path to the openDRIVE file.
    /// @return The mantle_map.
    std::unique_ptr<map_api::Map> LoadMap(const std::string& map_file) const override;

  private:
    std::unique_ptr<RoadLogicSuiteImpl> road_logic_suite_impl_;
};

}  // namespace road_logic_suite
#endif  // ROADLOGICSUITE_MAPLOADER_H
