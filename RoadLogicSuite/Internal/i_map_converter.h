/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_I_MAP_CONVERTER_H
#define ROADLOGICSUITE_I_MAP_CONVERTER_H

#include "RoadLogicSuite/Internal/open_drive_data.h"

namespace road_logic_suite
{
/// @brief Defines the interface of a map converter.
/// @tparam TTargetMap the target map type.
template <typename TTargetMap>
class IMapConverter
{
  public:
    /// @brief The actual conversion method.
    /// @return The target map.
    [[nodiscard]] virtual TTargetMap Convert(const OpenDriveData& data_storage) = 0;
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_I_MAP_CONVERTER_H
