/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/default_validation_config.h"

#include "RoadLogicSuite/Internal/MapValidation/Validators/road_length_validator.h"

namespace road_logic_suite::map_validation
{
std::vector<std::unique_ptr<IValidator<Road>>> DefaultValidationConfig::GetRoadValidators()
{
    std::vector<std::unique_ptr<IValidator<Road>>> all_validators;
    all_validators.emplace_back(std::make_unique<RoadLengthValidator>());
    return all_validators;
}
}  // namespace road_logic_suite::map_validation
