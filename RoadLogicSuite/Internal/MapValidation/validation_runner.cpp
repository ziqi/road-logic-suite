/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/validation_runner.h"

namespace road_logic_suite::map_validation
{
void ValidationRunner::SetConfiguration(std::unique_ptr<AbstractValidationConfig> config)
{
    config_ = std::move(config);
}

std::vector<MapError> ValidationRunner::Validate() const
{
    std::vector<MapError> all_errors{};
    for (const auto& road_validator : config_->GetRoadValidators())
    {
        for (const auto& road : data_storage_.roads)
        {
            const auto validation_errors = road_validator->Validate(road.second, data_storage_, road.first);
            all_errors.insert(all_errors.end(), validation_errors.begin(), validation_errors.end());
        }
    }
    return all_errors;
}
}  // namespace road_logic_suite::map_validation