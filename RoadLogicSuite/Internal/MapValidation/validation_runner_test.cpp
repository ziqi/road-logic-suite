/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/validation_runner.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include <gtest/gtest.h>

#include <tuple>  // required for std::ignore

namespace road_logic_suite::map_validation::validators::test
{
TEST(ValidationRunnerTest, GivenAVerySimpledata_storage_WhenValidating_ThenThereShouldBeNoErrors)
{
    // Data storage mimics a simple road with two line of geometries of 500_m length.
    Road road{};
    road.length = units::length::meter_t(500);
    road.index_geometries_start = 0;
    road.index_geometries_end = 2;
    OpenDriveData data{};
    data.roads.insert_or_assign("0", road);
    data.geometries.push_back(
        std::make_unique<Line>(units::length::meter_t(0),
                               Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                               units::angle::radian_t(0),
                               units::length::meter_t(250),
                               "0"));
    data.geometries.push_back(
        std::make_unique<Line>(units::length::meter_t(250),
                               Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                               units::angle::radian_t(0),
                               units::length::meter_t(250),
                               "0"));
    ValidationRunner runner(data);

    const auto result = runner.Validate();

    ASSERT_TRUE(result.empty());
}

class FakeValidator : public IValidator<Road>
{
  public:
    std::vector<MapError> Validate(const Road& tested_object,
                                   const OpenDriveData& data_storage,
                                   const std::string& object_identifier) override
    {
        std::ignore = tested_object;
        std::ignore = data_storage;
        std::ignore = object_identifier;
        return std::vector<MapError>{MapError{.error_message = "This test fails."}};
    }
};

class FakeConfig : public AbstractValidationConfig
{
  public:
    std::vector<std::unique_ptr<IValidator<Road>>> GetRoadValidators() override
    {
        std::vector<std::unique_ptr<IValidator<Road>>> validators{};
        validators.emplace_back(std::make_unique<FakeValidator>());
        return validators;
    }
};

TEST(ValidationRunnerTest,
     GivenACustomConfigurationWithCustomValidatorThatThrowsAnError_WhenValidating_ThenTheErrorMustShowUp)
{
    OpenDriveData data{};
    data.roads.insert_or_assign("0", Road{});
    std::unique_ptr<AbstractValidationConfig> config = std::make_unique<FakeConfig>();
    ValidationRunner runner(data);
    runner.SetConfiguration(std::move(config));

    const auto results = runner.Validate();

    ASSERT_EQ(1, results.size());
    EXPECT_EQ("This test fails.", results[0].error_message);
    EXPECT_FALSE(config) << "Configuration ownership must be moved into runner.";
}
}  // namespace road_logic_suite::map_validation::validators::test
