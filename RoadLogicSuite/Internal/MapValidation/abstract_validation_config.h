/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ABSTRACT_VALIDATION_CONFIG_H
#define ROADLOGICSUITE_ABSTRACT_VALIDATION_CONFIG_H

#include "RoadLogicSuite/Internal/MapValidation/Validators/i_validator.h"
#include "RoadLogicSuite/Internal/road.h"

#include <memory>
#include <vector>

namespace road_logic_suite::map_validation
{
/// @brief Interface for validation configurations. This allows quickly adding configurations just by deriving from this
/// class.
class AbstractValidationConfig
{
  public:
    AbstractValidationConfig() = default;
    AbstractValidationConfig(const AbstractValidationConfig&) = default;
    AbstractValidationConfig(AbstractValidationConfig&&) = default;
    AbstractValidationConfig& operator=(const AbstractValidationConfig&) = default;
    AbstractValidationConfig& operator=(AbstractValidationConfig&&) = default;
    virtual ~AbstractValidationConfig() = default;

    /// @return Validators for road objects.
    virtual std::vector<std::unique_ptr<IValidator<Road>>> GetRoadValidators() { return {}; }
};
}  // namespace road_logic_suite::map_validation

#endif  // ROADLOGICSUITE_ABSTRACT_VALIDATION_CONFIG_H
