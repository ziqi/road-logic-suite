/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MAP_ERROR_H
#define ROADLOGICSUITE_MAP_ERROR_H

#include <string>

namespace road_logic_suite::map_validation
{
/// @brief A struct that defines a map error.
struct MapError
{
    /// @brief The error message.
    std::string error_message{};
};
}  // namespace road_logic_suite::map_validation

#endif  // ROADLOGICSUITE_MAP_ERROR_H
