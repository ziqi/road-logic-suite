/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/Validators/road_length_validator.h"

#include <cmath>
#include <sstream>

namespace road_logic_suite::map_validation
{
std::vector<MapError> RoadLengthValidator::Validate(const Road& road,
                                                    const OpenDriveData& data,
                                                    const std::string& object_identifier)
{
    std::vector<MapError> errors{};
    const auto expected_length = road.length;
    const auto actual_length = std::accumulate(
        std::next(data.geometries.begin(), road.index_geometries_start),
        std::next(data.geometries.begin(), road.index_geometries_end),
        units::length::meter_t(0),
        [](const units::length::meter_t& current_length, const std::unique_ptr<IGeometry>& current_geom) {
            return current_length + current_geom->length;
        });
    if (std::abs(expected_length() - actual_length()) > 1e-6)
    {
        std::ostringstream error_string;
        error_string << "Error in road \"" << object_identifier << "\": The road length (" << expected_length
                     << ") does not match the sum of the geometry lengths (" << actual_length << ").";
        errors.emplace_back(MapError{.error_message = error_string.str()});
    }
    return errors;
}
}  // namespace road_logic_suite::map_validation
