/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/Validators/road_length_validator.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"

#include <gtest/gtest.h>

#include <memory>

namespace
{
TEST(RoadLengthValidatorTestFixture, GivenOpenDriveRoadWithCorrectLength_WhenValidating_NoErrorMustBeReported)
{
    road_logic_suite::Road road{};
    road.length = units::length::meter_t(1000);
    road.index_geometries_start = 0;
    road.index_geometries_end = 2;
    road_logic_suite::OpenDriveData data{};
    data.roads.insert_or_assign("0", road);
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(500),
        "0"));
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(500),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(500),
        "0"));
    const auto road_identifier = "0";
    road_logic_suite::map_validation::RoadLengthValidator validator{};

    const auto errors = validator.Validate(road, data, road_identifier);

    ASSERT_EQ(0, errors.size());
}

TEST(RoadLengthValidatorTestFixture, GivenOpenDriveRoadWithInCorrectLength_WhenValidating_TheErrorMustBeReported)
{
    road_logic_suite::Road road{};
    road.length = units::length::meter_t(1000);
    road.index_geometries_start = 0;
    road.index_geometries_end = 2;
    road_logic_suite::OpenDriveData data{};
    data.roads.insert_or_assign("0", road);
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(250),
        "0"));
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(250),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(250),
        "0"));
    const auto road_identifier = "0";
    road_logic_suite::map_validation::RoadLengthValidator validator{};

    const auto errors = validator.Validate(road, data, road_identifier);

    ASSERT_EQ(1, errors.size());
    EXPECT_EQ(errors[0].error_message,
              "Error in road \"0\": The road length (1000 m) does not match the sum of the geometry lengths (500 m).");
}
}  // namespace
