/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "libopendrive_road_signal_converter.h"

#include "RoadLogicSuite/Internal/Types/signal.h"
#include "RoadLogicSuite/Internal/Utils/math_utils.h"
#include "RoadLogicSuite/Internal/libopendrive_converter_helpers.h"

namespace road_logic_suite
{

SignalUnit ConvertSignalUnit(const std::string& unit)
{
    static const std::unordered_map<std::string, SignalUnit> unit_map = {
        {"ft", SignalUnit::kFeet},
        {"km", SignalUnit::kKilometer},
        {"m", SignalUnit::kMeter},
        {"mile", SignalUnit::kMile},
        {"%", SignalUnit::kPercentage},
        {"km/h", SignalUnit::kKilometerPerHour},
        {"m/s", SignalUnit::kMeterPerSecond},
        {"mph", SignalUnit::kMilePerHour},
        {"kg", SignalUnit::kKilo},
        {"t", SignalUnit::kMetricTon},
        {"", SignalUnit::kNoUnit},
    };

    auto it = unit_map.find(unit);
    if (it != unit_map.end())
    {
        return it->second;
    }
    else
    {
        std::cerr << "[Signal Unit Conversion] Warning: Unmatched input '" << unit
                  << "', indicating a potential error from the input file (e.g., a typo). The signal unit is converted "
                     "to type kUnknown.\n";
        return SignalUnit::kUnknown;
    }
}

SignalOrientation ConvertSignalOrientation(const std::string& orientation)
{
    static const std::unordered_map<std::string, SignalOrientation> orientation_map = {
        {"+", SignalOrientation::kPostive},
        {"-", SignalOrientation::kNegative},
        {"none", SignalOrientation::kNone},
    };

    auto it = orientation_map.find(orientation);
    if (it != orientation_map.end())
    {
        return it->second;
    }
    else
    {
        std::cerr << "[Signal Orientation Conversion] Warning: Unmatched input '" << orientation
                  << "', indicating a potential error from the input file (e.g., a typo). The signal orientation is "
                     "converted to type kUnknown.\n";
        return SignalOrientation::kUnknown;
    }
}

bool IsSignalDynamic(const std::string& is_dynamic)
{
    if (is_dynamic == "yes")
    {
        return true;
    }
    else if (is_dynamic == "no")
    {
        return false;
    }
    else
    {
        std::cerr << "[Signal Dynamic Conversion] Warning: Unmatched input '" << is_dynamic
                  << "', indicating a potential error from the input file (e.g., a typo). The signal dynamic is "
                     "converted to false.\n";
        return false;
    }
}

std::vector<Vec2<units::length::meter_t>> CreateShapeForSingleSignal(const pugi::xml_node& xml_node)
{
    const auto length = xml_node.attribute("length").as_double(0);
    const auto width = xml_node.attribute("width").as_double(0);
    if (length != 0 && width != 0)
    {
        return math_utils::CreateRectangularShape(length, width);
    }
    return {};
}

std::vector<std::string> ConvertSignalDependencies(const pugi::xml_node& xml_node)
{
    std::vector<std::string> result;
    for (const auto& dependency_node : xml_node.children("dependency"))
    {
        const auto dependent_lane_id = dependency_node.attribute("id").as_string("");
        result.push_back(dependent_lane_id);
    }

    return result;
}

units::angle::radian_t ConvertSignalYawAngle(const pugi::xml_node& xml_node,
                                             const SignalOrientation& orientation,
                                             const double road_heading_value)
{
    const auto signal_hOffset = xml_node.attribute("hOffset").as_double(0);
    double yaw_angle;

    switch (orientation)
    {
        case SignalOrientation::kPostive:
            yaw_angle = M_PI + road_heading_value + signal_hOffset;
            break;
        case SignalOrientation::kNegative:
        case SignalOrientation::kNone:
        default:
            if (orientation != SignalOrientation::kNegative && orientation != SignalOrientation::kNone)
            {
                std::cerr << "[Signal YawAngle Conversion] Warning: Unknown orientation of the signal, the yaw angle "
                             "of the signal is converted relative to road reference line as default.\n";
            }
            yaw_angle = road_heading_value + signal_hOffset;
            break;
    }

    return units::angle::radian_t(yaw_angle);
}

Signal CreateSignal(const CoordinateConverter& converter, const odr::Road& xml_road, const pugi::xml_node& xml_node)
{
    const auto s = xml_node.attribute("s").as_double(0);
    const auto t = xml_node.attribute("t").as_double(0);
    const auto road_heading = converter.GetRoadPositionHeading(xml_road.id, units::length::meter_t(s));
    const auto z_offset = xml_node.attribute("zOffset").as_double(0);
    const auto position = helpers::GetInertialPosFromReferenceLinePos(converter, xml_road, s, t, z_offset);
    const auto unit = ConvertSignalUnit(xml_node.attribute("unit").as_string(""));
    const auto is_dynamic = IsSignalDynamic(xml_node.attribute("dynamic").as_string(""));
    const auto orientation = ConvertSignalOrientation(xml_node.attribute("orientation").as_string(""));
    const auto shape = CreateShapeForSingleSignal(xml_node);
    const auto valid_lanes = helpers::ConvertLaneValidity(xml_node);
    const auto dependencies = ConvertSignalDependencies(xml_node);
    return {
        .name = xml_node.attribute("name").as_string(""),
        .road_id = xml_road.id,
        .id = xml_node.attribute("id").as_string(""),
        .is_dynamic = is_dynamic,
        .country = xml_node.attribute("country").as_string(""),
        .country_revision = xml_node.attribute("countryRevision").as_string(""),
        .type = xml_node.attribute("type").as_string(""),
        .subtype = xml_node.attribute("subtype").as_string(""),
        .value = xml_node.attribute("value").as_double(0),
        .unit = unit,
        .length = units::length::meter_t(xml_node.attribute("length").as_double(0)),
        .width = units::length::meter_t(xml_node.attribute("width").as_double(0)),
        .height = units::length::meter_t(xml_node.attribute("height").as_double(0)),
        .yaw = ConvertSignalYawAngle(xml_node, orientation, road_heading->value()),
        .pitch = units::angle::radian_t(xml_node.attribute("pitch").as_double(0)),
        .roll = units::angle::radian_t(xml_node.attribute("roll").as_double(0)),
        .orientation = orientation,
        .text = xml_node.attribute("text").as_string(""),
        .position = position,
        .shape = shape,
        .valid_lanes = valid_lanes,
        .dependencies = dependencies,

    };
}

void ConvertSingleSignal(OpenDriveData& data_storage,
                         const odr::Road& xml_road,
                         const pugi::xml_node& xml_node,
                         const CoordinateConverter& converter)
{
    const auto signal = CreateSignal(converter, xml_road, xml_node);
    data_storage.signals.insert_or_assign(signal.id, signal);
}

void LibOpenDriveRoadSignalConverter::ConvertForRoad(OpenDriveData& data_storage, const odr::Road& xml_road)
{
    const CoordinateConverter converter(data_storage);
    for (const auto& xml_node : xml_road.xml_node.child("signals").children("signal"))
    {
        ConvertSingleSignal(data_storage, xml_road, xml_node, converter);
    }
}
}  // namespace road_logic_suite
