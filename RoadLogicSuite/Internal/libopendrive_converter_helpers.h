/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LIBOPENDRIVE_CONVERTER_HELPERS_H
#define ROADLOGICSUITE_LIBOPENDRIVE_CONVERTER_HELPERS_H

#include "RoadLogicSuite/Internal/coordinate_converter.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <OpenDriveMap.h>

namespace road_logic_suite
{
namespace helpers
{

LinkContactType GetContactType(odr::RoadLink::ContactPoint xml_contact_point);

types::JunctionContactType GetContactType(odr::JunctionConnection::ContactPoint xml_connection_contact_point);

std::vector<RoadLink> CreateRoadLinks(const odr::Road& xml_road);

std::vector<types::JunctionLaneLink> CreateJunctionLinks(const odr::JunctionConnection& xml_junction_connection);

std::vector<types::LaneId> ConvertLaneValidity(const pugi::xml_node& xml_node);

mantle_api::Vec3<units::length::meter_t> GetInertialPosFromReferenceLinePos(const CoordinateConverter& converter,
                                                                            const odr::Road& xml_road,
                                                                            double s,
                                                                            double t,
                                                                            double z_offset);

}  // namespace helpers
}  // namespace road_logic_suite

#endif
