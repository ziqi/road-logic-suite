/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_COORDINATE_CONVERTER_H
#define ROADLOGICSUITE_COORDINATE_CONVERTER_H

#include "RoadLogicSuite/Internal/open_drive_data.h"
#include "i_geometry_lookup.h"

#include <MantleAPI/Common/position.h>

namespace road_logic_suite
{
/// @brief This class is responsible for converting different kinds of coordinate systems into each other. The following
/// coordinate systems that are defined by the OpenDRIVE standard are supported:
/// <ul>
/// <li> Inertial Coordinates (Cartesian Coordinates x-y-z)
/// <li> Road position coordinates (road identifier-s-t-h)
/// </ul>
/// The following coordinate systems, that are not defined by the OpenDRIVE standard are supported:
/// <ul>
/// <li> A lane coordinate system which is defined through the road identifier, the lane id and then works
///      similar to the road position coordinate system using s-t-h coordinates.
/// </ul>
class CoordinateConverter
{
  public:
    /// @brief Creates a new coordinate converter using the internal data storage.
    /// @param data_storage The data storage.
    explicit CoordinateConverter(const OpenDriveData& data_storage);
    ~CoordinateConverter() = default;

    /// @brief Takes coordinates in the inertial coordinate system and converts them into a road position coordinate
    /// system.
    /// @param query The x-y-z coordinate in inertial coordinates.
    /// @return When valid input: The road position coordinates as an OpenDriveRoadPosition.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::OpenDriveRoadPosition> ConvertInertialToRoadPositionCoordinates(
        const mantle_api::Vec3<units::length::meter_t>& query) const;

    /// @brief Takes coordinate in the inertial coordinate system and converts them into a lane coordinate system
    /// for road @p road_id and lane @p lane_id.
    /// @param query The x-y-z coordinate in inertial coordinates.
    /// @return When valid input: The lane coordinates as LanePosition.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::OpenDriveLanePosition> ConvertInertialToLaneCoordinates(
        const mantle_api::Vec3<units::length::meter_t>& query) const;

    /// @brief Takes coordinates in the lane coordinate system for road @p road_id and lane @p lane_id and converts
    /// them into an inertial coordinate system.
    /// @param query The s-t-h coordinate, road id and lane id as LanePosition.
    /// @return When valid input: The lane coordinates as inertial coordinates.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::Vec3<units::length::meter_t>> ConvertLaneToInertialCoordinates(
        const mantle_api::OpenDriveLanePosition& query) const;

    /// @brief Takes coordinates in a road position coordinate system for road @p road_id and converts it to inertial
    /// coordinates (x-y-z). If the input values are invalid (i.e. s out of range or road does not exist), then the
    /// method will return std::nullopt.
    /// @param query The coordinates in a road position coordinate system.
    /// @return When valid input: The coordinates in the inertial coordinate system.<br>
    ///         When invalid input: std::nullopt.
    [[nodiscard]] std::optional<mantle_api::Vec3<units::length::meter_t>> ConvertRoadPositionToInertialCoordinates(
        const mantle_api::OpenDriveRoadPosition& query) const;

    /// @brief Calculate the road position heading at a specific s coordinate.
    /// @param road_id The road id.
    /// @param s The s coordinate.
    /// @return The road position heading [rad].
    [[nodiscard]] std::optional<units::angle::radian_t> GetRoadPositionHeading(const std::string& road_id,
                                                                               const units::length::meter_t& s) const;

    /// @brief May optimize the geometry with an internal RTree for a faster search.
    /// Can influence the processing speed of subsequent conversions from inertial coordinates.
    void OptimizeStorage();

  private:
    const OpenDriveData& data_storage_;
    std::unique_ptr<IGeometryLookup> geometry_lookup_;

    [[nodiscard]] IGeometry* GetGeometryAtS(const Road& road, double s) const;

    static double CalcAt(const types::RangeBasedMap<units::length::meter_t, types::Poly3>& polys,
                         const units::length::meter_t& s);
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_COORDINATE_CONVERTER_H
