/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/arc.h"

#include <gtest/gtest.h>

namespace
{
constexpr double kAbsError = 1e-6;
constexpr double kAbsErrorLow = 1e-3;

/// @note  Tests are through integration tests. Add more tests if necessary
TEST(ArcTest, GivenASampleArc_WhenCalculatingTheHeadingAtTheBeginning_ThenItMustBeEqualToTheInitialHeading)
{
    const auto initial_heading = units::angle::radian_t(-0.565);
    const road_logic_suite::Arc arc(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        initial_heading,
        units::length::meter_t(100),
        road_logic_suite::custom_units::curvature_t(0.001),
        "0");

    EXPECT_NEAR(arc.GetHeadingAt(units::length::meter_t(0)).value(), initial_heading(), kAbsError);
}

TEST(ArcTest, GivenAQuarterCircle_WhenCalculatingTheHeadingAfterPiOverTwo_ThenTheHeadingMustBePiOverTwo)
{
    const auto pi_over_two = 1.570796;
    const auto length = pi_over_two * 1000;

    const road_logic_suite::Arc arc(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(length),
        road_logic_suite::custom_units::curvature_t(0.001),
        "0");

    EXPECT_NEAR(arc.GetHeadingAt(units::length::meter_t(length)).value(), pi_over_two, kAbsError);
}

TEST(ArcTest, GivenAQuarterCircle_WhenCalculatingBoundingBox_ThenDefaultImplementationShouldBeCorrect)
{
    const auto pi_over_two = 1.570796;
    const auto length = pi_over_two * 1000;

    const road_logic_suite::Arc arc(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(length),
        road_logic_suite::custom_units::curvature_t(0.001),
        "0");

    const auto aabb = arc.GetBoundingBox();

    // minimal size for quarter circle is radius;radius and expansion is 20m
    EXPECT_NEAR(aabb.lower_left.x(), -20.0, kAbsErrorLow);
    EXPECT_NEAR(aabb.lower_left.y(), -20.0, kAbsErrorLow);
    EXPECT_NEAR(aabb.upper_right.x(), 1020.0, kAbsErrorLow);
    EXPECT_NEAR(aabb.upper_right.y(), 1020.0, kAbsErrorLow);
}

}  // namespace
