/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LINE_H
#define ROADLOGICSUITE_LINE_H

#include "RoadLogicSuite/Internal/Geometry/i_geometry.h"

namespace road_logic_suite
{

class Line : public IGeometry
{
  public:
    Line(const units::length::meter_t& s,
         const Vec2<units::length::meter_t>& position,
         const units::angle::radian_t& hdg,
         const units::length::meter_t& length,
         const std::string& road_id);

    /// @brief For a line geometry, this calculates the s and t values for the query point.
    /// Only two dimensions are considered currently as streets are simplified to be flat surfaces here.
    /// @param query_point the query point.
    /// @return when input valid: an optional array consisting of the s and t values for the line geometry.<br>
    /// when input invalid: std::nullopt
    std::optional<Vec2<units::length::meter_t>> ConvertInertialCoordinatesToST(
        const Vec2<units::length::meter_t>& query_point) const override;

    /// @brief Calculates the offset, also known as 'lambda' of the query point on the line. The result can also be
    /// negative or bigger than the length of the line, however those values are invalid.
    /// @param query_point the query point.
    /// @return the offset.
    units::length::meter_t OffsetOfClosestPointOnLine(const Vec2<units::length::meter_t>& query_point) const;

    /// @brief Calculates the signed distance of the query point to the line. The sign is defined in the same way as
    /// OpenDRIVE is defined. When looking from the line in the heading direction, if the query point is to the left of
    /// the line, then the result is positive, otherwise negative.
    /// @param query_point the query point.
    /// @return the signed distance.
    units::length::meter_t SignedDistanceToLine(const Vec2<units::length::meter_t>& query_point) const;

    Vec2<units::length::meter_t> ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                const units::length::meter_t& query_t) const override;

    custom_units::curvature_t CalcCurvatureAt([[maybe_unused]] const units::length::meter_t& query_s) const override;

    [[nodiscard]] units::angle::radian_t GetHeadingAt(const units::length::meter_t& s) const override;

    [[nodiscard]] types::Rect<units::length::meter_t> GetBoundingBox() const override;
};

}  // namespace road_logic_suite
#endif  // ROADLOGICSUITE_LINE_H
