/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/i_geometry.h"

namespace
{
constexpr units::length::meter_t kSamplingRate(50.0);
}
namespace road_logic_suite
{
IGeometry::IGeometry(const units::length::meter_t& s,
                     const Vec2<units::length::meter_t>& position,
                     const units::angle::radian_t& hdg,
                     const units::length::meter_t& length,
                     const std::string& road_id)
    : s(s), position(position), hdg(hdg), length(length), road_id(road_id)
{
}

types::Rect<units::length::meter_t> IGeometry::GetBoundingBox() const
{
    types::Rect<units::length::meter_t> aabb = types::Rect<units::length::meter_t>::CreateRectAroundPoint(position, {});
    for (auto s_i = s; s_i < s + length; s_i += kSamplingRate)
    {
        aabb.Encapsulate(ConvertSTCoordinatesToInertial(s_i, units::length::meter_t(0)));
    }
    aabb.Encapsulate(ConvertSTCoordinatesToInertial(s + length, units::length::meter_t(0)));
    aabb.Expand(units::length::meter_t(20));
    return aabb;
}
}  // namespace road_logic_suite
