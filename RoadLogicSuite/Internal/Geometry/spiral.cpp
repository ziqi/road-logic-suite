/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/spiral.h"

#include <Geometries/Spiral/odrSpiral.h>

namespace road_logic_suite
{
constexpr int kAmountPreSamplingPoints = 11;
constexpr int kMaxNewtonSteps = 50;

Spiral::Spiral(const units::length::meter_t& s,
               const Vec2<units::length::meter_t>& position,
               const units::angle::radian_t& hdg,
               const units::length::meter_t& length,
               const std::string& road_id,
               const custom_units::curvature_t& curvature_start,
               const custom_units::curvature_t& curvature_end)
    : IGeometry(s, position, hdg, length, road_id), curvature_start(curvature_start), curvature_end(curvature_end)
{
    this->curvature_change_rate_ = ((curvature_end - curvature_start) / length).value();
    this->s0_spiral_ = curvature_start() / curvature_change_rate_;
    this->clothoid_scale_ = std::sqrt(2 / std::abs(curvature_change_rate_));
    odrSpiral(s0_spiral_, curvature_change_rate_, &x0_spiral_, &y0_spiral_, &hdg0_spiral_);
}

std::optional<Vec2<units::length::meter_t>> Spiral::ConvertInertialCoordinatesToST(
    const Vec2<units::length::meter_t>& query_point) const
{
    const auto xq = query_point.x();
    const auto yq = query_point.y();
    auto s_current = GetStartingValueForNewtonsMethod(xq, yq);
    auto signed_distance = 0.0;

    uint newton_step = 0;
    for (; newton_step < kMaxNewtonSteps; newton_step++)
    {
        const auto [x, y] = GetSpiralPositionAtS(s_current);
        const auto [dx, dy] = GetFirstDerivativeAtS(s_current);
        const auto [ddx, ddy] = GetSecondDerivativeAtS(s_current);
        // Calculate derivative of squared distance function:
        const auto d_sq_dist_s = -2 * ((xq - x) * dx + (yq - y) * dy);
        const auto dd_sq_dist_s = 2 * (-(xq - x) * ddx + dx * dx - (yq - y) * ddy + dy * dy);
        if (std::abs(d_sq_dist_s) < 1e-6)
        {
            const auto cross_product = (dx * (yq - y)) - (dy * (xq - x));
            const auto sgn = (0 < cross_product) - (cross_product < 0);
            signed_distance = sgn * std::sqrt((xq - x) * (xq - x) + (yq - y) * (yq - y));
            break;
        }
        s_current = s_current - d_sq_dist_s / dd_sq_dist_s;
    }

    if ((newton_step == kMaxNewtonSteps) || (s_current < this->s() || s_current > this->s() + length()))
    {
        return std::nullopt;
    }

    return Vec2(units::length::meter_t(s_current), units::length::meter_t(signed_distance));
}

Vec2<units::length::meter_t> Spiral::ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                    const units::length::meter_t& query_t) const
{
    double xs_spiral{0.0};
    double ys_spiral{0.0};
    double as_spiral{0.0};
    const auto s_in = query_s() - s() + s0_spiral_;
    odrSpiral(s_in, curvature_change_rate_, &xs_spiral, &ys_spiral, &as_spiral);

    const auto hdg = this->hdg() - hdg0_spiral_;
    const auto xt =
        (std::cos(hdg) * (xs_spiral - x0_spiral_)) - (std::sin(hdg) * (ys_spiral - y0_spiral_)) + position.x();
    const auto yt =
        (std::sin(hdg) * (xs_spiral - x0_spiral_)) + (std::cos(hdg) * (ys_spiral - y0_spiral_)) + position.y();

    const auto heading_at_query = as_spiral + hdg;
    const auto x_offset = -std::sin(heading_at_query) * query_t();
    const auto y_offset = std::cos(heading_at_query) * query_t();

    return Vec2<units::length::meter_t>{units::length::meter_t(xt + x_offset), units::length::meter_t(yt + y_offset)};
}

custom_units::curvature_t Spiral::CalcCurvatureAt([[maybe_unused]] const units::length::meter_t& query_s) const
{
    /// @todo  Not implemented, yet. (Just linear interpolation)
    return custom_units::curvature_t(0);
}
units::angle::radian_t Spiral::GetHeadingAt(const units::length::meter_t& s) const
{
    double xs_spiral{0.0};
    double ys_spiral{0.0};
    double as_spiral{0.0};
    const auto s_in = s() - this->s() + s0_spiral_;
    odrSpiral(s_in, curvature_change_rate_, &xs_spiral, &ys_spiral, &as_spiral);
    return units::angle::radian_t(as_spiral) + this->hdg - units::angle::radian_t(hdg0_spiral_);
}

std::pair<double, double> Spiral::GetSpiralPositionAtS(double query_s) const
{
    double xs_spiral{0.0};
    double ys_spiral{0.0};
    double as_spiral{0.0};
    const auto s_in = query_s - this->s() + s0_spiral_;
    odrSpiral(s_in, curvature_change_rate_, &xs_spiral, &ys_spiral, &as_spiral);

    const auto hdg = this->hdg() - hdg0_spiral_;
    const auto xt =
        (std::cos(hdg) * (xs_spiral - x0_spiral_)) - (std::sin(hdg) * (ys_spiral - y0_spiral_)) + position.x();
    const auto yt =
        (std::sin(hdg) * (xs_spiral - x0_spiral_)) + (std::cos(hdg) * (ys_spiral - y0_spiral_)) + position.y();

    return {xt, yt};
}

std::pair<double, double> Spiral::GetFirstDerivativeAtS(double query_s) const
{
    const auto s_in = (query_s - this->s() + s0_spiral_);
    const auto sgn = (0 < curvature_change_rate_) - (curvature_change_rate_ < 0);
    const auto hdg = sgn * (this->hdg() - hdg0_spiral_);
    const auto dx = std::cos((s_in * s_in) / (clothoid_scale_ * clothoid_scale_) + hdg);
    const auto dy = std::sin((s_in * s_in) / (clothoid_scale_ * clothoid_scale_) + hdg);
    return {
        dx,
        sgn * dy,
    };
}

std::pair<double, double> Spiral::GetSecondDerivativeAtS(double query_s) const
{
    const auto s_in = (query_s - this->s() + s0_spiral_);
    const auto factor = 2 * s_in / (clothoid_scale_ * clothoid_scale_);
    const auto sgn = (0 < curvature_change_rate_) - (curvature_change_rate_ < 0);
    const auto hdg = sgn * (this->hdg() - hdg0_spiral_);
    const auto ddx = -factor * std::sin((s_in * s_in) / (clothoid_scale_ * clothoid_scale_) + hdg);
    const auto ddy = factor * std::cos((s_in * s_in) / (clothoid_scale_ * clothoid_scale_) + hdg);
    return {ddx, sgn * ddy};
}

double Spiral::GetStartingValueForNewtonsMethod(double xq, double yq) const
{
    auto s_start = 0.0;
    auto closest_dist = std::numeric_limits<double>::max();
    for (int i = 0; i < kAmountPreSamplingPoints; i++)
    {
        const auto s_candidate = this->s() + length() * (static_cast<double>(i) / 10);
        const auto [x0, y0] = GetSpiralPositionAtS(s_candidate);
        const double sq_dist = (xq - x0) * (xq - x0) + (yq - y0) * (yq - y0);
        if (sq_dist < closest_dist)
        {
            s_start = s_candidate;
            closest_dist = sq_dist;
        }
    }
    return s_start;
}

}  // namespace road_logic_suite
