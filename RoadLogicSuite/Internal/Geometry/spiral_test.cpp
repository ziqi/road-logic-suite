/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/spiral.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace road_logic_suite
{
static const double kAbsErrorMax = 1e-3;

struct ConversionTestCaseDefinition
{
    std::string name{};
    std::pair<double, double> input_inertial{};
    std::pair<double, double> expected_output_st{};
    // Spiral parameters
    double curvature_start{};
    double curvature_end{};
    // Optional geometry parameters
    double s0{};
    double length{100};
    double x0{};
    double y0{};
    double hdg0{};
};

class SpiralInertialToSTConversionTestFixture : public ::testing::TestWithParam<ConversionTestCaseDefinition>
{
};

const std::vector<ConversionTestCaseDefinition> kConversionTestCases{
    {.name = "Simple_Spiral",
     .input_inertial = {10, 0},
     .expected_output_st = {9.999, -0.083},
     .curvature_start = 0,
     .curvature_end = 0.05},
    {.name = "Ascending_Positive_Curvature",
     .input_inertial = {22, 31},
     .expected_output_st = {47.375, 2.983},
     .curvature_start = 0.0351,
     .curvature_end = 0.05714},
    {.name = "Descending_Positive_Curvature",
     .input_inertial = {29, 45},
     .expected_output_st = {60.048, -2.017},
     .curvature_start = 0.0413,
     .curvature_end = 0.0044},
    {.name = "Right_Turn_To_Left_Turn",
     .input_inertial = {-18, 0},
     .expected_output_st = {46.573, 0.341},
     .curvature_start = -0.17,
     .curvature_end = 0.151},
    {.name = "Left_Turn_To_Right_Turn",
     .input_inertial = {17, 12},
     .expected_output_st = {20.902, -2.281},
     .curvature_start = 0.0733,
     .curvature_end = -0.0148},
    {.name = "Spiral_With_S0",
     .input_inertial = {52.8, -4.8},
     .expected_output_st = {90.185, -2.312},
     .curvature_start = -0.0622,
     .curvature_end = 0.1492,
     .s0 = 24.4,
     .length = 75.6},
    {.name = "Spiral_With_NonZeroInitPosition",
     .input_inertial = {15.0, -65.24},
     .expected_output_st = {37.010, 2.490},
     .curvature_start = -0.059,
     .curvature_end = -0.0498,
     .x0 = -1.7,
     .y0 = -37.2},
    {.name = "Spiral_With_NonZeroHeading",
     .input_inertial = {-43.65, -14.42},
     .expected_output_st = {46.544, 2.54},
     .curvature_start = -0.0109,
     .curvature_end = 0.0526,
     .hdg0 = 3.431},
};

TEST(SpiralTest, GivenSimpleSpiralRoad_WhenConvertingSTCoordinates_ThenCorrectInertialCoordinatesMustBeReturned)
{
    auto spiral =
        Spiral(units::length::meter_t(0.0),                                                         // s
               Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),  // position
               units::angle::radian_t(0.0),                                                         // hdg
               units::length::meter_t(100.0),                                                       // length
               "roadId",                                                                            // id
               custom_units::curvature_t(0),                                                        // curvature_start
               custom_units::curvature_t(100));                                                     // curvature_end)

    auto result = spiral.ConvertSTCoordinatesToInertial(units::length::meter_t(3.4), units::length::meter_t(0));

    // compare with the plotted graph in
    // Documentation/img/klothoide-mit-näherungsformel-und-im-straßenbau-verwendeter-klothoidenabschnitt.jpg
    ASSERT_NEAR(result.x(), 0.726545, kAbsErrorMax);  //  tip of the yellow line
    ASSERT_NEAR(result.y(), 0.644613, kAbsErrorMax);
}

TEST(SpiralTest,
     GivenSpiralGeometryWithXYOffset_WhenConvertingToSTCoordinates_ThenCorrectInertialCoordinatesMustBeReturned)
{
    const units::length::meter_t x_offset(-1.76);
    const units::length::meter_t y_offset(0.67);
    const auto spiral = Spiral(units::length::meter_t(0.0),                       // s
                               Vec2<units::length::meter_t>(x_offset, y_offset),  // position
                               units::angle::radian_t(0.0),                       // hdg
                               units::length::meter_t(100.0),                     // length
                               "roadId",                                          // id
                               custom_units::curvature_t(0),                      // curvature_start
                               custom_units::curvature_t(100));                   // curvature_end)

    const auto result = spiral.ConvertSTCoordinatesToInertial(units::length::meter_t(3.4), units::length::meter_t(0));

    // compare with the plotted graph in
    // Documentation/img/klothoide-mit-näherungsformel-und-im-straßenbau-verwendeter-klothoidenabschnitt.jpg
    ASSERT_NEAR(result.x(), -1.033455, kAbsErrorMax);  //  tip of the yellow line
    ASSERT_NEAR(result.y(), 1.314613, kAbsErrorMax);
}

TEST(SpiralTest,
     GivenSpiralWithNonZeroCurvatureStart_WhenConvertingToSTCoordinates_ThenCorrectInertialCoordinatesMustBeReturned)
{
    const Spiral spiral(units::length::meter_t(0.0),
                        Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                        units::angle::radian_t(0.0),
                        units::length::meter_t(100.0),
                        "0",
                        custom_units::curvature_t(0.00382),
                        custom_units::curvature_t(0.00211));

    const auto result = spiral.ConvertSTCoordinatesToInertial(units::length::meter_t(87.5), units::length::meter_t(0));

    EXPECT_NEAR(result.x(), 86.3163, kAbsErrorMax);
    EXPECT_NEAR(result.y(), 12.6323, kAbsErrorMax);
}

TEST_P(SpiralInertialToSTConversionTestFixture, GivenSpiral_WhenConvertInertialCoordinatesToST_ThenReturnNullopt)
{
    const auto params = GetParam();
    // ConvertInertialCoordinatesToST is not implemented
    const auto spiral = Spiral(
        units::length::meter_t(params.s0),                                                                   // s
        Vec2<units::length::meter_t>(units::length::meter_t(params.x0), units::length::meter_t(params.y0)),  // position
        units::angle::radian_t(params.hdg0),                                                                 // hdg
        units::length::meter_t(params.length),                                                               // length
        "roadId",                                                                                            // id
        custom_units::curvature_t(params.curvature_start),  // curvature_start
        custom_units::curvature_t(params.curvature_end));   // curvature_end)
    const auto [x_in, y_in] = params.input_inertial;
    const auto result = spiral.ConvertInertialCoordinatesToST(
        Vec2<units::length::meter_t>(units::length::meter_t(x_in), units::length::meter_t(y_in)));

    ASSERT_TRUE(result);
    const auto [s, t] = result.value();
    const auto [s_expected, t_expected] = params.expected_output_st;
    EXPECT_NEAR(s(), s_expected, kAbsErrorMax);
    EXPECT_NEAR(t(), t_expected, kAbsErrorMax);
}

TEST(SpiralTest, GivenSpiral_WhenCalcCurvatureAt_ThenReturnDefaultValue)
{
    // CalcCurvatureAt is not implemented
    const auto spiral =
        Spiral(units::length::meter_t(0.0),                                                         // s
               Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),  // position
               units::angle::radian_t(0.0),                                                         // hdg
               units::length::meter_t(100.0),                                                       // length
               "roadId",                                                                            // id
               custom_units::curvature_t(0),                                                        // curvature_start
               custom_units::curvature_t(100));                                                     // curvature_end)

    const auto result = spiral.CalcCurvatureAt(units::length::meter_t(0));

    EXPECT_EQ(result, custom_units::curvature_t(0));
}

TEST(SpiralTest, GivenSpiral_WenCalculatingHeadingAtSpecificPosition_ThenReturnCorrectValue)
{
    const Spiral spiral(units::length::meter_t(0),
                        Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                        units::angle::radian_t(0),
                        units::length::meter_t(1000),
                        "",
                        custom_units::curvature_t(0.0065),
                        custom_units::curvature_t(0.0056));

    EXPECT_NEAR(spiral.GetHeadingAt(units::length::meter_t(712)).value(), 4.400, kAbsErrorMax);
    EXPECT_NEAR(spiral.GetHeadingAt(units::length::meter_t(796)).value(), 4.889, kAbsErrorMax);
    EXPECT_NEAR(spiral.GetHeadingAt(units::length::meter_t(255)).value(), 1.628, kAbsErrorMax);
}

INSTANTIATE_TEST_SUITE_P(SpiralTest,
                         SpiralInertialToSTConversionTestFixture,
                         ::testing::ValuesIn(kConversionTestCases),
                         [](const ::testing::TestParamInfo<ConversionTestCaseDefinition>& info) {
                             return info.param.name;
                         });
}  // namespace road_logic_suite
