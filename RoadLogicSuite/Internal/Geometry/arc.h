/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ARC_H
#define ROADLOGICSUITE_ARC_H

#include "RoadLogicSuite/Internal/Geometry/i_geometry.h"

namespace road_logic_suite
{

class Arc : public IGeometry
{
  public:
    /// @brief For arc geometries this is the curvature k. A positive value denotes a left turn. A negative value
    /// denotes a right turn and a value of 0 is set for any geometry that is not an arc.

    custom_units::curvature_t curvature{0};

    Arc(const units::length::meter_t& s,
        const Vec2<units::length::meter_t>& position,
        const units::angle::radian_t& hdg,
        const units::length::meter_t& length,
        const custom_units::curvature_t& curvature,
        const std::string& road_id);

    std::optional<Vec2<units::length::meter_t>> ConvertInertialCoordinatesToST(
        const Vec2<units::length::meter_t>& query_point) const override;

    Vec2<units::length::meter_t> ConvertSTCoordinatesToInertial(const units::length::meter_t& query_s,
                                                                const units::length::meter_t& query_t) const override;

    custom_units::curvature_t CalcCurvatureAt([[maybe_unused]] const units::length::meter_t& query_s) const override;

    [[nodiscard]] units::angle::radian_t GetHeadingAt(const units::length::meter_t& s) const override;

    units::length::meter_t ArcLengthToQueryProjectedToCircle(const Vec2<units::length::meter_t>& query,
                                                             const Vec2<units::length::meter_t>& center_point) const;

    /// @brief Calculates the center point of the given arc.
    /// @param arc the given arc.
    /// @return the center point in two dimensional inertial coordinates.

    Vec2<units::length::meter_t> CenterPoint() const;

    /// @brief Calculates the arc length and applies the proper unit.
    /// @param angle the given angle in radians.
    /// @param radius the given radius in meters.
    /// @return the arc length in meters.

    static units::length::meter_t ArcLength(const units::angle::radian_t& angle, const units::length::meter_t& radius);

    /// @brief Calculates the angle for a given arc length and curvature and applies the correct units.
    /// @param arc_length the given arc length in meters.
    /// @param curvature the given curvature in 1/m.
    /// @return the angle in radians.

    static units::angle::radian_t Angle(const units::length::meter_t& arc_length,
                                        const custom_units::curvature_t& curvature);

  private:
    bool ArcIsValidQuery(const Vec2<units::length::meter_t>& query,
                         const Vec2<units::length::meter_t>& center_point) const;
};

}  // namespace road_logic_suite
#endif  // ROADLOGICSUITE_ARC_H
