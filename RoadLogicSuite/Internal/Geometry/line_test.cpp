/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/line.h"

#include <gtest/gtest.h>

namespace
{
constexpr double kAbsError = 1e-6;
/// @note Tests are through integration tests. Add more tests if necessary

TEST(LineTest, GivenASimpleLineDefinition_WhenCalculatingTheHeading_ThenTheInitialHeadingMustAlwaysBeReturned)
{
    const auto initial_heading = units::angle::radian_t(-0.817);
    const road_logic_suite::Line line(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        initial_heading,
        units::length::meter_t(100),
        "");

    EXPECT_EQ(initial_heading, line.GetHeadingAt(units::length::meter_t(13)));
    EXPECT_EQ(initial_heading, line.GetHeadingAt(units::length::meter_t(41)));
    EXPECT_EQ(initial_heading, line.GetHeadingAt(units::length::meter_t(31)));
}

TEST(LineTest, GivenASimpleLineWithSOffset_WhenCalculatingTheBoundingBox_ThenTheBoundingBoxIsCorrect)
{
    const road_logic_suite::Line line(
        units::length::meter_t(61.5),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        units::angle::radian_t(0),
        units::length::meter_t(54.2),
        "");

    const auto aabb = line.GetBoundingBox();

    EXPECT_NEAR(0, aabb.lower_left.x(), kAbsError);
    EXPECT_NEAR(0, aabb.lower_left.y(), kAbsError);
    EXPECT_NEAR(54.2, aabb.upper_right.x(), kAbsError);
    EXPECT_NEAR(0, aabb.upper_right.y(), kAbsError);
}

TEST(LineTest, GivenASimpleLineDefinition_WhenConvertingInertialCoordinatesToST_ThenCorrectSTCoordinateMustBeReturned)
{
    const auto initial_heading = units::angle::radian_t(0);
    const road_logic_suite::Line line(
        units::length::meter_t(10),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
        initial_heading,
        units::length::meter_t(100),
        "");

    auto solution = line.ConvertInertialCoordinatesToST(
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(1.0), units::length::meter_t(-1.0)));

    ASSERT_TRUE(solution.has_value());
    EXPECT_EQ(solution.value().x(), 11.0);
    EXPECT_EQ(solution.value().y(), -1);
}

}  // namespace
