/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Geometry/param_poly_3.h"

#include <gtest/gtest.h>

namespace road_logic_suite
{

static const double kAbsErrorMax = 1e-6;

// GivenSimpleStraightRoad_WhenConvertingSTCoordinates_ThenCorrectInertialPositionMustBeReturned
TEST(ParamPoly3Test,
     GivenSimpleParametricPolynomialRoad_WhenConvertingInertialCoordinates_ThenCorrectSTCoordinatesMustBeReturned)
{
    auto param_poly = ParamPoly3(units::length::meter_t(0),
                                 Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                                 units::angle::radian_t(0),
                                 units::length::meter_t(100),
                                 "polynomial_road",
                                 types::Poly3(0, 1, 0, 0),
                                 types::Poly3(1.5, 1, -3.5, 1),
                                 false);

    auto solution = param_poly.ConvertInertialCoordinatesToST(
        Vec2<units::length::meter_t>(units::length::meter_t(0.5), units::length::meter_t(-1)));

    ASSERT_TRUE(solution.has_value());
    // compare with the plotted graph in Documentation/img/parametric_polynomial.png
    ASSERT_NEAR(solution.value().x(), 1.246931, kAbsErrorMax);
    EXPECT_LT(solution.value().y(), 0);
}

TEST(
    ParamPoly3Test,
    GivenSimpleParametricPolynomialRoadWithNonZeroS_WhenConvertingInertialCoordinates_ThenCorrectSTCoordinatesMustBeReturned)
{
    auto param_poly = ParamPoly3(units::length::meter_t(10),
                                 Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                                 units::angle::radian_t(0),
                                 units::length::meter_t(100),
                                 "polynomial_road",
                                 types::Poly3(0, 1, 0, 0),
                                 types::Poly3(1.5, 1, -3.5, 1),
                                 false);

    auto solution = param_poly.ConvertInertialCoordinatesToST(
        Vec2<units::length::meter_t>(units::length::meter_t(0.5), units::length::meter_t(-1)));

    ASSERT_TRUE(solution.has_value());
    // compare with the plotted graph in Documentation/img/parametric_polynomial.png
    ASSERT_NEAR(solution.value().x(), 11.246931, kAbsErrorMax);
    EXPECT_LT(solution.value().y(), 0);
}

TEST(
    ParamPoly3Test,
    GivenSimpleParametricPolynomialRoadWithNonZeroHeading_WhenConvertingInertialCoordinates_ThenCorrectSTCoordinatesMustBeReturned)
{
    auto param_poly = ParamPoly3(units::length::meter_t(10),
                                 Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                                 units::angle::radian_t(1),
                                 units::length::meter_t(100),
                                 "polynomial_road",
                                 types::Poly3(0, 1, 0, 0),
                                 types::Poly3(0, 0, -3.5, 1),
                                 false);

    auto solution = param_poly.ConvertInertialCoordinatesToST(
        Vec2<units::length::meter_t>(units::length::meter_t(0.5), units::length::meter_t(-1)));

    ASSERT_TRUE(solution.has_value());
    ASSERT_NEAR(solution.value().x(), 10.423149, kAbsErrorMax);
    EXPECT_LT(solution.value().y(), 0);
}

TEST(ParamPoly3Test,
     GivenSimpleParametricPolynomialRoad_WhenGetHeadingsAt_ThenCorrectHeadingAtFiftyMetersMustBeReturned)
{
    auto param_poly = ParamPoly3(units::length::meter_t(0),
                                 Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                                 units::angle::radian_t(0),
                                 units::length::meter_t(100),
                                 "polynomial_road",
                                 types::Poly3(0, 1, 0, 0),
                                 types::Poly3(1.5, 1, -3.5, 1),
                                 false);

    const auto result = param_poly.GetHeadingAt(units::length::meter_t(50));

    ASSERT_NEAR(result.value(), 1.570656, kAbsErrorMax);
}

TEST(ParamPoly3Test,
     GivenSimpleParametricPolynomialRoad_WhenCalcDirectionAt_ThenCorrectDirectionAtFiftyMetersMustBeReturned)
{
    auto param_poly = ParamPoly3(units::length::meter_t(0),
                                 Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                                 units::angle::radian_t(0),
                                 units::length::meter_t(100),
                                 "polynomial_road",
                                 types::Poly3(0, 1, 0, 0),
                                 types::Poly3(1.5, 1, -3.5, 1),
                                 false);

    const auto result = param_poly.CalcDirectionAt(units::length::meter_t(50));

    ASSERT_NEAR(result.x.value(), 0.0001398, kAbsErrorMax);
    ASSERT_NEAR(result.y.value(), 1.0, kAbsErrorMax);
}

}  // namespace road_logic_suite
