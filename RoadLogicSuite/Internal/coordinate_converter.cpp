/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/coordinate_converter.h"

#include "geometry_lookup_rtree.h"

namespace road_logic_suite
{

// public methods

CoordinateConverter::CoordinateConverter(const OpenDriveData& data_storage)
    : data_storage_(data_storage), geometry_lookup_(std::make_unique<GeometryLookupRTree>(data_storage.geometries))
{
}

void CoordinateConverter::OptimizeStorage()
{
    geometry_lookup_->OptimizeStorage();
}

std::optional<mantle_api::OpenDriveRoadPosition> CoordinateConverter::ConvertInertialToRoadPositionCoordinates(
    const mantle_api::Vec3<units::length::meter_t>& query) const
{
    std::optional<mantle_api::OpenDriveRoadPosition> result = std::nullopt;
    auto minimum_t = units::length::meter_t(std::numeric_limits<double>::max());

    std::function<bool(const IGeometry*)> search_callback = [&](const IGeometry* geom) {
        auto closest_point = geom->ConvertInertialCoordinatesToST(Vec2(query));
        if (!closest_point)
        {
            return true;  // return `true` to keep going, return `false` to stop
        }

        auto [s, t] = closest_point.value();

        if (s - geom->s > geom->length)
        {
            // No solution is desired if the value is greater than length of the geometry.
            return true;
        }

        if (units::math::abs(t) < minimum_t)
        {
            minimum_t = units::math::abs(t);
            result = mantle_api::OpenDriveRoadPosition{.road = geom->road_id, .s_offset = s, .t_offset = t};
        }

        return true;  // return `true` to keep going, return `false` to stop
    };

    geometry_lookup_->VisitGeometry(Vec2(query), search_callback);
    return result;
}

std::optional<mantle_api::Vec3<units::length::meter_t>> CoordinateConverter::ConvertRoadPositionToInertialCoordinates(
    const mantle_api::OpenDriveRoadPosition& query) const
{
    auto it = data_storage_.roads.find(query.road);
    if (it == data_storage_.roads.end())
    {
        return std::nullopt;
    }
    auto geometry = GetGeometryAtS(it->second, query.s_offset());
    if (geometry == nullptr)
    {
        return std::nullopt;
    }

    auto elevation_h = units::length::meter_t(CalcAt(it->second.elevations, query.s_offset));
    auto theta = units::angle::radian_t(CalcAt(it->second.superelevations, query.s_offset));

    auto projected_t = query.t_offset;
    auto superelevation_h = units::length::meter_t(0.0);
    if (theta != units::angle::radian_t(0.0))
    {
        projected_t = units::math::cos(theta) * query.t_offset;
        superelevation_h = units::math::sin(theta) * query.t_offset;
    }

    auto ret_projected = geometry->ConvertSTCoordinatesToInertial(query.s_offset, projected_t);
    auto ret_z = elevation_h + superelevation_h;
    return mantle_api::Vec3<units::length::meter_t>(ret_projected.x, ret_projected.y, ret_z);
}

double CoordinateConverter::CalcAt(const types::RangeBasedMap<units::length::meter_t, types::Poly3>& polys,
                                   const units::length::meter_t& s)
{
    auto ret_val = 0.0;
    auto poly = polys.Find(s);
    if (poly != polys.end())
    {
        ret_val = poly->second.CalcAt(s.value());
    }
    return ret_val;
}

std::optional<mantle_api::OpenDriveLanePosition> CoordinateConverter::ConvertInertialToLaneCoordinates(
    const mantle_api::Vec3<units::length::meter_t>& query) const
{
    auto road_position = ConvertInertialToRoadPositionCoordinates(query);
    if (!road_position.has_value())
    {
        return std::nullopt;
    }

    auto it = data_storage_.roads.find(road_position->road);
    if (it == data_storage_.roads.end())
    {
        return std::nullopt;
    }

    auto lane_section = it->second.lane_sections.At(road_position->s_offset);
    auto& lanes =
        road_position->t_offset >= units::length::meter_t(0) ? lane_section.left_lanes : lane_section.right_lanes;

    auto t_road_position_abs = units::math::abs(road_position->t_offset);
    auto lane_width_last_element = units::length::meter_t(0.);
    auto lane_width_sum = units::length::meter_t(0.);
    std::optional<types::Lane> found_lane = std::nullopt;
    for (auto const& lane : lanes)
    {
        lane_width_last_element = lane.GetWidthAt(road_position->s_offset);
        lane_width_sum += lane_width_last_element;

        if (lane_width_sum >= t_road_position_abs)
        {
            found_lane = lane;
            break;
        }
    }

    if (!found_lane)
    {
        return std::nullopt;
    }

    auto lane_middle_offset = lane_width_sum - lane_width_last_element / 2.;
    auto lane_middle_offset_signed =
        road_position->t_offset >= units::length::meter_t(0.) ? lane_middle_offset : -lane_middle_offset;

    auto t_lane_local = road_position->t_offset - lane_middle_offset_signed;

    return mantle_api::OpenDriveLanePosition{.road = road_position->road,
                                             .lane = found_lane->id,
                                             .s_offset = road_position->s_offset,
                                             .t_offset = t_lane_local};
}

std::optional<mantle_api::Vec3<units::length::meter_t>> CoordinateConverter::ConvertLaneToInertialCoordinates(
    const mantle_api::OpenDriveLanePosition& query) const
{
    auto it = data_storage_.roads.find(query.road);
    if (it == data_storage_.roads.end())
    {
        return std::nullopt;
    }

    auto lane_section = it->second.lane_sections.At(query.s_offset);
    auto lane_range = lane_section.GetLaneRangeFromReferenceLineToId(query.lane);
    if (lane_range.begin() == lane_range.end())
    {
        return std::nullopt;
    }

    auto last_element_width = units::length::meter_t(0.);
    auto lane_width_sum = units::length::meter_t(0.);
    for (auto const& lane : lane_range)
    {
        last_element_width = lane.GetWidthAt(query.s_offset);
        lane_width_sum += last_element_width;
    }

    auto lane_middle_offset = lane_width_sum - last_element_width / 2.;
    auto lane_middle_offset_signed = query.lane >= 0 ? lane_middle_offset : -lane_middle_offset;
    auto t_road_position = query.t_offset + lane_middle_offset_signed;

    return ConvertRoadPositionToInertialCoordinates(
        mantle_api::OpenDriveRoadPosition{.road = query.road, .s_offset = query.s_offset, .t_offset = t_road_position});
}

std::optional<units::angle::radian_t> CoordinateConverter::GetRoadPositionHeading(const std::string& road_id,
                                                                                  const units::length::meter_t& s) const
{
    auto it = data_storage_.roads.find(road_id);
    if (it == data_storage_.roads.end())
    {
        return std::nullopt;
    }
    auto geometry = GetGeometryAtS(it->second, s());
    if (geometry == nullptr)
    {
        return std::nullopt;
    }

    return geometry->GetHeadingAt(s);
}

IGeometry* CoordinateConverter::GetGeometryAtS(const Road& road, double s) const
{
    auto begin = std::next(data_storage_.geometries.begin(), road.index_geometries_start);
    auto end = std::next(data_storage_.geometries.begin(), road.index_geometries_end);
    auto geometry = std::find_if(begin, end, [s](const std::unique_ptr<IGeometry>& obj) {
        return obj->s.value() <= s && s <= (obj->s + obj->length).value();
    });

    return (geometry != end) ? geometry->get() : nullptr;
}

}  // namespace road_logic_suite
