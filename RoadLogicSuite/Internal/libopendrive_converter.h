/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LIBOPENDRIVE_CONVERTER_H
#define ROADLOGICSUITE_LIBOPENDRIVE_CONVERTER_H

#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <OpenDriveMap.h>

#include <map>

namespace road_logic_suite
{
/// @brief This class is responsible to fill the data storage with the parsed information from the OpenDRIVE map.
class LibOpenDriveConverter
{
  public:
    /// @brief Initializes the converter.
    /// @param data The data storage reference.
    explicit LibOpenDriveConverter(OpenDriveData& data) : data_storage_reference_(data) {}

    /// @brief This converts the given OpenDRIVE map into the internal data structure of the RoadLogicSuite.
    /// @note This method will fail when the data storage is already filled with information.
    /// @param map The given map.
    /// @return True, when conversion is successful, false otherwise.
    bool Convert(const odr::OpenDriveMap& map);

  private:
    OpenDriveData& data_storage_reference_;
    void AddToStorage(const odr::Road& xml_road,
                      std::vector<std::unique_ptr<IGeometry>>& geometries,
                      const types::RangeBasedMap<units::length::meter_t, types::LaneSection>& lane_sections,
                      const std::vector<RoadLink>& road_links,
                      const types::RangeBasedMap<units::length::meter_t, types::Poly3>& elevations,
                      const types::RangeBasedMap<units::length::meter_t, types::Poly3>& superelevations,
                      const types::RangeBasedMap<units::length::meter_t, types::Poly3>& laneoffsets);

    void AddJunctionToStorage(const odr::Junction& xml_junction,
                              const types::JunctionType& junction_type,
                              const std::vector<types::JunctionConnection>& junction_connections,
                              const std::vector<types::JunctionCrossPath>& junction_cross_paths,
                              const std::vector<types::JunctionController>& junction_controllers,
                              const std::vector<types::JunctionPriority>& junction_priorities,
                              const std::vector<types::JunctionRoadSection>& junction_road_sections);

    [[nodiscard]] static std::vector<std::unique_ptr<IGeometry>> ConvertGeometries(
        const std::set<const odr::RoadGeometry*>& xml_geometries,
        const std::string& road_id);
    [[nodiscard]] static types::RangeBasedMap<units::length::meter_t, types::LaneSection> ConvertLaneSections(
        const std::map<double, odr::LaneSection>& xml_lane_sections,
        units::length::meter_t road_length);
    [[nodiscard]] static types::RangeBasedMap<units::length::meter_t, types::Poly3> ConvertPoly3AlongSAxis(
        const std::map<double, odr::Poly3>& xml_poly3);

    static void ConvertLanes(const std::map<double, odr::LaneSection>& xml_lane_sections,
                             const std::map<double, odr::LaneSection>::const_reverse_iterator& xml_lane_section_it,
                             types::LaneSection& lane_section);

    [[nodiscard]] static types::JunctionType ConvertJunctionType(const odr::Junction& xml_junction);

    [[nodiscard]] static std::vector<types::JunctionConnection> ConvertJunctionConnections(
        const std::map<std::string, odr::JunctionConnection>& xml_junction_connections);

    [[nodiscard]] static std::vector<types::JunctionCrossPath> ConvertJunctionCrossPaths(
        const odr::Junction& xml_junction);

    [[nodiscard]] static std::vector<types::JunctionController> ConvertJunctionControllers(
        const std::map<std::string, odr::JunctionController>& xml_junction_controllers);

    [[nodiscard]] static std::vector<types::JunctionPriority> ConvertJunctionPriorities(
        const odr::Junction& xml_junction);

    [[nodiscard]] static std::vector<types::JunctionRoadSection> ConvertJunctionRoadSections(
        const odr::Junction& xml_junction);
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_LIBOPENDRIVE_CONVERTER_H
