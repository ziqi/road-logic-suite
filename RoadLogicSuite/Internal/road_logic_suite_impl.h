/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_LOGIC_SUITE_IMPL_H
#define ROADLOGICSUITE_ROAD_LOGIC_SUITE_IMPL_H

#include "RoadLogicSuite/Internal/MapValidation/validation_runner.h"
#include "RoadLogicSuite/Internal/coordinate_converter.h"
#include "RoadLogicSuite/Internal/i_map_converter.h"
#include "RoadLogicSuite/Internal/libopendrive_converter.h"
#include "RoadLogicSuite/map_converter_config.h"

#include <OpenDriveMap.h>

#include <memory>
#include <string>

namespace road_logic_suite
{
/// @brief This is the main class that can be used to access the RoadLogicSuite.
///
/// Usage Example:
/// @code{.cpp}
/// RoadLogicSuiteImpl suite;
/// suite.LoadFile("path/to/sample.xodr")
/// // do validations, conversions, etc.
/// @endcode
class RoadLogicSuiteImpl
{
  public:
    /// @brief Load a given map.
    /// @param xodr_file The path to the OpenDRIVE file.
    /// @return When loaded successfully: true.<br>
    ///         Otherwise: false.
    bool LoadFile(const std::string& xodr_file);

    /// @brief Get the coordinate converter.
    /// @return The coordinate converter as reference.
    const CoordinateConverter& GetCoordinateConverter();

    /// @brief Get the map validator.
    /// @return The map validation runner with default configuration.
    [[nodiscard]] map_validation::ValidationRunner GetMapValidator() const;

    template <typename TTargetMap>
    TTargetMap ConvertMap(IMapConverter<TTargetMap>& converter) const
    {
        return converter.Convert(data_storage_);
    }

  private:
    OpenDriveData data_storage_{};
    std::unique_ptr<CoordinateConverter> coordinate_converter_;
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_ROAD_LOGIC_SUITE_IMPL_H
