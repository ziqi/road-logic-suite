/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Utils/converter_utils.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/Types/lane_section.h"
#include "math_utils.h"

#include <gtest/gtest.h>
#include <units.h>

namespace
{
const double kAbsErrorMax = 1e-6;

road_logic_suite::types::LaneSection CreateSectionWithSingleRightLaneWithConstantWidth()
{
    road_logic_suite::types::LaneSection section{};
    road_logic_suite::types::Lane right_lane{};
    road_logic_suite::types::Poly3 constant_width(4, 0, 0, 0);
    units::length::meter_t s0{};
    right_lane.width_polys.Insert(s0, constant_width);
    right_lane.id = -1;
    section.right_lanes.push_back(right_lane);
    return section;
}

TEST(ConverterUtilsTest,
     GivenALaneSectionWithASingleRightLane_WhenGettingTheBoundaryOffsetForThatLane_ThenTheReturnedOffsetShouldBeCorrect)
{
    units::length::meter_t s{};
    const auto section = CreateSectionWithSingleRightLaneWithConstantWidth();
    const auto lane_id = -1;

    const auto ret = road_logic_suite::GetBoundaryTValueAt(s, section, lane_id);

    EXPECT_EQ(-4, ret());
}

TEST(ConverterUtilsTest,
     GivenALaneSectionWithASingleRightLane_WhenGettingTheLaneCenterOffset_ThenTheReturnedOffsetShouldBeCorrect)
{
    units::length::meter_t s{};
    const auto section = CreateSectionWithSingleRightLaneWithConstantWidth();
    const auto lane_id = -1;

    const auto ret = road_logic_suite::GetLaneCenterTValueAt(s, section, lane_id);

    EXPECT_EQ(-2, ret());
}

TEST(ConverterUtilsTest, GivenLineWithLessThanTwoPoints_WhenDownsampling_ThenSkipDownsamplingAndPrintWarning)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {
        {units::length::meter_t(0.0), units::length::meter_t(0.0), units::length::meter_t(0.0)}};

    testing::internal::CaptureStdout();

    const auto expected_warning_output =
        "[RoadLogicSuite] WARN: Cannot simplify a polyline with less than two points. Downsampling is skipped!\n";

    const auto polyline_after_downsampling = road_logic_suite::DecimatePolyline(polyline.begin(), polyline.end(), 0.0);

    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_warning_output);
    EXPECT_EQ(polyline_after_downsampling, polyline);
}

TEST(ConverterUtilsTest, GivenLineWithTwoPoints_WhenDownsampling_ThenNoPointsAreRemoved)
{
    const double downsampling_epsilon = 0.0;
    const mantle_api::Vec3<units::length::meter_t> first_point{
        units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> last_point{
        units::length::meter_t(4), units::length::meter_t(0), units::length::meter_t(0)};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {first_point, last_point};

    const std::vector<mantle_api::Vec3<units::length::meter_t>> downsampled_polyline =
        road_logic_suite::DecimatePolyline(polyline.begin(), polyline.end(), downsampling_epsilon);

    ASSERT_EQ(downsampled_polyline.size(), 2);
    EXPECT_EQ(downsampled_polyline.front(), first_point);
    EXPECT_EQ(downsampled_polyline.back(), last_point);
}

TEST(ConverterUtilsTest,
     GivenLineWithThreeNonCollinearPoints_WhenDownsamplingWithEpsilonGreaterThanOffset_ThenOffsetPointIsRemoved)
{
    const double middle_point_offset = 2.0;
    const double downsampling_epsilon = middle_point_offset + 1.0;
    const mantle_api::Vec3<units::length::meter_t> first_point{
        units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> middle_point{
        units::length::meter_t(2), units::length::meter_t(middle_point_offset), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> last_point{
        units::length::meter_t(4), units::length::meter_t(0), units::length::meter_t(0)};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {first_point, middle_point, last_point};

    const std::vector<mantle_api::Vec3<units::length::meter_t>> downsampled_polyline =
        road_logic_suite::DecimatePolyline(polyline.begin(), polyline.end(), downsampling_epsilon);

    ASSERT_EQ(downsampled_polyline.size(), 2);
    EXPECT_EQ(downsampled_polyline.front(), first_point);
    EXPECT_EQ(downsampled_polyline.back(), last_point);
}

TEST(ConverterUtilsTest,
     GivenLineWithThreeNonCollinearPoints_WhenDownsamplingWithEpsilonLessThanOffset_ThenOffsetPointIsNotRemoved)
{
    const units::length::meter_t middle_point_offset{2.0};
    const double downsampling_epsilon{middle_point_offset() - 1.0};
    const mantle_api::Vec3<units::length::meter_t> first_point{
        units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> middle_point{
        units::length::meter_t(2), middle_point_offset, units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> last_point{
        units::length::meter_t(4), units::length::meter_t(0), units::length::meter_t(0)};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {first_point, middle_point, last_point};

    const std::vector<mantle_api::Vec3<units::length::meter_t>> downsampled_polyline =
        road_logic_suite::DecimatePolyline(polyline.begin(), polyline.end(), downsampling_epsilon);

    ASSERT_EQ(downsampled_polyline.size(), 3);
    EXPECT_EQ(downsampled_polyline.front(), first_point);
    EXPECT_EQ(downsampled_polyline.at(1), middle_point);
    EXPECT_EQ(downsampled_polyline.back(), last_point);
}

TEST(ConverterUtilsTest, GivenManyPointsWithinOffset_WhenDownsampling_ThenLineRemainsUnchanged)
{
    const units::length::meter_t offset{2.0};
    const double downsampling_epsilon{0.5};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {
        {units::length::meter_t(0), offset, units::length::meter_t(0)},
        {units::length::meter_t(1), offset, units::length::meter_t(0)},
        {units::length::meter_t(2), -offset, units::length::meter_t(0)},
        {units::length::meter_t(3), -offset, units::length::meter_t(0)},
        {units::length::meter_t(4), offset, units::length::meter_t(0)},
        {units::length::meter_t(5), -offset, units::length::meter_t(0)},
        {units::length::meter_t(6), offset, units::length::meter_t(0)}};

    const std::vector<mantle_api::Vec3<units::length::meter_t>> downsampled_polyline =
        road_logic_suite::DecimatePolyline(polyline.begin(), polyline.end(), downsampling_epsilon);

    ASSERT_EQ(downsampled_polyline.size(), polyline.size());
    for (std::size_t i = 0; i < polyline.size(); i++)
    {
        EXPECT_EQ(downsampled_polyline.at(i), polyline.at(i));
    }
}

TEST(ConverterUtilsTest, GivenLineWithThreeConsecutiveCollinearPoints_WhenDownsampling_ThenSinglePointIsRemoved)
{
    const units::length::meter_t offset{2.0};
    const double downsampling_epsilon = 0.5;
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {
        {units::length::meter_t(0), offset, units::length::meter_t(0)},
        {units::length::meter_t(1), offset, units::length::meter_t(0)},
        {units::length::meter_t(2), offset, units::length::meter_t(0)},
        {units::length::meter_t(3), -offset, units::length::meter_t(0)},
        {units::length::meter_t(4), offset, units::length::meter_t(0)},
        {units::length::meter_t(5), -offset, units::length::meter_t(0)},
        {units::length::meter_t(6), offset, units::length::meter_t(0)}};

    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_polyline = {
        {units::length::meter_t(0), offset, units::length::meter_t(0)},
        {units::length::meter_t(2), offset, units::length::meter_t(0)},
        {units::length::meter_t(3), -offset, units::length::meter_t(0)},
        {units::length::meter_t(4), offset, units::length::meter_t(0)},
        {units::length::meter_t(5), -offset, units::length::meter_t(0)},
        {units::length::meter_t(6), offset, units::length::meter_t(0)}};

    const std::vector<mantle_api::Vec3<units::length::meter_t>> downsampled_polyline =
        road_logic_suite::DecimatePolyline(polyline.begin(), polyline.end(), downsampling_epsilon);

    ASSERT_EQ(downsampled_polyline.size(), expected_polyline.size());
    for (std::size_t i = 0; i < expected_polyline.size(); i++)
    {
        EXPECT_EQ(downsampled_polyline.at(i), expected_polyline.at(i));
    }
}

template <typename T>
void TestPerpendicularDistance(const T& point, const T& line_start, const T& line_end, double exp)
{
    double res = road_logic_suite::PerpendicularDistance(point, line_start, line_end);
    EXPECT_EQ(res, exp);
}

TEST(ConverterUtilsTest, GivenLineAndPoints_WhenGettingPerpendicularDistance_ThenDistanceShouldBeCorrect)
{
    // Define sample points
    const mantle_api::Vec3<units::length::meter_t> point1{
        units::length::meter_t(-1), units::length::meter_t(2), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> point2{
        units::length::meter_t(2), units::length::meter_t(0), units::length::meter_t(-1)};
    const mantle_api::Vec3<units::length::meter_t> point3{
        units::length::meter_t(5), units::length::meter_t(1), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> point4{
        units::length::meter_t(10), units::length::meter_t(-3), units::length::meter_t(0)};

    // Define horizontal line
    const mantle_api::Vec3<units::length::meter_t> first_point_h{
        units::length::meter_t(2), units::length::meter_t(1), units::length::meter_t(0)};
    const mantle_api::Vec3<units::length::meter_t> last_point_h{
        units::length::meter_t(6), units::length::meter_t(1), units::length::meter_t(0)};

    // Define vertical line
    const mantle_api::Vec3<units::length::meter_t> first_point_v{
        units::length::meter_t(2), units::length::meter_t(-3), units::length::meter_t(1)};
    const mantle_api::Vec3<units::length::meter_t> last_point_v{
        units::length::meter_t(2), units::length::meter_t(10), units::length::meter_t(0)};

    // Calculate and check perpendicular distances.
    // Hint: The function PerpendicularDistance only considers the x-y values.
    // Test points which lie inside and also outside the x-y range of the lines.
    TestPerpendicularDistance(point1, first_point_h, last_point_h, 1);
    TestPerpendicularDistance(point2, first_point_h, last_point_h, 1);
    TestPerpendicularDistance(point3, first_point_h, last_point_h, 0);
    TestPerpendicularDistance(point4, first_point_h, last_point_h, 4);

    TestPerpendicularDistance(point1, first_point_v, last_point_v, 3);
    TestPerpendicularDistance(point2, first_point_v, last_point_v, 0);
    TestPerpendicularDistance(point3, first_point_v, last_point_v, 3);
    TestPerpendicularDistance(point4, first_point_v, last_point_v, 8);
}

TEST(ConverterUtilsTest, GivenLineAndPoints_WhenFindPointWithMaxDistanceToLine_ThenDistanceShouldBeCorrect)
{
    // Hint: The function FindPointWithMaxDistanceToLine only considers the x-y values.
    // In case if more than one point have the same max distance then the first finding will be considered.
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline = {
        {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)},
        {units::length::meter_t(1), units::length::meter_t(1), units::length::meter_t(0)},
        {units::length::meter_t(2), units::length::meter_t(-2), units::length::meter_t(0)},
        {units::length::meter_t(3), units::length::meter_t(-1), units::length::meter_t(0)},
        {units::length::meter_t(4), units::length::meter_t(3), units::length::meter_t(0)},
        {units::length::meter_t(5), units::length::meter_t(3), units::length::meter_t(0)},
        {units::length::meter_t(6), units::length::meter_t(0), units::length::meter_t(0)}};

    const auto [max_distance, max_distance_index] =
        road_logic_suite::FindPointWithMaxDistanceToLine(polyline.begin(), polyline.end());

    EXPECT_EQ(max_distance, 3);
    EXPECT_EQ(max_distance_index, std::next(polyline.begin(), 4));
}

TEST(ConverterUtilsTest, Given3dPosition_WhenGetPosition_Then2dPositionShouldBeCorrect)
{
    const mantle_api::Vec3<units::length::meter_t> position3d{
        units::length::meter_t(-1), units::length::meter_t(2), units::length::meter_t(0)};

    road_logic_suite::Vec2<units::length::meter_t> postion2d = road_logic_suite::GetPosition(position3d);
    EXPECT_EQ(postion2d.x, position3d.x);
    EXPECT_EQ(postion2d.y, position3d.y);
}

TEST(ConverterUtilsTest, GivenBoundaryPoint_WhenGetPosition_Then2dPositionShouldBeCorrect)
{
    const mantle_api::Vec3<units::length::meter_t> position3d{
        units::length::meter_t(-1), units::length::meter_t(2), units::length::meter_t(0)};

    const map_api::LaneBoundary::BoundaryPoint bp{.position = position3d};

    road_logic_suite::Vec2<units::length::meter_t> postion2d = road_logic_suite::GetPosition(bp);
    EXPECT_EQ(postion2d.x, position3d.x);
    EXPECT_EQ(postion2d.y, position3d.y);
}

TEST(ConverterUtilsTest, GivenTwoPoints_WhenGetDirection_ThenDirectionShouldBeCorrect)
{
    const road_logic_suite::Vec2<units::length::meter_t> start{units::length::meter_t(1), units::length::meter_t(1)};
    const road_logic_suite::Vec2<units::length::meter_t> target{units::length::meter_t(2), units::length::meter_t(2)};

    auto direction = road_logic_suite::GetDirection(start, target);

    // Direction should be normalized
    EXPECT_NEAR(direction.Length().value(), 1, kAbsErrorMax);
    EXPECT_NEAR(direction.x.value(), road_logic_suite::math_utils::kSqrt1_2.value(), kAbsErrorMax);
    EXPECT_NEAR(direction.y.value(), road_logic_suite::math_utils::kSqrt1_2.value(), kAbsErrorMax);
}
}  // namespace