/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_CONVERTER_UTILS_H
#define ROADLOGICSUITE_CONVERTER_UTILS_H

#include "RoadLogicSuite/Internal/Types/lane_section.h"
#include "RoadLogicSuite/Internal/coordinate_converter.h"

#include <MantleAPI/Common/vector.h>
#include <MapAPI/lane_boundary.h>
#include <units.h>

namespace road_logic_suite::utils
{
std::string ToLower(const std::string& str);

}  // namespace road_logic_suite::utils

namespace road_logic_suite
{
/// @brief Convert a 3d to a 2d position by only considering the x and y values.
/// @param point The point to be converted.
/// @return The position as Vec2.
static inline road_logic_suite::Vec2<units::length::meter_t> GetPosition(
    const mantle_api::Vec3<units::length::meter_t>& point)
{
    return {point.x, point.y};
}

/// @brief Get the 2d position of a boundary point
/// @param point The boundary point to be converted.
/// @return The position as Vec2.
static inline road_logic_suite::Vec2<units::length::meter_t> GetPosition(
    const map_api::LaneBoundary::BoundaryPoint& point)
{
    return GetPosition(point.position);
}

/// @brief Get the 2d position of a boundary point
/// @param start A point on the direction vector.
/// @param target A point with an offset to the start position.
/// @return The normalized direction vector as Vec2.
static inline road_logic_suite::Vec2<units::length::meter_t> GetDirection(
    const road_logic_suite::Vec2<units::length::meter_t>& start,
    const road_logic_suite::Vec2<units::length::meter_t>& target)
{
    const auto diff = target - start;
    return diff / diff.Length().value();
}

/// @brief Get the perpendicular distance from a \p point to a line defined by \p line_start and \p line_end.
/// @note This function only considers the x-y coordinates of the input values.
/// @param point A point next to the line.
/// @param line_start Start of the line.
/// @param line_end End of the line.
/// @return The perpendicular distance.
template <typename T>
double PerpendicularDistance(const T& point, const T& line_start, const T& line_end)
{
    const auto point_2d = GetPosition(point);
    const auto line_start_2d = GetPosition(line_start);
    const auto line_end_2d = GetPosition(line_end);
    const auto line_direction = GetDirection(line_start_2d, line_end_2d);
    const auto vector_to_point = point_2d - line_start_2d;
    const double lambda = (line_direction.x * vector_to_point.x + line_direction.y * vector_to_point.y) /
                          (line_direction.x * line_direction.x + line_direction.y * line_direction.y);
    const auto point_projected_on_line = line_direction * lambda + line_start_2d;
    const auto point_to_projected = point_projected_on_line - point_2d;
    return point_to_projected.Length().value();
}

/// @brief Find the point of a polyline which has the max distance to a line defined by \p begin and \p end.
/// @note This function only considers the x-y coordinates of the input values.
/// @note In case if more than one point have the same max distance then the first finding will be considered.
/// @param begin Start point of the polyline (e.g. pointer to the first entry of a vector).
/// @param end End point of the polyline (e.g. pointer to the last entry of a vector).
/// @return The max distance.
/// @return The point which has the max distance.
template <typename T>
std::pair<double, T> FindPointWithMaxDistanceToLine(const T begin, const T end)
{
    double max_distance = 0.0;
    T max_distance_index = begin;
    const auto last_point = end - 1;
    for (auto it = std::next(begin, 1); it < end; ++it)
    {
        const double distance = PerpendicularDistance(*it, *begin, *last_point);
        if (distance > max_distance)
        {
            max_distance_index = it;
            max_distance = distance;
        }
    }
    return {max_distance, max_distance_index};
}

/// @brief Get the t value of the boundary from lane @p lane_id at a certain @p s position of the lane section.
/// @param s Query point along the road. s=0 is at the start of the lane section.
/// @param section The lane section that is currently queried. A lane with the ID @p lane_id must exist.
/// @param lane_id A @p lane_id according to OpenSCENARIO specification.
/// @return The distance t from the reference line to the boundary of the lane @p lane_id.
units::length::meter_t GetBoundaryTValueAt(const units::length::meter_t& s,
                                           const road_logic_suite::types::LaneSection& section,
                                           const road_logic_suite::types::Lane::LaneId& lane_id);

/// @brief Get the t value of the center line from lane @p lane_id at a certain @p s position of the lane section.
/// @param s Query point along the road. s=0 is at the start of the lane section.
/// @param section The lane section that is currently queried. A lane with the ID @p lane_id must exist.
/// @param lane_id A @p lane_id according to OpenSCENARIO specification.
/// @return The distance t from the reference line to the center of the lane @p lane_id.
units::length::meter_t GetLaneCenterTValueAt(const units::length::meter_t& s,
                                             const road_logic_suite::types::LaneSection& section,
                                             const road_logic_suite::types::Lane::LaneId& lane_id);

/// @brief Simplifies a polyline by applying the Ramer–Douglas–Peucker algorithm to the given range.
/// References:
/// - https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
/// @param begin Start of the to be simplified range (e.g. pointer to the first entry of a vector).
/// @param end End of the to be simplified range (e.g. pointer to the last entry of a vector).
/// @param epsilon Value used to define if the points in between (begin, end) should be discarded. It represents the
/// distance of the farthest possible point within the range (begin, end) from the original line.
template <typename T, typename ReturnType = std::vector<typename std::iterator_traits<T>::value_type>>
ReturnType DecimatePolyline(T begin, T end, const double epsilon)  // NOLINT(misc-no-recursion)
{
    if (std::distance(begin, end) < 2)
    {
        std::cout << "[RoadLogicSuite] WARN: Cannot simplify a polyline with less than two points. "
                  << "Downsampling is skipped!" << std::endl;
        return ReturnType(begin, end);
    }

    const auto [max_distance, max_distance_index] = FindPointWithMaxDistanceToLine(begin, end);

    ReturnType result;
    if (max_distance > epsilon)
    {
        const ReturnType result_1 = DecimatePolyline(begin, max_distance_index + 1, epsilon);
        const ReturnType result_2 = DecimatePolyline(max_distance_index, end, epsilon);

        result.assign(result_1.begin(), result_1.end() - 1);
        result.insert(result.end(), result_2.begin(), result_2.end());
    }
    else
    {
        result.push_back(*begin);
        result.push_back(*(end - 1));
    }

    if (result.size() < 2)
    {
        throw std::runtime_error("Polyline simplification failed. Please contact Ansys support.");
    }
    return result;
}
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_CONVERTER_UTILS_H
