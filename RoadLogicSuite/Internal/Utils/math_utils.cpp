/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Utils/math_utils.h"

namespace road_logic_suite::math_utils
{
std::vector<double> LinSpace(double start, double end, int num_entries)
{
    std::vector<double> linspaced{};
    linspaced.reserve(num_entries);
    double delta = (end - start) / (num_entries - 1);

    for (int i = 0; i < num_entries - 1; ++i)
    {
        linspaced.emplace_back(start + delta * i);
    }
    linspaced.emplace_back(end);

    return linspaced;
}

std::vector<Vec2<units::length::meter_t>> CreateRectangularShape(double length, double width)
{
    return {
        {units::length::meter_t(length / 2), units::length::meter_t(width / 2)},
        {units::length::meter_t(-length / 2), units::length::meter_t(width / 2)},
        {units::length::meter_t(-length / 2), units::length::meter_t(-width / 2)},
        {units::length::meter_t(length / 2), units::length::meter_t(-width / 2)},
    };
}

std::vector<Vec2<units::length::meter_t>> CreateCircularShape(double radius)
{
    std::vector<Vec2<units::length::meter_t>> shape{};
    const units::length::meter_t radius_units(radius);
    for (units::angle::radian_t angle{0}; angle < 2 * math_utils::kPi; angle += math_utils::kPiOver4)
    {
        shape.emplace_back(radius_units * units::math::cos(angle), radius_units * units::math::sin(angle));
    }
    return shape;
}

}  // namespace road_logic_suite::math_utils