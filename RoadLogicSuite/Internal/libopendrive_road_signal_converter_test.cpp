/*******************************************************************************
 * Copyright (C) 2024-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/libopendrive_road_signal_converter.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"
#include "RoadLogicSuite/Tests/Utils/assertions.h"

#include <gmock/gmock.h>

namespace
{

constexpr double kSampleRoadLength = 100;
constexpr double kSampleRoadOriginPointX = 0.0;
constexpr double kSampleRoadOriginPointY = 0.0;
constexpr double kSampleRoadHeading = 1.5;
constexpr double kSignalHeading = 0.1;
constexpr double kSampleSingleSignalSOffset = 50;
constexpr double kSampleSingleSignalTOffset = -4;

road_logic_suite::OpenDriveData CreateDataStorageWithSimpleStraightRoad()
{
    road_logic_suite::OpenDriveData data{};
    road_logic_suite::Road road{.id = "0",
                                .length = units::length::meter_t(kSampleRoadLength),
                                .index_geometries_start = 0,
                                .index_geometries_end = 1};
    data.roads.insert_or_assign("0", road);
    data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
        units::length::meter_t(0),
        road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(kSampleRoadOriginPointX),
                                                       units::length::meter_t(kSampleRoadOriginPointY)),
        units::angle::radian_t(kSampleRoadHeading),
        units::length::meter_t(kSampleRoadLength),
        "0"));
    return data;
}

odr::Road CreateSignalsInRoad(pugi::xml_document& doc)
{
    odr::Road road("0", kSampleRoadLength, "-1", "TestRoad");

    road.xml_node = doc.append_child("road");

    auto signals_node = road.xml_node.append_child("signals");
    auto signal_node_1 = signals_node.append_child("signal");
    auto signal_node_2 = signals_node.append_child("signal");

    // create attributes for dummy signal node 1.
    signal_node_1.append_attribute("name").set_value("SpeedLimit60");
    signal_node_1.append_attribute("id").set_value("1");
    signal_node_1.append_attribute("s").set_value(kSampleSingleSignalSOffset);
    signal_node_1.append_attribute("t").set_value(kSampleSingleSignalTOffset);
    signal_node_1.append_attribute("dynamic").set_value("no");
    signal_node_1.append_attribute("type").set_value("274");
    signal_node_1.append_attribute("subtype").set_value("56");
    signal_node_1.append_attribute("country").set_value("DE");
    signal_node_1.append_attribute("countryRevision").set_value("2013");
    signal_node_1.append_attribute("orientation").set_value("+");
    signal_node_1.append_attribute("zOffset").set_value("1.90");
    signal_node_1.append_attribute("value").set_value("60.0");
    signal_node_1.append_attribute("unit").set_value("km/h");
    signal_node_1.append_attribute("hOffset").set_value(kSignalHeading);
    signal_node_1.append_attribute("pitch").set_value("0.0");
    signal_node_1.append_attribute("roll").set_value("0.0");
    signal_node_1.append_attribute("height").set_value("0.61");
    signal_node_1.append_attribute("width").set_value("0.61");
    signal_node_1.append_child("validity").append_attribute("fromLane").set_value("-2");
    signal_node_1.child("validity").append_attribute("toLane").set_value("1");
    signal_node_1.append_child("dependency").append_attribute("id").set_value("2");

    // create attributes for dummy signal node 2.
    signal_node_2.append_attribute("name").set_value("LorriesOnly");
    signal_node_2.append_attribute("id").set_value("2");
    signal_node_2.append_attribute("s").set_value(kSampleSingleSignalSOffset);
    signal_node_2.append_attribute("t").set_value(kSampleSingleSignalTOffset);
    signal_node_2.append_attribute("dynamic").set_value("no");
    signal_node_2.append_attribute("type").set_value("1048");
    signal_node_2.append_attribute("subtype").set_value("12");
    signal_node_2.append_attribute("country").set_value("DE");
    signal_node_2.append_attribute("countryRevision").set_value("2013");
    signal_node_2.append_attribute("orientation").set_value("+");
    signal_node_2.append_attribute("zOffset").set_value("1.56");
    signal_node_2.append_attribute("hOffset").set_value(kSignalHeading);
    signal_node_2.append_attribute("pitch").set_value("0.0");
    signal_node_2.append_attribute("roll").set_value("0.0");
    signal_node_2.append_attribute("height").set_value("0.33");
    signal_node_2.append_attribute("width").set_value("0.60");

    return road;
}

TEST(LibOpenDriveRoadSignalConverterTest, GivenMapWithSimpleSignal_WhenConverting_ThenSignalMustBeCorrectlyStored)
{
    road_logic_suite::OpenDriveData data = CreateDataStorageWithSimpleStraightRoad();

    pugi::xml_document doc;
    const auto xml_road = CreateSignalsInRoad(doc);

    road_logic_suite::LibOpenDriveRoadSignalConverter::ConvertForRoad(data, xml_road);

    auto it_1 = data.signals.find("1");
    ASSERT_TRUE(it_1 != data.signals.end()) << "The signal was not found in data storage!";
    const auto signal_1 = it_1->second;

    auto it_2 = data.signals.find("2");
    ASSERT_TRUE(it_2 != data.signals.end()) << "The signal was not found in data storage!";
    const auto signal_2 = it_2->second;

    const mantle_api::Vec3<units::length::meter_t> expected_position_signal_1(
        units::length::meter_t(kSampleRoadOriginPointX + kSampleSingleSignalSOffset * std::cos(kSampleRoadHeading) -
                               kSampleSingleSignalTOffset * std::sin(kSampleRoadHeading)),
        units::length::meter_t(kSampleRoadOriginPointY + kSampleSingleSignalTOffset * std::cos(kSampleRoadHeading) +
                               kSampleSingleSignalSOffset * std::sin(kSampleRoadHeading)),
        units::length::meter_t(1.90));

    const mantle_api::Vec3<units::length::meter_t> expected_position_signal_2(
        units::length::meter_t(kSampleRoadOriginPointX + kSampleSingleSignalSOffset * std::cos(kSampleRoadHeading) -
                               kSampleSingleSignalTOffset * std::sin(kSampleRoadHeading)),
        units::length::meter_t(kSampleRoadOriginPointY + kSampleSingleSignalTOffset * std::cos(kSampleRoadHeading) +
                               kSampleSingleSignalSOffset * std::sin(kSampleRoadHeading)),
        units::length::meter_t(1.56));

    const auto expected_signal_1_dynamic = false;
    const auto expected_signal_2_dynamic = false;
    const auto expected_signal_1_type = "274";
    const auto expected_signal_2_type = "1048";
    const auto expected_signal_1_subtype = "56";
    const auto expected_signal_2_subtype = "12";
    const auto expected_signal_1_country = "DE";
    const auto expected_signal_1_country_revision = "2013";
    const auto expected_signal_1_width = units::length::meter_t(0.61);
    const auto expected_signal_1_height = units::length::meter_t(0.61);
    const auto expected_signal_2_width = units::length::meter_t(0.60);
    const auto expected_signal_2_height = units::length::meter_t(0.33);
    const auto expected_signal_1_value = 60.0;
    const auto expected_signal_1_unit = road_logic_suite::SignalUnit::kKilometerPerHour;
    const auto expected_signal_2_unit = road_logic_suite::SignalUnit::kNoUnit;
    const auto expected_signal_1_yaw = units::angle::radian_t(kSampleRoadHeading + M_PI + kSignalHeading);
    const auto expected_signal_1_pitch = units::angle::radian_t(0.0);
    const auto expected_signal_1_roll = units::angle::radian_t(0.0);
    const auto expected_signal_2_yaw = units::angle::radian_t(kSampleRoadHeading + M_PI + kSignalHeading);
    const auto expected_signal_2_pitch = units::angle::radian_t(0.0);
    const auto expected_signal_2_roll = units::angle::radian_t(0.0);

    ASSERT_THAT(signal_1.position, road_logic_suite::test::InertialPositionIsNear(expected_position_signal_1));
    ASSERT_THAT(signal_2.position, road_logic_suite::test::InertialPositionIsNear(expected_position_signal_2));

    EXPECT_EQ(expected_signal_1_dynamic, signal_1.is_dynamic);
    EXPECT_EQ(expected_signal_2_dynamic, signal_2.is_dynamic);
    EXPECT_EQ(expected_signal_1_type, signal_1.type);
    EXPECT_EQ(expected_signal_2_type, signal_2.type);
    EXPECT_EQ(expected_signal_1_subtype, signal_1.subtype);
    EXPECT_EQ(expected_signal_2_subtype, signal_2.subtype);
    EXPECT_EQ(expected_signal_1_country, signal_1.country);
    EXPECT_EQ(expected_signal_1_country_revision, signal_1.country_revision);
    EXPECT_EQ(expected_signal_1_width, signal_1.width);
    EXPECT_EQ(expected_signal_1_height, signal_1.height);
    EXPECT_EQ(expected_signal_2_width, signal_2.width);
    EXPECT_EQ(expected_signal_2_height, signal_2.height);
    EXPECT_EQ(expected_signal_1_value, signal_1.value);
    EXPECT_EQ(expected_signal_1_unit, signal_1.unit);
    EXPECT_EQ(expected_signal_2_unit, signal_2.unit);
    EXPECT_EQ(expected_signal_1_yaw, signal_1.yaw);
    EXPECT_EQ(expected_signal_1_roll, signal_1.roll);
    EXPECT_EQ(expected_signal_1_pitch, signal_1.pitch);
    EXPECT_EQ(expected_signal_2_yaw, signal_2.yaw);
    EXPECT_EQ(expected_signal_2_roll, signal_2.roll);
    EXPECT_EQ(expected_signal_2_pitch, signal_2.pitch);

    EXPECT_THAT(signal_1.valid_lanes, ::testing::ElementsAre(-2, -1, 1));
    EXPECT_THAT(signal_1.dependencies, ::testing::ElementsAre("2"));
}

class UnitsConversionTest : public ::testing::TestWithParam<std::pair<std::string, road_logic_suite::SignalUnit>>
{
  protected:
    UnitsConversionTest() : road("0", kSampleRoadLength, "-1", "TestRoad") {}
    void SetUp() override
    {
        data = CreateDataStorageWithSimpleStraightRoad();
        road.xml_node = doc.append_child("road");
        signals_node = road.xml_node.append_child("signals");
        signal_node = signals_node.append_child("signal");
        signal_node.append_attribute("id").set_value("1");
    }

    road_logic_suite::OpenDriveData data;
    pugi::xml_document doc;
    odr::Road road;
    pugi::xml_node signals_node;
    pugi::xml_node signal_node;
};

TEST_P(UnitsConversionTest, GivenSignalWithDifferentUnits_WhenConverting_ThenSignalUnitMustBeCorrectlyStored)
{
    auto test_case = GetParam();
    signal_node.append_attribute("unit").set_value(std::get<0>(test_case).c_str());
    road_logic_suite::LibOpenDriveRoadSignalConverter::ConvertForRoad(data, road);
    auto it = data.signals.find("1");
    ASSERT_TRUE(it != data.signals.end()) << "The signal was not found in data storage!";
    const auto signal = it->second;
    EXPECT_EQ(signal.unit, std::get<1>(test_case));
}

const std::vector<std::pair<std::string, road_logic_suite::SignalUnit>> units_test_cases{
    {"ft", road_logic_suite::SignalUnit::kFeet},
    {"km", road_logic_suite::SignalUnit::kKilometer},
    {"m", road_logic_suite::SignalUnit::kMeter},
    {"mile", road_logic_suite::SignalUnit::kMile},
    {"%", road_logic_suite::SignalUnit::kPercentage},
    {"km/h", road_logic_suite::SignalUnit::kKilometerPerHour},
    {"m/s", road_logic_suite::SignalUnit::kMeterPerSecond},
    {"mph", road_logic_suite::SignalUnit::kMilePerHour},
    {"kg", road_logic_suite::SignalUnit::kKilo},
    {"t", road_logic_suite::SignalUnit::kMetricTon},
    {"", road_logic_suite::SignalUnit::kNoUnit},
    {"FT", road_logic_suite::SignalUnit::kUnknown},
    {"Km", road_logic_suite::SignalUnit::kUnknown},
    {"MILE", road_logic_suite::SignalUnit::kUnknown}};

INSTANTIATE_TEST_SUITE_P(LibOpenDriveRoadSignalConverterTest,
                         UnitsConversionTest,
                         ::testing::ValuesIn(units_test_cases));

class OrientationConversionTest
    : public ::testing::TestWithParam<std::pair<std::string, road_logic_suite::SignalOrientation>>
{
  protected:
    OrientationConversionTest() : road("0", kSampleRoadLength, "-1", "TestRoad") {}
    void SetUp() override
    {
        data = CreateDataStorageWithSimpleStraightRoad();
        road.xml_node = doc.append_child("road");
        signals_node = road.xml_node.append_child("signals");
        signal_node = signals_node.append_child("signal");
        signal_node.append_attribute("id").set_value("1");
    }

    road_logic_suite::OpenDriveData data;
    pugi::xml_document doc;
    odr::Road road;
    pugi::xml_node signals_node;
    pugi::xml_node signal_node;
};

TEST_P(OrientationConversionTest,
       GivenSignalWithDifferentOrientations_WhenConverting_ThenSignalOrientationMustBeCorrectlyStored)
{
    auto test_case = GetParam();
    signal_node.append_attribute("orientation").set_value(std::get<0>(test_case).c_str());
    road_logic_suite::LibOpenDriveRoadSignalConverter::ConvertForRoad(data, road);
    auto it = data.signals.find("1");
    ASSERT_TRUE(it != data.signals.end()) << "The signal was not found in data storage!";
    const auto signal = it->second;
    EXPECT_EQ(signal.orientation, std::get<1>(test_case));
}

INSTANTIATE_TEST_SUITE_P(LibOpenDriveRoadSignalConverterTest,
                         OrientationConversionTest,
                         ::testing::Values(std::make_pair("+", road_logic_suite::SignalOrientation::kPostive),
                                           std::make_pair("-", road_logic_suite::SignalOrientation::kNegative),
                                           std::make_pair("none", road_logic_suite::SignalOrientation::kNone),
                                           std::make_pair("unknownstr",
                                                          road_logic_suite::SignalOrientation::kUnknown)));

struct SignalYawAngleTestParams
{
    std::string orientation;
    units::angle::radian_t expected_yaw;
};

class YawAngleConversionTests : public ::testing::TestWithParam<SignalYawAngleTestParams>
{
  protected:
    YawAngleConversionTests() : road("0", kSampleRoadLength, "-1", "TestRoad") {}

    void SetUp() override
    {
        data = CreateDataStorageWithSimpleStraightRoad();
        road.xml_node = doc.append_child("road");

        signals_node = road.xml_node.append_child("signals");
        signal_node = signals_node.append_child("signal");
        signal_node.append_attribute("id").set_value("1");
        signal_node.append_attribute("hOffset").set_value(kSignalHeading);
    }

    road_logic_suite::OpenDriveData data;
    pugi::xml_document doc;
    odr::Road road;
    pugi::xml_node signals_node;
    pugi::xml_node signal_node;
};

TEST_P(YawAngleConversionTests,
       GivenSignalWithDifferentOrientations_WhenConverting_ThenSignalYawAngleMustBeCorrectlyConverted)
{
    const auto& params = GetParam();

    signal_node.append_attribute("orientation").set_value(params.orientation.c_str());
    road_logic_suite::LibOpenDriveRoadSignalConverter::ConvertForRoad(data, road);
    auto it = data.signals.find("1");
    ASSERT_TRUE(it != data.signals.end()) << "The signal was not found in data storage!";
    const auto signal = it->second;
    EXPECT_EQ(signal.yaw, params.expected_yaw);
}

INSTANTIATE_TEST_SUITE_P(
    LibOpenDriveRoadSignalConverterTest,
    YawAngleConversionTests,
    ::testing::Values(SignalYawAngleTestParams{"+", units::angle::radian_t(kSampleRoadHeading + M_PI + kSignalHeading)},
                      SignalYawAngleTestParams{"-", units::angle::radian_t(kSampleRoadHeading + kSignalHeading)},
                      SignalYawAngleTestParams{"none", units::angle::radian_t(kSampleRoadHeading + kSignalHeading)},
                      SignalYawAngleTestParams{"unknownstr",
                                               units::angle::radian_t(kSampleRoadHeading + kSignalHeading)}));

}  // namespace
