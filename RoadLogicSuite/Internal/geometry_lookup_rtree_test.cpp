/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

// clang-format is disabled for the next line. Because it suggests to reorder the gtest include after the
// internal includes which will leads to an compiler error.
// clang-format on
#include <gtest/gtest.h>
// clang-format off

#include "RoadLogicSuite/Internal/geometry_lookup_rtree.h"
#include "open_drive_data.h"
#include <MantleAPI/Common/position.h>
#include "RoadLogicSuite/Internal/Geometry/line.h"

using namespace units::length;

namespace road_logic_suite::test
{

// This function creates a provied number of line geometries.
// These lines are aligned straight along the x axis.
// Each line has a distance of 1 meter to its neighbour line.
// Each line has a length of 1 meter.
// Each line is assigned to a seperate road id.
road_logic_suite::OpenDriveData CreateDataStorageWithManyGeometries(int count_geometries)
{
    road_logic_suite::OpenDriveData data{};

    int road_id = 0;

    for (int x = 0; x < count_geometries; x++)
    {
        data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
            units::length::meter_t(0),
            road_logic_suite::Vec2<units::length::meter_t>(units::length::meter_t(x), units::length::meter_t(0)),
            units::angle::radian_t(0),
            units::length::meter_t(1),
            std::to_string(road_id)));

        road_id++;
    }

    return data;
}

TEST(GeometryLookupRTreeTest, GivenManyGeometries_WhenVisitGeometriesWithRTree_ThenSearchAreaShouldBeSmaller)
{
    // Create sample geometries
    auto data = road_logic_suite::test::CreateDataStorageWithManyGeometries(10000);

    std::unique_ptr<GeometryLookupRTree> geometry_lookup_;
    geometry_lookup_ = (std::make_unique<GeometryLookupRTree>(data.geometries));

    // Sample point
    const mantle_api::Vec3<units::length::meter_t> query{
        units::length::meter_t(1000.5), units::length::meter_t(0), units::length::meter_t(0)};

    // Result parameters and search callback
    int analyzed_geometries = 0;
    std::optional<mantle_api::OpenDriveRoadPosition> result = std::nullopt;
    auto minimum_distance = units::length::meter_t(std::numeric_limits<double>::max());

    std::function<bool(const IGeometry*)> search_callback = [&](const IGeometry* geom) {
        analyzed_geometries++;

        auto closest_point = geom->ConvertInertialCoordinatesToST(Vec2(query));
        if (!closest_point)
        {
            return true;  // return `true` to keep going, return `false` to stop
        }

        auto [s, t] = closest_point.value();
        if (units::math::abs(t) < minimum_distance)
        {
            minimum_distance = t;
            result = mantle_api::OpenDriveRoadPosition{.road = geom->road_id, .s_offset = s, .t_offset = t};
        }

        return true;  // return `true` to keep going, return `false` to stop
    };

    // Visit geometry without r-tree
    geometry_lookup_->VisitGeometry(search_callback);
    auto analysed_geometries_without_r_tree = analyzed_geometries;
    auto road_without_r_tree = result->road ;
    auto s_without_r_tree = result->s_offset;
    auto t_without_r_tree = result->t_offset;

    // Visit geometry with r-tree
    geometry_lookup_->OptimizeStorage();
    analyzed_geometries = 0;
    geometry_lookup_->VisitGeometry(Vec2(query), search_callback);
    auto analysed_geometries_with_r_tree = analyzed_geometries;
    auto road_with_r_tree = result->road;
    auto s_with_r_tree = result->s_offset;
    auto t_with_r_tree = result->t_offset;

    // Without r-tree all geometries should be analyzed.
    ASSERT_EQ(analysed_geometries_without_r_tree, data.geometries.size());
    // With r-tree only geometries located in the near the sample point will be analyzed.
    // A search area of 50 meter is defined in geometry_lookup_rtree.cpp
    // So, from the sample point x = 1000.5, 101 lines (each 1 meter long) are within a search area.
    ASSERT_EQ(analysed_geometries_with_r_tree, 101);
    // The sample point lies on the 1000th road.
    ASSERT_EQ(road_with_r_tree, "1000");
    ASSERT_EQ(road_without_r_tree, "1000");
    // The sample point lies in position s = 0.5 of the road.
    ASSERT_EQ(s_with_r_tree.value(), 0.5);
    ASSERT_EQ(s_without_r_tree.value(), 0.5);
    // The sample point does not have a lateral offset.
    ASSERT_EQ(t_with_r_tree.value(), 0);
    ASSERT_EQ(t_without_r_tree.value(), 0);
}

}  // namespace road_logic_suite::test
