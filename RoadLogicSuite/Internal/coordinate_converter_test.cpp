/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/coordinate_converter.h"

#include "RoadLogicSuite/Internal/Geometry/line.h"
#include "RoadLogicSuite/Internal/road_logic_suite_impl.h"
#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include <gtest/gtest.h>
#include <units.h>

#include <cmath>

using namespace units::length;
using namespace units::literals;

namespace road_logic_suite::test
{
static const double kAbsErrorMax = 1e-6;

/// @note unit test of Coordinate converter should not involve road_logic_suite
/// @note these tests are at integration test level

class CoordinateConverterTestFixture : public ::testing::Test
{
  public:
    const CoordinateConverter& GetCoordinateConverter(const std::string& xodr_path)
    {
        const auto absolute_path = utils::Resolve(xodr_path);
        suite_.LoadFile(absolute_path);
        return suite_.GetCoordinateConverter();
    }

  private:
    RoadLogicSuiteImpl suite_{};
};

/**
 * This test checks whether coordinate conversions for straight lines work. The given OpenDRIVE map is a straight road
 * that starts at x=0, y=0 and has a heading of 45°.
 */
TEST_F(CoordinateConverterTestFixture,
       GivenSimpleStraightRoad_WhenConvertingSTCoordinates_ThenCorrectInertialPositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/simple_road_two_parts.xodr");
    const auto s = 159.0;
    const auto t = -5.833;
    mantle_api::OpenDriveRoadPosition road_position_coords{
        .road = "0",
        .s_offset = meter_t(s),
        .t_offset = meter_t(t),
    };

    const auto inertial_coordinate_opt = converter.ConvertRoadPositionToInertialCoordinates(road_position_coords);

    const std::array<double, 3> expected_coordinates = {
        0.5 * sqrt(2) * s - 0.5 * sqrt(2) * t,  // X, note that cos(45) = sin(45) = 0.5 * sqrt(2).
        0.5 * sqrt(2) * s + 0.5 * sqrt(2) * t,  // Y
        0.0};
    ASSERT_TRUE(inertial_coordinate_opt);  // non-nullopt was returned
    const auto inertial_coordinate = inertial_coordinate_opt.value();
    EXPECT_NEAR(inertial_coordinate.x(), expected_coordinates[0], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.y(), expected_coordinates[1], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.z(), expected_coordinates[2], kAbsErrorMax);
}

/**
 * GIVEN: A simple straight road.
 *  AND: Inertial coordinates that lie on the road.
 * WHEN: Converting the coordinates into the reference coordinate system.
 * THEN: The correct coordinates must be returned.
 */
TEST_F(CoordinateConverterTestFixture,
       GivenSimpleStraightRoad_WhenConvertingInertialCoordinatesOnRoad_ThenCorrectRoadPositionCoordinatesMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/simple_road_two_parts.xodr");
    const auto s_expected = 159.0;
    const auto t_expected = -5.833;

    // note that cos(45) = sin(45) = 0.5 * sqrt(2).
    const auto x_p = meter_t(0.5 * sqrt(2) * s_expected - 0.5 * sqrt(2) * t_expected);
    const auto y_p = meter_t(0.5 * sqrt(2) * s_expected + 0.5 * sqrt(2) * t_expected);
    const auto inertial_coordinates = mantle_api::Vec3<units::length::meter_t>(x_p, y_p, 0_m);
    const auto road_position_coordinates_opt = converter.ConvertInertialToRoadPositionCoordinates(inertial_coordinates);

    ASSERT_TRUE(road_position_coordinates_opt);
    const auto& road_position_coordinates = road_position_coordinates_opt.value();
    EXPECT_EQ("0", road_position_coordinates.road);
    EXPECT_NEAR(s_expected, road_position_coordinates.s_offset(), kAbsErrorMax);
    EXPECT_NEAR(t_expected, road_position_coordinates.t_offset(), kAbsErrorMax);
}

/**
 * A check that should work for any kind of geometry. If the s coordinate is too big, i.e. 1000m if the road is only
 * 750m long, then the converter should return a nullopt.
 */
TEST_F(CoordinateConverterTestFixture,
       GivenSimpleStraightRoad_WhenConvertingInvalidSTCoordinates_ThenNoInertialPositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/simple_road_two_parts.xodr");
    mantle_api::OpenDriveRoadPosition road_position_coordinates{"0", 1000_m, 0_m};

    // s = 1000 is outside the s range for the road.
    const auto inertial_coordinate_opt = converter.ConvertRoadPositionToInertialCoordinates(road_position_coordinates);

    ASSERT_FALSE(inertial_coordinate_opt);  // nullopt was returned
}

/**
 * This test checks if the conversion from s-t coordinates work for left turns (positive curvature).
 */
TEST_F(CoordinateConverterTestFixture,
       GivenArcRoad_WhenConvertingValidSTCoordinates_ThenCorrectInertialPositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/road_with_arc.xodr");
    // note: For the arc, the 'center point' is @ (500;100) in inertial coordinates - we can deduce the resulting
    // position from that. The calculation is slightly simplified compared to the actual implementation since we already
    // know the center point of the arc and the start heading (hdg=0).
    const auto x_m = 500;
    const auto y_m = 100;
    const auto s = 65.626;
    const auto t = -2.256;
    const auto radius = 100;
    const auto theta = s / radius;
    const std::array<double, 3> expected_coordinates{x_m + (radius - t) * sin(theta),  // X
                                                     y_m - (radius - t) * cos(theta),  // Y
                                                     0.0};
    mantle_api::OpenDriveRoadPosition road_position_coordinates{
        "0",
        meter_t(500 + s),
        meter_t(t),
    };

    const auto inertial_coordinate_opt = converter.ConvertRoadPositionToInertialCoordinates(road_position_coordinates);

    ASSERT_TRUE(inertial_coordinate_opt);
    const auto inertial_coordinate = inertial_coordinate_opt.value();
    EXPECT_NEAR(inertial_coordinate.x(), expected_coordinates[0], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.y(), expected_coordinates[1], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.z(), expected_coordinates[2], kAbsErrorMax);
}

/**
 * GIVEN: A road with an arc at the end.
 *  AND: Inertial coordinates that lie on the curved part of the road.
 * WHEN: Converting the coordinates into the reference coordinate system.
 * THEN: The correct coordinates must be returned.
 */
TEST_F(CoordinateConverterTestFixture,
       GivenArcRoad_WhenConvertingInertialCoordinatesArc_ThenCorrectRoadPositionCoordinatesMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/road_with_arc.xodr");
    const auto s_expected = 565.626;
    const auto t_expected = -2.256;
    // note: For the arc, the 'center point' is @ (500;100) in inertial coordinates - we can deduce the resulting
    // position from that. The calculation is slightly simplified compared to the actual implementation since we already
    // know the center point of the arc and the start heading (hdg=0).
    const auto x_m = 500;
    const auto y_m = 100;
    const auto s = 65.626;
    const auto t = -2.256;
    const auto radius = 100;
    const auto theta = s / radius;
    const auto x_p = meter_t(x_m + (radius - t) * sin(theta));
    const auto y_p = meter_t(y_m - (radius - t) * cos(theta));
    const auto inertial_coordinates = mantle_api::Vec3<units::length::meter_t>(x_p, y_p, 0_m);

    const auto road_position_coordinates_opt = converter.ConvertInertialToRoadPositionCoordinates(inertial_coordinates);

    ASSERT_TRUE(road_position_coordinates_opt);
    const auto& road_position_coordinates = road_position_coordinates_opt.value();
    EXPECT_EQ("0", road_position_coordinates.road);
    EXPECT_NEAR(s_expected, road_position_coordinates.s_offset(), kAbsErrorMax);
    EXPECT_NEAR(t_expected, road_position_coordinates.t_offset(), kAbsErrorMax);
}

/**
 * This test checks if the conversion from s-t coordinates work for right turns (negative curvature).
 */
TEST_F(CoordinateConverterTestFixture,
       GivenMirroredArcRoad_WhenConvertingValidSTCoordinates_ThenCorrectInertialPositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/road_with_arc_mirror.xodr");
    // note: For the arc, the 'center point' is @ (500;100) in inertial coordinates - we can deduce the resulting
    // position from that. The calculation is slightly simplified compared to the actual implementation since we already
    // know the center point of the arc and the start heading.
    const auto x_m = 500;
    const auto y_m = -100;
    const auto s = 65.626;
    const auto t = -2.256;
    const auto radius = -100;  // caused by negative curvature. This causes a negative theta and in the end the correct
                               // results are calculated.
    const auto theta = s / radius;
    const std::array<double, 3> expected_coordinates{x_m + (radius - t) * sin(theta),  // X
                                                     y_m - (radius - t) * cos(theta),  // Y
                                                     0.0};
    const mantle_api::OpenDriveRoadPosition road_position_coordinates{
        "0",
        meter_t(500 + s),
        meter_t(t),
    };

    const auto inertial_coordinate_opt = converter.ConvertRoadPositionToInertialCoordinates(road_position_coordinates);

    ASSERT_TRUE(inertial_coordinate_opt);
    const auto inertial_coordinate = inertial_coordinate_opt.value();
    EXPECT_NEAR(inertial_coordinate.x(), expected_coordinates[0], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.y(), expected_coordinates[1], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.z(), expected_coordinates[2], kAbsErrorMax);
}

TEST_F(CoordinateConverterTestFixture,
       GivenMirroredArcRoad_WhenConvertingValidInertialCoordinates_ThenCorrectReferenceLienCoordinatesMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/road_with_arc_mirror.xodr");
    // backwards to above
    const auto x_m = 500;
    const auto y_m = -100;
    const auto expected_s = 65.626;
    const auto expected_t = -2.256;
    const auto radius = -100;  // caused by negative curvature. This causes a negative theta and in the end the correct
                               // results are calculated.
    const auto theta = expected_s / radius;
    mantle_api::Vec3<units::length::meter_t> inertial_coordinates(
        meter_t(x_m + (radius - expected_t) * sin(theta)),  // X
        meter_t(y_m - (radius - expected_t) * cos(theta)),  // Y
        0_m);

    const auto ref_line_coordinates_opt = converter.ConvertInertialToRoadPositionCoordinates(inertial_coordinates);
    ASSERT_TRUE(ref_line_coordinates_opt);
    const auto& ref_line_coordinates = ref_line_coordinates_opt.value();
    EXPECT_EQ(ref_line_coordinates.road, "0");
    EXPECT_NEAR(ref_line_coordinates.s_offset(), 500 + expected_s, kAbsErrorMax);
    EXPECT_NEAR(ref_line_coordinates.t_offset(), expected_t, kAbsErrorMax);
}

TEST_F(CoordinateConverterTestFixture,
       GivenParamPolyRoad_WhenConvertingValidSTCoordinates_ThenCorrectInertialPositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/road_with_param_poly_simple.xodr");

    const auto x_m = 500;
    const auto y_m = 0;
    const auto s = 1;
    const auto t = 0;

    const std::array<double, 3> expected_coordinates{x_m + 1,  // X
                                                     y_m + 1,  // Y
                                                     0.0};
    const mantle_api::OpenDriveRoadPosition road_position_coordinates{
        "0",
        meter_t(500 + s),
        meter_t(t),

    };

    const auto inertial_coordinate_opt = converter.ConvertRoadPositionToInertialCoordinates(road_position_coordinates);

    ASSERT_TRUE(inertial_coordinate_opt);
    const auto inertial_coordinate = inertial_coordinate_opt.value();
    EXPECT_NEAR(inertial_coordinate.x(), expected_coordinates[0], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.y(), expected_coordinates[1], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.z(), expected_coordinates[2], kAbsErrorMax);
}

TEST_F(CoordinateConverterTestFixture,
       GivenSimpleStraightRoad_WhenConvertingLaneCoordinates_ThenCorrectInertialPositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/simple_road_two_parts.xodr");

    // get coordinates on road position
    mantle_api::OpenDriveRoadPosition road_position_coords{
        .road = "0",
        .s_offset = meter_t(159.0),
        .t_offset = meter_t(0),
    };
    const auto inertial_coordinate_on_road_position_opt =
        converter.ConvertRoadPositionToInertialCoordinates(road_position_coords);
    const std::array<double, 3> expected_coordinates = {
        0.5 * sqrt(2) * road_position_coords.s_offset.value(),  // X, note that cos(45) = sin(45) = 0.5 * sqrt(2).
        0.5 * sqrt(2) * road_position_coords.s_offset.value(),  // Y
        0.0};
    ASSERT_TRUE(inertial_coordinate_on_road_position_opt);  // non-nullopt was returned
    const auto inertial_coordinate = inertial_coordinate_on_road_position_opt.value();
    EXPECT_NEAR(inertial_coordinate.x(), expected_coordinates[0], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.y(), expected_coordinates[1], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate.z(), expected_coordinates[2], kAbsErrorMax);

    // check local coordinates from lane id 1
    const mantle_api::Vec3<meter_t> t_direction = {meter_t(-0.5 * sqrt(2)), meter_t(0.5 * sqrt(2)), meter_t(0)};
    auto expected_coordinates_from_lane = inertial_coordinate + t_direction;
    mantle_api::OpenDriveLanePosition lane_coords{
        .road = "0", .lane = 1, .s_offset = road_position_coords.s_offset, .t_offset = meter_t(-0.5)};
    auto inertial_coordinates_lane_opt = converter.ConvertLaneToInertialCoordinates(lane_coords);
    ASSERT_TRUE(inertial_coordinates_lane_opt);  // non-nullopt was returned
    auto inertial_coordinate_lane = inertial_coordinates_lane_opt.value();
    EXPECT_NEAR(inertial_coordinate_lane.x(), expected_coordinates_from_lane.x.value(), kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate_lane.y(), expected_coordinates_from_lane.y.value(), kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate_lane.z(), expected_coordinates_from_lane.z.value(), kAbsErrorMax);

    // check local coordinates from lane id 2
    expected_coordinates_from_lane =
        inertial_coordinate + 4.0 * t_direction;  // road width is 3, so we should be on 2nd road with t=-0.5
    lane_coords = {.road = "0", .lane = 2, .s_offset = road_position_coords.s_offset, .t_offset = meter_t(-0.5)};
    inertial_coordinates_lane_opt = converter.ConvertLaneToInertialCoordinates(lane_coords);
    ASSERT_TRUE(inertial_coordinates_lane_opt);  // non-nullopt was returned
    inertial_coordinate_lane = inertial_coordinates_lane_opt.value();
    EXPECT_NEAR(inertial_coordinate_lane.x(), expected_coordinates_from_lane.x.value(), kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate_lane.y(), expected_coordinates_from_lane.y.value(), kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinate_lane.z(), expected_coordinates_from_lane.z.value(), kAbsErrorMax);
}

TEST_F(CoordinateConverterTestFixture,
       GivenSimpleStraightRoad_WhenConvertingInertialCoordinates_ThenCorrectLanePositionMustBeReturned)
{
    const auto& converter = GetCoordinateConverter("RoadLogicSuite/Tests/Data/Maps/simple_road_two_parts.xodr");

    // get coordinates on road position
    mantle_api::OpenDriveRoadPosition road_position_coords{
        .road = "0",
        .s_offset = meter_t(159.0),
        .t_offset = meter_t(0),
    };
    const auto inertial_coordinates_on_road_position_opt =
        converter.ConvertRoadPositionToInertialCoordinates(road_position_coords);
    const std::array<double, 3> expected_coordinates = {
        0.5 * sqrt(2) * road_position_coords.s_offset.value(),  // X, note that cos(45) = sin(45) = 0.5 * sqrt(2).
        0.5 * sqrt(2) * road_position_coords.s_offset.value(),  // Y
        0.0};
    ASSERT_TRUE(inertial_coordinates_on_road_position_opt);  // non-nullopt was returned
    const auto inertial_coordinates_on_road_position = inertial_coordinates_on_road_position_opt.value();
    EXPECT_NEAR(inertial_coordinates_on_road_position.x(), expected_coordinates[0], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinates_on_road_position.y(), expected_coordinates[1], kAbsErrorMax);
    EXPECT_NEAR(inertial_coordinates_on_road_position.z(), expected_coordinates[2], kAbsErrorMax);

    // check local coordinates from lane id 1
    const mantle_api::Vec3<meter_t> t_direction = {meter_t(-0.5 * sqrt(2)), meter_t(0.5 * sqrt(2)), meter_t(0)};
    auto inertial_coordinates = inertial_coordinates_on_road_position + t_direction;
    auto lane_coordinates_opt = converter.ConvertInertialToLaneCoordinates(inertial_coordinates);
    ASSERT_TRUE(lane_coordinates_opt);  // non-nullopt was returned
    auto lane_coordinates = lane_coordinates_opt.value();
    EXPECT_NEAR(lane_coordinates.s_offset.value(),
                road_position_coords.s_offset.value(),
                kAbsErrorMax);  // s coordinates should not change as we just move in t direction
    EXPECT_NEAR(lane_coordinates.t_offset.value(),
                -0.5,
                kAbsErrorMax);  // lane width is 3, and we moved 1 in t direction from reference line
    EXPECT_NEAR(lane_coordinates.lane, 1, kAbsErrorMax);          // we should still be on the first left lane
    EXPECT_EQ(lane_coordinates.road, road_position_coords.road);  // should be the same road as we started with

    // check local coordinates from lane id 2
    inertial_coordinates = inertial_coordinates_on_road_position + 4.0 * t_direction;
    lane_coordinates_opt = converter.ConvertInertialToLaneCoordinates(inertial_coordinates);
    ASSERT_TRUE(lane_coordinates_opt);  // non-nullopt was returned
    lane_coordinates = lane_coordinates_opt.value();
    EXPECT_NEAR(lane_coordinates.s_offset.value(),
                road_position_coords.s_offset.value(),
                kAbsErrorMax);  // s coordinates should not change as we just move in t direction
    EXPECT_NEAR(lane_coordinates.t_offset.value(),
                -0.5,
                kAbsErrorMax);  // lane width is 3, and we moved 4 in t direction from reference line
    EXPECT_NEAR(lane_coordinates.lane, 2, kAbsErrorMax);          // we should now be on the second left lane
    EXPECT_EQ(lane_coordinates.road, road_position_coords.road);  // should be the same road as we started with
}

TEST(CoordinateConverterTest, GivenSimpleGeometry_WhenQueryingForTheHeading_ThenTheCorrectValueShouldBeReturned)
{
    OpenDriveData data{.roads = {{"0", {.id = "0", .index_geometries_start = 0, .index_geometries_end = 1}}}};
    data.geometries.push_back(
        std::make_unique<Line>(units::length::meter_t(0),
                               Vec2<units::length::meter_t>(units::length::meter_t(0), units::length::meter_t(0)),
                               units::angle::radian_t(0.87),
                               units::length::meter_t(100),
                               "0"));
    const CoordinateConverter converter(data);

    const auto result = converter.GetRoadPositionHeading("0", units::length::meter_t(42.0));
    ASSERT_TRUE(result);
    EXPECT_NEAR(result.value().value(), 0.87, kAbsErrorMax);
}

road_logic_suite::OpenDriveData CreateDataStorageWithManyGeometries()
{
    road_logic_suite::OpenDriveData data{};

    auto current_x = units::length::meter_t(0);
    auto current_y = units::length::meter_t(0);
    std::uint32_t id = 0;

    for (unsigned int y = 0; y < 100; y++)
    {
        current_x = units::length::meter_t(50 * (y % 2));

        for (unsigned int x = 0; x < 100; x++)
        {
            auto id_string = std::to_string(id);

            road_logic_suite::Road road{.id = id_string,
                                        .length = units::length::meter_t(100),
                                        .index_geometries_start = id,
                                        .index_geometries_end = id + 1};
            data.roads.insert_or_assign(id_string, road);
            data.geometries.push_back(std::make_unique<road_logic_suite::Line>(
                units::length::meter_t(0),
                road_logic_suite::Vec2<units::length::meter_t>(current_x, current_y),
                units::angle::radian_t(0),
                units::length::meter_t(100),
                id_string));

            id++;
            current_x += units::length::meter_t(150);
        }

        current_y += units::length::meter_t(50);
    }

    return data;
}

TEST(CoordinateConverterTest, GivenBigMap_WhenConvertInertialToRoadPositionCoordinates_RTreeIsFaster)
{
    auto data = CreateDataStorageWithManyGeometries();
    CoordinateConverter converter(data);

    const auto inertial_position = mantle_api::Vec3<units::length::meter_t>(1500.0_m, 1000.0_m, 0.0_m);

    // convert without r-tree
    auto start = std::chrono::high_resolution_clock::now();
    const auto converted_position_without_r_tree =
        converter.ConvertInertialToRoadPositionCoordinates(inertial_position);
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration_without_r_tree = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    // convert with r-tree
    converter.OptimizeStorage();
    start = std::chrono::high_resolution_clock::now();
    const auto converted_position_with_r_tree = converter.ConvertInertialToRoadPositionCoordinates(inertial_position);
    stop = std::chrono::high_resolution_clock::now();
    auto duration_with_r_tree = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    EXPECT_NEAR(converted_position_without_r_tree.value().s_offset.value(),
                converted_position_with_r_tree.value().s_offset.value(),
                kAbsErrorMax);
    EXPECT_NEAR(converted_position_without_r_tree.value().t_offset.value(),
                converted_position_with_r_tree.value().t_offset.value(),
                kAbsErrorMax);
    EXPECT_EQ(converted_position_without_r_tree.value().road, converted_position_with_r_tree.value().road);
    EXPECT_LT(duration_with_r_tree.count(), duration_without_r_tree.count());

    std::cout << duration_without_r_tree.count() << " - " << duration_with_r_tree.count() << std::endl;
}

}  // namespace road_logic_suite::test
