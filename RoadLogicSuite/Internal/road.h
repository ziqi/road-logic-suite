/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_H
#define ROADLOGICSUITE_ROAD_H

#include "RoadLogicSuite/Internal/Types/lane_section.h"
#include "RoadLogicSuite/Internal/Types/road_link.h"

#include <units.h>

#include <cstdint>

namespace road_logic_suite
{
/// @brief This struct is a representation of a road inside the RoadLogicSuite.
struct Road
{
    /// @brief Identifier according to OpenDRIVE specification.
    std::string id{};

    /// @brief The corresponding junction id assigned to the road.
    std::string road_assigned_junction_id{};

    /// @brief Total length of the reference line in the xy-plane. Change in length due to elevation is not considered.
    /// @noote: This is the value that is read from the XML directly and is not calculated through the geometries.
    units::length::meter_t length{};

    /// @brief This index points to the first geometry in the data storage that belongs to this road.
    /// You can for example use this for iterating only the geometries you care about or for using STL algorithms:
    /// @code
    /// auto begin = std::next(data_storage_reference_.geometries.begin(), current_road.index_geometries_start);
    /// auto end = std::next(data_storage_reference_.geometries.begin(), current_road.index_geometries_end);
    /// for (auto it = begin; it != end; ++it)
    /// {
    ///     std::cout << "This geometry is @ s=" << it->s << std::endl;
    /// }
    /// @endcode
    std::uint32_t index_geometries_start = 0;

    /// @brief This index points to the last geometry in the data storage that belongs to this road.
    std::uint32_t index_geometries_end = 0;

    /// @brief Maps lane sections along the s-coordinate of the reference line.
    types::RangeBasedMap<units::length::meter_t, types::LaneSection> lane_sections;

    /// @brief List of predecessors and successors for this road. So far, only road types.
    std::vector<RoadLink> road_links;

    /// @brief Maps elevation polynomials along the s-coordinate of the reference line.
    types::RangeBasedMap<units::length::meter_t, types::Poly3> elevations{};

    /// @brief Maps superelevation polynomials along the s-coordinate of the reference line.
    types::RangeBasedMap<units::length::meter_t, types::Poly3> superelevations{};

    /// @brief Maps laneoffsets polynomials along the s-coordinate of the reference line.
    types::RangeBasedMap<units::length::meter_t, types::Poly3> laneoffsets{};
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_ROAD_H
