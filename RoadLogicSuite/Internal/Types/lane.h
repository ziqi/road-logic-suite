/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_LANE_H
#define ROADLOGICSUITE_LANE_H

#include "RoadLogicSuite/Internal/Types/poly3.h"
#include "RoadLogicSuite/Internal/Types/range_based_map.h"
#include "RoadLogicSuite/Internal/Types/road_barrier.h"
#include "RoadLogicSuite/Internal/Types/road_mark.h"

#include <units.h>

#include <vector>

namespace road_logic_suite::types
{
struct Lane
{
    using LaneId = std::int32_t;
    LaneId id{};

    /// @brief Maps road marks along the s-coordinate of this lane. s=0 is the start of the lane.
    RangeBasedMap<units::length::meter_t, RoadMark> road_mark{};

    /// @brief Maps road barriers along the s-coordinate of this lane. s=0 is the start of the lane.
    std::vector<RoadBarrier> road_barriers{};

    /// @brief Maps the lane width along the s-coordinate of this lane. s=0 is the start of the lane.
    RangeBasedMap<units::length::meter_t, Poly3> width_polys{};

    /// @brief Lane IDs for the predecessors of this lane.
    std::vector<LaneId> predecessors{};

    /// @brief Lane IDs for the successors of this lane.
    std::vector<LaneId> successors{};

    /// @brief Lane type defined by OpenDrive
    std::string type{};

    /// @brief Returns the width of the lane at a specific s-coordinate.
    /// @param s the given s-coordinate.
    /// @return the width at s.
    [[nodiscard]] units::length::meter_t GetWidthAt(const units::length::meter_t& s) const;
};
}  // namespace road_logic_suite::types

#endif  // ROADLOGICSUITE_LANE_H
