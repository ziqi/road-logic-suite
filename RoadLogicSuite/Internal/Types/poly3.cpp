/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/poly3.h"

namespace road_logic_suite
{
namespace types
{

Poly3::Poly3(double a, double b, double c, double d) : a(a), b(b), c(c), d(d) {}

double Poly3::CalcAt(double t) const
{
    auto tt = t * t;
    auto ttt = tt * t;
    return a + b * t + c * tt + d * ttt;
}

double Poly3::CalcDerivativeAt(double t) const
{
    return b + 2 * c * t + 3 * d * t * t;
}

double Poly3::CalcDerivative2At(double t) const
{
    return 2 * c + 6 * d * t;
}

}  // namespace types
}  // namespace road_logic_suite
