/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_POLY3_H
#define ROADLOGICSUITE_POLY3_H

namespace road_logic_suite::types
{

/**
 * Represents a cubic polynomial in the form of a + b*t + c*t² + d*t³
 */
struct Poly3
{
    double a;
    double b;
    double c;
    double d;

    Poly3(double a, double b, double c, double d);

    /// @brief Calculates the value of the polynomial at t
    /// @param t
    /// @return The result of the polynomial
    double CalcAt(double t) const;

    /// @brief Calculates the first derivative of the polynomial at t
    /// @param t
    /// @return The result of the first derivative
    double CalcDerivativeAt(double t) const;

    /// @brief Calculates the second derivative of the polynomial at t
    /// @param t
    /// @return The result of the second derivative
    double CalcDerivative2At(double t) const;
};

}  // namespace road_logic_suite::types

#endif  // ROADLOGICSUITE_POLY3_H
