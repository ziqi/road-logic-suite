/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/lane.h"

#include "RoadLogicSuite/Internal/Types/poly3.h"

#include <gtest/gtest.h>

namespace
{
TEST(LaneTest, GivenALaneWithWidth_WhenFetchingTheWidth_TheCorrectValueMustBeReturned)
{
    road_logic_suite::types::Lane lane{};
    lane.width_polys.Insert(units::length::meter_t(0), road_logic_suite::types::Poly3(0.2, 3.41, -2.99, 0.58));

    const auto width = lane.GetWidthAt(units::length::meter_t(6.76));

    EXPECT_NEAR(65.787, width(), 1e-3);
}
}  // namespace