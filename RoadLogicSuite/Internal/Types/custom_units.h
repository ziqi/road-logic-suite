/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_CUSTOM_UNITS_H
#define ROADLOGICSUITE_CUSTOM_UNITS_H

#include <units.h>

namespace road_logic_suite::custom_units
{
/// @brief Define curvature as 1/m.
using curvature = units::unit<std::ratio<1>, units::inverse<units::length::meters>>;
using curvature_t = units::unit_t<curvature>;
}  // namespace road_logic_suite::custom_units

#endif  // ROADLOGICSUITE_CUSTOM_UNITS_H
