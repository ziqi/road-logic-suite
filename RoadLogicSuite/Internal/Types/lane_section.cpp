/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/lane_section.h"

#include "RoadLogicSuite/Internal/Types/lane.h"

namespace road_logic_suite::types
{
const Lane& LaneSection::GetLaneById(Lane::LaneId id) const
{
    auto vector_index = static_cast<std::make_unsigned<decltype(id)>::type>(std::abs(id) - 1);
    if (id > 0 && left_lanes.size() > vector_index)
    {
        return left_lanes[vector_index];
    }
    else if (id < 0 && right_lanes.size() > vector_index)
    {
        return right_lanes[vector_index];
    }
    else if (id == 0)
    {
        return center_lane;
    }
    else
    {
        throw std::runtime_error("Given lane id does not exist: " + std::to_string(id));
    }
}

[[nodiscard]] unsigned int LaneSection::GetLaneCount() const
{
    const int center_line = 1;
    return left_lanes.size() + right_lanes.size() + center_line;
}

Range<std::vector<types::Lane>::iterator> LaneSection::GetLaneRangeFromReferenceLineToId(types::Lane::LaneId id)
{
    auto end_index =
        static_cast<std::make_unsigned<decltype(id)>::type>(std::abs(id));  // index right behind the searched one
    if (id > 0 && left_lanes.size() >= end_index)
    {
        return make_range(left_lanes, 0, end_index);
    }
    else if (id < 0 && right_lanes.size() >= end_index)
    {
        return make_range(right_lanes, 0, end_index);
    }
    else
    {
        // id does not exist or it is 0.
        return make_range(left_lanes, 0, 0);
    }
}

bool LaneSection::IsValidLaneId(types::Lane::LaneId id) const
{
    const auto min_lane_id = static_cast<std::make_signed<std::int64_t>::type>(-right_lanes.size());
    const auto max_lane_id = static_cast<std::make_signed<std::int64_t>::type>(left_lanes.size());
    return (min_lane_id <= id && id <= max_lane_id);
}

types::Lane& LaneSection::GetLaneByBoundaryOffset(units::length::meter_t s, units::length::meter_t t)
{
    /// @todo: When implementing lane offsets (see: OpenDRIVE 1.6: Ch. 9.3), this needs to be considered as well.
    const auto abs_offset = units::math::abs(t);
    auto lane_ref = std::ref(center_lane);
    auto current_width = units::length::meter_t(0);
    auto min_distance = units::math::abs(abs_offset - current_width);
    auto& lanes = t > units::length::meter_t(0) ? left_lanes : right_lanes;
    for (auto& lane : lanes)
    {
        current_width += lane.GetWidthAt(s);
        const auto dist = units::math::abs(abs_offset - current_width);
        if (dist < min_distance)
        {
            lane_ref = std::ref(lane);
            min_distance = dist;
        }
        else
        {
            break;
        }
    }
    return lane_ref;
}
}  // namespace road_logic_suite::types