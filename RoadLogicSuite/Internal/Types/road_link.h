/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROADLINK_H
#define ROADLOGICSUITE_ROADLINK_H

#include "RoadLogicSuite/Internal/Types/link_contact_type.h"
#include "RoadLogicSuite/Internal/Types/road_link_type.h"

namespace road_logic_suite
{

/// @brief brief description are three types of road links: <br>
/// <example>
/// ---->|----> // Successor points to Start and Predecessor to End
/// ---->|<---- // SUccessor and predecessor point to End
/// <----|----> // Successor and predecessor point to Start
/// </example>
struct RoadLink
{
    /// @brief ID of the road this link points to.
    std::string target_id{};

    /// @brief Type of road link (predecessor/successor)
    RoadLinkType link_type{};

    /// @brief Type of the contact point (start/end)
    LinkContactType contact_type{};
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_ROADLINK_H
