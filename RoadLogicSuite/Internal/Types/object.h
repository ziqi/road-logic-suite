/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_OBJECT_H
#define ROADLOGICSUITE_OBJECT_H

#include <units.h>

#include <vector>

namespace road_logic_suite
{
struct Object
{
    using LaneId = std::int32_t;

    /// @brief Name of the object. Equal to OpenDrive definition.
    std::string name{};

    /// @brief Id of the road it belongs to.
    std::string road_id{};

    /// @brief Position of the object in inertial coordinates.
    mantle_api::Vec3<units::length::meter_t> position{};

    /// @brief Heading (angle around z axis) in inertial coordinates. An angle of 0 determines a direction parallel to
    /// the X axis.
    units::angle::radian_t hdg{};

    /// @brief Pitch in inertial coordinates.
    units::angle::radian_t pitch{};

    /// @brief Roll in inertial coordinates.
    units::angle::radian_t roll{};

    /// @brief Two-dimensional shape of the object.
    std::vector<Vec2<units::length::meter_t>> shape{};

    /// @brief Height of the object’s bounding box defined in the local coordinate system u/v along the z-axis.
    units::length::meter_t height{};

    /// @brief Object type.
    std::string type{};

    /// @brief Defines the lanes for which a object is valid.
    /// @see \link
    /// https://publications.pages.asam.net/standards/ASAM_OpenDRIVE/ASAM_OpenDRIVE_Specification/latest/specification/13_objects/13_06_lane_validity_obj.html
    /// \endlink
    std::vector<LaneId> valid_lanes{};
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_OBJECT_H
