/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/road_mark_utils.h"

#include <gtest/gtest.h>

namespace
{
using namespace road_logic_suite::types;

TEST(RoadMarkTypeTest, GivenValidInput_WhenParseRoadMarkType_ParsesColorCorrectly)
{
    EXPECT_EQ(RoadMarkType::BOTTS_DOTS, utils::ParseRoadMarkType("Botts Dots"));
    EXPECT_EQ(RoadMarkType::BROKEN_BROKEN, utils::ParseRoadMarkType("Broken Broken"));
    EXPECT_EQ(RoadMarkType::BROKEN_SOLID, utils::ParseRoadMarkType("Broken Solid"));
    EXPECT_EQ(RoadMarkType::BROKEN, utils::ParseRoadMarkType("Broken"));
    EXPECT_EQ(RoadMarkType::CURB, utils::ParseRoadMarkType("Curb"));
    EXPECT_EQ(RoadMarkType::CUSTOM, utils::ParseRoadMarkType("Custom"));
    EXPECT_EQ(RoadMarkType::EDGE, utils::ParseRoadMarkType("Edge"));
    EXPECT_EQ(RoadMarkType::GRASS, utils::ParseRoadMarkType("Grass"));
    EXPECT_EQ(RoadMarkType::NONE, utils::ParseRoadMarkType("None"));
    EXPECT_EQ(RoadMarkType::SOLID_BROKEN, utils::ParseRoadMarkType("Solid Broken"));
    EXPECT_EQ(RoadMarkType::SOLID_SOLID, utils::ParseRoadMarkType("Solid Solid"));
    EXPECT_EQ(RoadMarkType::SOLID, utils::ParseRoadMarkType("Solid"));
}

TEST(ParseRoadMarkTypeTest, GivenInvalidInput_WhenParseRoadMarkType_ThenReturnsUnknownAndPrintsWarningMessage)
{
    testing::internal::CaptureStdout();
    EXPECT_EQ(RoadMarkType::UNKNOWN, utils::ParseRoadMarkType("invalid_type"));
    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ("WARNING: Invalid road mark type input: invalid_type, please check your map file.\n", console_output);
}

TEST(ParseRoadMarkWeightTest, GivenValidInput_WhenParseRoadMarkWeight_ParsesColorCorrectly)
{
    EXPECT_EQ(RoadMarkWeight::BOLD, utils::ParseRoadMarkWeight("bold"));
    EXPECT_EQ(RoadMarkWeight::STANDARD, utils::ParseRoadMarkWeight("standard"));
}

TEST(ParseRoadMarkWeightTest, GivenInvalidInput_WhenParseRoadMarkWeight_ThenReturnsUnknownAndPrintsWarningMessage)
{
    testing::internal::CaptureStdout();
    EXPECT_EQ(RoadMarkWeight::UNKNOWN, utils::ParseRoadMarkWeight("invalid_weight"));
    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ("WARNING: Invalid road mark weight input: invalid_weight, please check your map file.\n", console_output);
}

TEST(ParseRoadMarkColorTest, GivenValidInput_WhenParseRoadMarkColor_ParsesColorCorrectly)
{
    EXPECT_EQ(RoadMarkColor::BLUE, utils::ParseRoadMarkColor("blue"));
    EXPECT_EQ(RoadMarkColor::GREEN, utils::ParseRoadMarkColor("green"));
    EXPECT_EQ(RoadMarkColor::ORANGE, utils::ParseRoadMarkColor("orange"));
    EXPECT_EQ(RoadMarkColor::RED, utils::ParseRoadMarkColor("red"));
    EXPECT_EQ(RoadMarkColor::STANDARD, utils::ParseRoadMarkColor("standard"));
    EXPECT_EQ(RoadMarkColor::VIOLET, utils::ParseRoadMarkColor("violet"));
    EXPECT_EQ(RoadMarkColor::WHITE, utils::ParseRoadMarkColor("white"));
    EXPECT_EQ(RoadMarkColor::YELLOW, utils::ParseRoadMarkColor("yellow"));
}

TEST(ParseRoadMarkColorTest, GivenInvalidInput_WhenParseRoadMarkColor_ThenReturnsUnknownAndPrintsWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(RoadMarkColor::UNKNOWN, utils::ParseRoadMarkColor("invalid_input"));
    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ("WARNING: Invalid road mark color input: invalid_input, please check your map file.\n", console_output);
}

TEST(ParseRoadMarkLaneChangeTest, GivenValidInput_ParseRoadMarkLaneChange_ParsesLaneChangeCorrectly)
{
    EXPECT_EQ(RoadMarkLaneChange::BOTH, utils::ParseRoadMarkLaneChange("both"));
    EXPECT_EQ(RoadMarkLaneChange::DECREASE, utils::ParseRoadMarkLaneChange("decrease"));
    EXPECT_EQ(RoadMarkLaneChange::INCREASE, utils::ParseRoadMarkLaneChange("increase"));
    EXPECT_EQ(RoadMarkLaneChange::NONE, utils::ParseRoadMarkLaneChange("none"));
}

TEST(ParseRoadMarkLaneChangeTest,
     GivenInvalidInput_WhenParseRoadMarkLaneChange_ThenReturnsUnknownAndPrintsWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(RoadMarkLaneChange::UNKNOWN, utils::ParseRoadMarkLaneChange("invalid_input"));
    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ("WARNING: Invalid road mark Lane Change input: invalid_input, please check your map file.\n",
              console_output);
}

}  // namespace
