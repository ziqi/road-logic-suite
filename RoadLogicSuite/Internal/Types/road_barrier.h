/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_BARRIER_H
#define ROADLOGICSUITE_ROAD_BARRIER_H

#include <units.h>

namespace road_logic_suite
{
/// @brief Internal definition of a road barrier. The fields all correspond to the field with the same name in
/// OpenDRIVEs "object" and "repeat" records.
struct RoadBarrier
{
    units::length::meter_t s_start{};
    units::length::meter_t s_end{};
    units::length::meter_t t_start{};
    units::length::meter_t t_end{};
    units::length::meter_t z_offset_start{};
    units::length::meter_t z_offset_end{};
    units::length::meter_t height_start{};
    units::length::meter_t height_end{};
    units::length::meter_t width_start{};
    units::length::meter_t width_end{};
    std::string name{};
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_ROAD_BARRIER_H
