/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/lane_section.h"

#include <gtest/gtest.h>

namespace
{
TEST(LaneSectionTest, GivenLaneSectionWithMultipleLanes_WhenFetchingLaneByOffset_ThenCorrectLaneIsReturned)
{
    const std::vector<std::pair<units::length::meter_t, road_logic_suite::types::Lane::LaneId>> test_cases{
        {units::length::meter_t(4.92), 2},
        {units::length::meter_t(0.48), 0},
        {units::length::meter_t(-5.28), -2},
        {units::length::meter_t(2.88), 1},
        {units::length::meter_t(-3.96), -1},
    };
    const units::length::meter_t s0(0);
    const road_logic_suite::types::Poly3 constant_width(3, 0, 0, 0);
    road_logic_suite::types::LaneSection section{
        .left_lanes = {{.id = 1, .width_polys = {{s0, constant_width}}},
                       {.id = 2, .width_polys = {{s0, constant_width}}}},
        .right_lanes = {{.id = -1, .width_polys = {{s0, constant_width}}},
                        {.id = -2, .width_polys = {{s0, constant_width}}}},
        .center_lane = {.id = 0},
    };

    for (const auto& test_case : test_cases)
    {
        ASSERT_EQ(section.GetLaneByBoundaryOffset(units::length::meter_t(0), test_case.first).id, test_case.second);
    }
}
}  // namespace