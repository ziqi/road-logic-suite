/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/Types/range_based_map.h"

#include <gtest/gtest.h>
#include <units.h>

#include <algorithm>
#include <string>

namespace
{
auto CreateSampleMap()
{
    road_logic_suite::types::RangeBasedMap<units::length::meter_t, std::string> map{};
    // values can be inserted in any order
    map.Insert(units::length::meter_t(21.95), "Last Entry");
    map.Insert(units::length::meter_t(7.12), "First Entry");
    map.Insert(units::length::meter_t(18.55), "Middle Entry");

    return map;
}

TEST(RangeBasedMapTest, GivenARangeBasedMap_WhenFetchingElement_ThenElementShouldBeReturned)
{
    const auto map = CreateSampleMap();

    // if between two entries, lower entry is returned.
    EXPECT_EQ("Middle Entry", map.At(units::length::meter_t(19.67))) << "between entries";
    // if lower than lowest entry, lowest entry is returned.
    EXPECT_EQ("First Entry", map.At(units::length::meter_t(2.16))) << "lower than lowest entry";
    // if higher than highest entry, highest entry is returned.
    EXPECT_EQ("Last Entry", map.At(units::length::meter_t(47.14))) << "higher than highest entry";
}

TEST(RangeBasedMapTest, GivenARangeBasedMap_WhenFetchingElementWithExactKey_ThenElementShouldBeReturned)
{
    const auto map = CreateSampleMap();

    EXPECT_EQ(map.At(units::length::meter_t(18.55)), "Middle Entry");
}

TEST(RangeBasedMapTest, GivenARangeBasedMap_WhenIteratingOverTheMap_ThenAllValuesMustBeInCorrectOrder)
{
    const auto map = CreateSampleMap();

    std::vector<std::string> results{};
    for (const auto& entry : map)
    {
        results.push_back(entry.second);
    }

    EXPECT_EQ("First Entry", results[0]);
    EXPECT_EQ("Middle Entry", results[1]);
    EXPECT_EQ("Last Entry", results[2]);
}

TEST(RangeBasedMapTest, GivenARangeBasedMap_WhenFindingElement_ThenElementIteratorShouldBeReturned)
{
    const auto map = CreateSampleMap();

    EXPECT_EQ(map.Find(units::length::meter_t(18.55))->second, "Middle Entry");
}

TEST(RangeBasedMapTest, GivenInitializerList_WhenConstructionMap_ItShouldBeCorrect)
{
    const road_logic_suite::types::RangeBasedMap<double, std::string> map{
        {0, "First Entry"},
        {50, "Second Entry"},
        {100, "Third Entry"},
    };

    EXPECT_EQ("First Entry", map.At(25));
}

TEST(RangeBasedMapTest, GivenNonConstMap_WhenTryingToChangeValue_ItShouldBePossible)
{
    const std::string replacement = "[REDACTED]";
    road_logic_suite::types::RangeBasedMap<double, std::string> map{
        {0, "First Entry"},
        {50, "Second Entry"},
        {100, "Third Entry"},
    };
    ASSERT_EQ(map.Find(56)->second, "Second Entry");

    map.Find(51)->second = "[REDACTED]";

    ASSERT_EQ(map.Find(56)->second, replacement);
}
}  // namespace