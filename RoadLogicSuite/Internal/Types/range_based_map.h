/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_RANGE_BASED_MAP_H
#define ROADLOGICSUITE_RANGE_BASED_MAP_H

#include <algorithm>
#include <cstdint>
#include <map>

namespace road_logic_suite::types
{

/// @brief A special map type, that allows mapping types to i.e. a floating point value. The map can be visualized as a
/// number line on which the different elements are defined, like follows:
/// @code
/// Example:
/// key: 0            3.45            8.9
///      |-------------|---------------|-------
/// val: E1            E2              E3
/// @endcode
/// Note, that the mapping goes from TKey's min value to the max value.
///
/// Example:
/// In the above example E1 is mapped to 0, E2 to 3.45 and so on. Here are some examples of what the map would return
/// with specific keys: <br>
/// map[5.0] -> E2 <br>
/// map[2.0] -> E1 <br>
/// map[130] -> E3 <br>
/// map[-40] -> E1
/// @tparam TKey input key. Must be comparable.
/// @tparam TValue The mapped value. Can be anything.
template <typename TKey, typename TValue>
class RangeBasedMap
{
  public:
    using ConstIterator = typename std::map<TKey, TValue>::const_iterator;
    using Iterator = typename std::map<TKey, TValue>::iterator;

    RangeBasedMap() = default;
    RangeBasedMap(std::initializer_list<std::pair<const TKey, TValue>> init) : internal_map_(init) {}

    /// @brief Return the value at the given key.
    /// @remark
    /// This map is mapping values continuously, and this method returns the next-lower one, so if a value is mapped to
    /// 2.0 and the next one at 5.0, if you call map.at(3.0), this method returns the value for 2.0.
    /// @param key the key.
    /// @return the value belonging to the next-lower key.
    TValue At(const TKey& key) const { return Find(key)->second; }

    /// @brief check is map empty
    /// @return true map is empty, false otherwise
    [[nodiscard]] bool Empty() const { return internal_map_.empty(); }

    /// @brief Finds element with specific key
    /// @param key the key.
    /// @return the iterator for the next-lower key.
    ConstIterator Find(const TKey& key) const
    {
        auto obj = std::find_if(internal_map_.rbegin(),
                                internal_map_.rend(),
                                [&key](const std::pair<TKey, TValue>& elem) { return elem.first <= key; });
        if (obj == internal_map_.rend())
        {
            return internal_map_.begin();
        }
        return (++obj).base();
    }

    /// @brief Finds element with specific key.
    /// @param key the key.
    /// @return the iterator for the next-lower key.
    Iterator Find(const TKey& key)
    {
        auto obj = std::find_if(internal_map_.rbegin(),
                                internal_map_.rend(),
                                [&key](const std::pair<TKey, TValue>& elem) { return elem.first <= key; });
        if (obj == internal_map_.rend())
        {
            return internal_map_.begin();
        }
        return (++obj).base();
    }

    /// @brief Finds the index of the given key.
    /// @param key the key
    /// @return the distance from begin() to the element with the given key.
    std::int64_t FindIndex(const TKey& key) const { return std::distance(internal_map_.begin(), Find(key)); }

    /// @brief Inserts a new key-value pair into the map.
    /// @param key the key.
    /// @param value the value.
    void Insert(const TKey& key, const TValue& value) { internal_map_.insert_or_assign(key, value); }

    /// @brief gives the const iterator begin
    /// @return ConstIterator begin()
    ConstIterator begin() const { return internal_map_.begin(); }

    /// @brief gives the const iterator end
    /// @return ConstIterator end()
    ConstIterator end() const { return internal_map_.end(); }

    /// @brief gives the size of the internal map.
    /// @return the size of the internal map.
    std::size_t size() const { return internal_map_.size(); }

  private:
    std::map<TKey, TValue> internal_map_{};
};
}  // namespace road_logic_suite::types

#endif  // ROADLOGICSUITE_RANGE_BASED_MAP_H
