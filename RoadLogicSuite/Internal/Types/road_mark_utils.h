/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ROAD_MARKUTILS_H
#define ROADLOGICSUITE_ROAD_MARKUTILS_H

#include "RoadLogicSuite/Internal/Types/road_mark.h"

namespace road_logic_suite::types::utils
{

/// @brief Parse a string representation of a road marking type and return its corresponding enum value.
/// @note The input string is case-insensitive and is converted to lowercase before being matched against the map.
/// @warning If the input string does not match any value in the map, a warning message is printed to the console.
/// @param type A string representing a road marking type.
/// @return The RoadMarkType enum value corresponding to the input string.
RoadMarkType ParseRoadMarkType(const std::string& type);

/// @brief Parses the road marking weight from a string input.
/// @note The input string is case-insensitive and is converted to lowercase before being matched against the map.
/// @warning If the input string does not match any value in the map, a warning message is printed to the console.
/// @param weight A string representing the road marking weight to be parsed.
/// @return  The corresponding RoadMarkWeight enum value.
RoadMarkWeight ParseRoadMarkWeight(const std::string& weight);

/// @brief  Parses a string into a RoadMarkColor enum value.
/// @note The input string is case-insensitive and is converted to lowercase before being matched against the map.
/// @warning If the input string does not match any value in the map, a warning message is printed to the console.
/// @param color A string representing the color of the road mark.
/// @return A RoadMarkColor enum value corresponding to the input string.
RoadMarkColor ParseRoadMarkColor(const std::string& color);

/// @brief  Parses a string into a RoadMarkLaneChange enum value.
/// @note The input string is case-insensitive and is converted to lowercase before being matched against the map.
/// @warning If the input string does not match any value in the map, a warning message is printed to the console.
/// @param color A string representing the lane change of the road mark.
/// @return A RoadMarkLaneChange enum value corresponding to the input string.
RoadMarkLaneChange ParseRoadMarkLaneChange(const std::string& lane_change);

}  // namespace road_logic_suite::types::utils

#endif  // ROADLOGICSUITE_ROAD_MARKUTILS_H
