/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "geometry_lookup_rtree.h"

namespace road_logic_suite
{

GeometryLookupRTree::GeometryLookupRTree(std::vector<std::unique_ptr<IGeometry>> const& geometries)
    : is_using_r_tree_(false), geometries_(geometries)
{
}

void GeometryLookupRTree::OptimizeStorage()
{
    tree_.RemoveAll();
    is_using_r_tree_ = false;
    // using a magic number of geometry size to decide if we optimize with r-tree or not
    if (geometries_.size() < 1000)
    {
        return;
    }

    for (auto& geometry : geometries_)
    {
        const auto bounding_box = geometry->GetBoundingBox();
        std::array<double, 2> min = {bounding_box.lower_left.x.value(), bounding_box.lower_left.y.value()};
        std::array<double, 2> max = {bounding_box.upper_right.x.value(), bounding_box.upper_right.y.value()};
        tree_.Insert(min.data(), max.data(), geometry.get());
    }
    is_using_r_tree_ = true;
}

void GeometryLookupRTree::VisitGeometry(const std::function<bool(const IGeometry*)>& search_callback) const
{
    for (const auto& geom : geometries_)
    {
        if (!search_callback(&*geom))
        {
            break;
        }
    }
}

void GeometryLookupRTree::VisitGeometry(const Vec2<units::length::meter_t>& search_point,
                                        const std::function<bool(const IGeometry*)>& search_callback) const
{
    // fallback if we are not using the r-tree
    if (!is_using_r_tree_)
    {
        VisitGeometry(search_callback);
        return;
    }

    auto current_size = units::length::meter_t(50);
    int num_found = 0;
    while (num_found <= 0)
    {
        auto search_rect = types::Rect<units::length::meter_t>::CreateRectAroundPoint(search_point, current_size);
        // search inside given bounding box, execute callback on each hit
        std::array<double, 2> min = {search_rect.lower_left.x.value(), search_rect.lower_left.y.value()};
        std::array<double, 2> max = {search_rect.upper_right.x.value(), search_rect.upper_right.y.value()};
        num_found = tree_.Search(min.data(), max.data(), search_callback);
        current_size *= 2;
    }
}

}  // namespace road_logic_suite