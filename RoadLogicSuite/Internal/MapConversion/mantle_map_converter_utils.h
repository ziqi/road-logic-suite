/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_MAP_CONVERTERUTILS_H
#define ROADLOGICSUITE_MANTLE_MAP_CONVERTERUTILS_H

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/Utils/converter_utils.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MapAPI/map.h>

namespace road_logic_suite::map_conversion::utils
{
/// @brief Creates a boundary point at the defined position.
///
/// @param converter the coordinate converter.
/// @param s road reference line coordinate.
/// @param t_offset optional offset (i.e. for double boundaries like solid-solid).
/// @param road_id the road id (string).
/// @param road_mark the road mark definition.
/// @param section the current lane section.
/// @param lane_id the lane id.
/// @return a new lane boundary point.
inline std::optional<map_api::LaneBoundary::BoundaryPoint> CreateBoundaryPointAt(
    const road_logic_suite::CoordinateConverter& converter,
    const units::length::meter_t& s,
    const units::length::meter_t& t_offset,
    const std::string& road_id,
    const road_logic_suite::types::RoadMark& road_mark,
    const road_logic_suite::types::LaneSection& section,
    const road_logic_suite::types::Lane::LaneId& lane_id)
{
    const auto t = road_logic_suite::GetBoundaryTValueAt(s, section, lane_id);
    const mantle_api::OpenDriveRoadPosition query_point{
        .road = road_id,
        .s_offset = s,
        .t_offset = t + t_offset,
    };

    if (auto position = converter.ConvertRoadPositionToInertialCoordinates(query_point))
    {
        return map_api::LaneBoundary::BoundaryPoint{
            .position = {position.value().x, position.value().y, position.value().z},
            .width = road_mark.width,
            .height = road_mark.height,
            .dash = map_api::LaneBoundary::BoundaryPoint::Dash::kOther,  ///< @todo Discuss and decide the default
                                                                         ///< boudary point type
        };
    }
    else
    {
        return std::nullopt;
    }
}

/// @brief Creates a reference line point at the defined position.
///
/// @param converter the coordinate converter.
/// @param s road reference line coordinate.
/// @param road_id the road id (string).
/// @return a new reference line point.
inline std::optional<map_api::ReferenceLinePoint> CreateReferenceLinePointAt(
    const road_logic_suite::CoordinateConverter& converter,
    const units::length::meter_t& s,
    const std::string& road_id)
{
    const mantle_api::OpenDriveRoadPosition query_point{
        .road = road_id,
        .s_offset = s,
        .t_offset = units::length::meter_t(0),
    };

    auto position = converter.ConvertRoadPositionToInertialCoordinates(query_point);
    auto heading = converter.GetRoadPositionHeading(road_id, s);

    if (position && heading)
    {
        return map_api::ReferenceLinePoint{.world_position = {position->x, position->y, position->z},
                                           .s_position = s,
                                           .t_axis_yaw = units::angle::radian_t(heading->value() + M_PI_2)};
    }

    return std::nullopt;
}

/// @brief Creates a logic boundary point at the defined position.
///
/// @param converter the coordinate converter.
/// @param s road reference line coordinate.
/// @param t_offset optional offset (i.e. for double boundaries like solid-solid).
/// @param road_id the road id (string).
/// @param section the current lane section.
/// @param lane_id the lane id.
/// @return a new logic boundary point.
inline std::optional<map_api::LogicalBoundaryPoint> CreateLogicBoundaryPointAt(
    const road_logic_suite::CoordinateConverter& converter,
    const units::length::meter_t& s,
    const units::length::meter_t& t_offset,
    const std::string& road_id,
    const road_logic_suite::types::LaneSection& section,
    const road_logic_suite::types::Lane::LaneId& lane_id)
{
    const auto t = road_logic_suite::GetBoundaryTValueAt(s, section, lane_id);
    const mantle_api::OpenDriveRoadPosition query_point{
        .road = road_id,
        .s_offset = s,
        .t_offset = t + t_offset,
    };

    if (auto position = converter.ConvertRoadPositionToInertialCoordinates(query_point))
    {
        return map_api::LogicalBoundaryPoint{.position = {position.value().x, position.value().y, position.value().z},
                                             .s_position = s,
                                             .t_position = t + t_offset};
    }
    else
    {
        return std::nullopt;
    }
}

/// @brief Calculates the lane offset at a given s position on a road.
///
/// @param road The road object.
/// @param s The s position on the road.
/// @return The lane offset at the given position.
inline units::length::meter_t CalculateLaneOffsetAt(const Road& road, const units::length::meter_t& s)
{
    if (s > road.length || s < units::length::meter_t(0))
    {
        throw std::runtime_error("[RoadLogicSuite: Lane Offset Calculation]: Invalid s value.");
    }
    const auto& laneoffsets = road.laneoffsets;
    if (const auto it = laneoffsets.Find(s); it != laneoffsets.end())
    {
        return units::length::meter_t(it->second.CalcAt(s.value()));
    }
    return units::length::meter_t(0);
}

inline map_api::LogicalLaneBoundary::PassingRule ConvertPassingRule(road_logic_suite::types::RoadMarkType type,
                                                                    types::LaneId lane_id)
{
    switch (type)
    {
        case road_logic_suite::types::RoadMarkType::UNKNOWN:
            return map_api::LogicalLaneBoundary::PassingRule::kUnknown;
        case road_logic_suite::types::RoadMarkType::BOTTS_DOTS:  // https://en.wikipedia.org/wiki/Botts%27_dots
            return map_api::LogicalLaneBoundary::PassingRule::kBothAllowed;
        case road_logic_suite::types::RoadMarkType::BROKEN_BROKEN:  // double lines. Inside Broken / Outside Broken,
                                                                    // exception: center lane – from left to right
            return map_api::LogicalLaneBoundary::PassingRule::kBothAllowed;
        case road_logic_suite::types::RoadMarkType::BROKEN_SOLID:  // double lines. Inside Broken / Outside Solid,
                                                                   // exception: center lane – from left to right
            return lane_id <= 0 ? map_api::LogicalLaneBoundary::PassingRule::kDecreasingT
                                : map_api::LogicalLaneBoundary::PassingRule::kIncreasingT;
        case road_logic_suite::types::RoadMarkType::BROKEN:
            return map_api::LogicalLaneBoundary::PassingRule::kBothAllowed;
        case road_logic_suite::types::RoadMarkType::CURB:
            return map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed;
        case road_logic_suite::types::RoadMarkType::CUSTOM:
            return map_api::LogicalLaneBoundary::PassingRule::kOther;
        case road_logic_suite::types::RoadMarkType::EDGE:
            return map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed;
        case road_logic_suite::types::RoadMarkType::GRASS:
            return map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed;
        case road_logic_suite::types::RoadMarkType::NONE:
            return map_api::LogicalLaneBoundary::PassingRule::kBothAllowed;
        case road_logic_suite::types::RoadMarkType::SOLID_BROKEN:  // double lines. Inside Solid / Outside Broken,
                                                                   // exception: center lane – from left to right
            return lane_id <= 0 ? map_api::LogicalLaneBoundary::PassingRule::kIncreasingT
                                : map_api::LogicalLaneBoundary::PassingRule::kDecreasingT;
        case road_logic_suite::types::RoadMarkType::SOLID_SOLID:  // double lines. Inside Solid / Outside Solid,
                                                                  // exception: center lane – from left to right
        case road_logic_suite::types::RoadMarkType::SOLID:
            return map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed;
        default:
            throw std::runtime_error("[RoadLogicSuite: Passing Rule Conversion]: Invalid road marking type.");
    }
}

/// @brief Converts an openDRIVE road mark type to the corresponding mantle lane boundary type. Since the mantle map
/// definition defines each lane boundary as it's own object, double lane boundaries like a broken and a solid line next
/// to each other must include the boolean to determine which one to return.
///
/// @param type the road mark type.
/// @param leftSide makes this method convert the left road mark type when true, or the right road mark type when false.
///  If the road mark type is not a double road mark, this is ignored. Please note that "left side" is a relative term.
///  For lanes which are not center lines, the inner side is considered the left side. For the center line, it runs from
///  left to right.
/// @return the corresponding mantle lane boundary type.
inline map_api::LaneBoundary::Type ConvertRoadMarkingsType(road_logic_suite::types::RoadMarkType type,
                                                           bool leftSide = true)
{
    switch (type)
    {
        case road_logic_suite::types::RoadMarkType::UNKNOWN:
            return map_api::LaneBoundary::Type::kUnknown;
        case road_logic_suite::types::RoadMarkType::BOTTS_DOTS:  // https://en.wikipedia.org/wiki/Botts%27_dots
            return map_api::LaneBoundary::Type::kBottsDots;
        case road_logic_suite::types::RoadMarkType::BROKEN_BROKEN:  // double lines. Left Broken / Right Broken
            return map_api::LaneBoundary::Type::kDashedLine;
        case road_logic_suite::types::RoadMarkType::BROKEN_SOLID:  // double lines. Left Broken / Right Solid
            return leftSide ? map_api::LaneBoundary::Type::kDashedLine : map_api::LaneBoundary::Type::kSolidLine;
        case road_logic_suite::types::RoadMarkType::BROKEN:
            return map_api::LaneBoundary::Type::kDashedLine;
        case road_logic_suite::types::RoadMarkType::CURB:
            return map_api::LaneBoundary::Type::kCurb;
        case road_logic_suite::types::RoadMarkType::CUSTOM:
            return map_api::LaneBoundary::Type::kOther;
        case road_logic_suite::types::RoadMarkType::EDGE:
            return map_api::LaneBoundary::Type::kRoadEdge;
        case road_logic_suite::types::RoadMarkType::GRASS:
            return map_api::LaneBoundary::Type::kGrassEdge;
        case road_logic_suite::types::RoadMarkType::NONE:
            return map_api::LaneBoundary::Type::kNoLine;
        case road_logic_suite::types::RoadMarkType::SOLID_BROKEN:  // double lines. Left Solid / Right Broken
            return leftSide ? map_api::LaneBoundary::Type::kSolidLine : map_api::LaneBoundary::Type::kDashedLine;
        case road_logic_suite::types::RoadMarkType::SOLID_SOLID:  // double lines. Left Solid / Right Solid
        case road_logic_suite::types::RoadMarkType::SOLID:
            return map_api::LaneBoundary::Type::kSolidLine;
        default:
            throw std::runtime_error("Invalid road marking type.");
    }
}

/// @brief Converts OpenDRIVE style roadmark colors to map_api lane boundary colors. 1:1 conversion from one enum to
/// another.
/// @param color the OpenDRIVE color.
/// @return the map_api lane boundary color.

inline map_api::LaneBoundary::Color ConvertRoadMarkingsColor(road_logic_suite::types::RoadMarkColor color)
{
    switch (color)
    {
        case road_logic_suite::types::RoadMarkColor::UNKNOWN:
            return map_api::LaneBoundary::Color::kUnknown;
        case road_logic_suite::types::RoadMarkColor::BLUE:
            return map_api::LaneBoundary::Color::kBlue;
        case road_logic_suite::types::RoadMarkColor::GREEN:
            return map_api::LaneBoundary::Color::kGreen;
        case road_logic_suite::types::RoadMarkColor::ORANGE:
            return map_api::LaneBoundary::Color::kOrange;
        case road_logic_suite::types::RoadMarkColor::RED:
            return map_api::LaneBoundary::Color::kRed;
        case road_logic_suite::types::RoadMarkColor::STANDARD:
            return map_api::LaneBoundary::Color::kStandard;
        case road_logic_suite::types::RoadMarkColor::VIOLET:
            return map_api::LaneBoundary::Color::kViolet;
        case road_logic_suite::types::RoadMarkColor::WHITE:
            return map_api::LaneBoundary::Color::kWhite;
        case road_logic_suite::types::RoadMarkColor::YELLOW:
            return map_api::LaneBoundary::Color::kYellow;
        default:
            throw std::runtime_error("Invalid road marking color.");
    }
}

/// @brief Converts stationary object types by type description.
/// @param s the type description.
/// @return the stationary object type.
inline map_api::StationaryObject::Type ConvertStationaryObjectType(const std::string& s)
{
    const static std::unordered_map<std::string, map_api::StationaryObject::Type> object_type_map{
        // type names source:
        // https://github.com/pyoscx/scenariogeneration/blob/main/scenariogeneration/xodr/enumerations.pyconst
        {"barrier", map_api::StationaryObject::Type::kBarrier},
        {"building", map_api::StationaryObject::Type::kBuilding},
        {"crosswalk", map_api::StationaryObject::Type::kOther},
        {"gantry", map_api::StationaryObject::Type::kOverheadStructure},
        {"none", map_api::StationaryObject::Type::kOther},
        {"obstacle", map_api::StationaryObject::Type::kOther},
        {"parkingspace", map_api::StationaryObject::Type::kOther},
        {"patch", map_api::StationaryObject::Type::kOther},
        {"pole", map_api::StationaryObject::Type::kPole},
        {"railing", map_api::StationaryObject::Type::kBarrier},
        {"roadmark", map_api::StationaryObject::Type::kOther},
        {"trafficisland", map_api::StationaryObject::Type::kOther},
        {"tree", map_api::StationaryObject::Type::kTree},
        {"vegetation", map_api::StationaryObject::Type::kVegetation},
        {"soundbarrier", map_api::StationaryObject::Type::kWall},
        {"streetlamp", map_api::StationaryObject::Type::kPole},
        {"railing", map_api::StationaryObject::Type::kBarrier},
    };

    if (auto it = object_type_map.find(road_logic_suite::utils::ToLower(s)); it != object_type_map.end())
    {
        return it->second;
    }
    std::cerr << "[RoadLogicSuite: StationaryObjectType Conversion] WARNING: Invalid StationaryObjectType input: " << s
              << ", please check your map file." << std::endl;

    return map_api::StationaryObject::Type::kUnknown;
}

/// @brief Sets proper map_api lane type according to the OpenDRIVE definition.
///
/// @param lane the map_api lane.
/// @param lane_type the lane type (string).
inline void SetLaneType(map_api::Lane& lane, const std::string& lane_type)
{
    const auto lane_type_str = road_logic_suite::utils::ToLower(lane_type);
    if (lane_type_str == "driving" ||  //
        lane_type_str == "exit" ||     //
        lane_type_str == "entry" ||    //
        lane_type_str == "onramp" ||   //
        lane_type_str == "offramp" ||  //
        lane_type_str == "biking" ||   //
        lane_type_str == "connectingramp")
    {
        lane.type = map_api::Lane::Type::kDriving;
    }
    else if (lane_type_str == "shoulder" ||    //
             lane_type_str == "stop" ||        //
             lane_type_str == "parking" ||     //
             lane_type_str == "restricted" ||  //
             lane_type_str == "median" ||      //
             lane_type_str == "sidewalk" ||    //
             lane_type_str == "curb")
    {
        lane.type = map_api::Lane::Type::kNonDriving;
    }
    else
    {
        lane.type = map_api::Lane::Type::kUnknown;
    }
}

/// @brief Checks if road belongs to junction.
///
/// @param road_assigned_junction_id the junction id assigned to the road (string).
inline bool IsJunction(const std::string& road_assigned_junction_id)
{
    return !(road_assigned_junction_id == "-1" || road_assigned_junction_id == "");
}

/// @brief Sets lane type to Intersection.
///
/// @param lane the map_api lane.
inline void SetLaneToIntersectionType(map_api::Lane& lane)
{
    lane.type = map_api::Lane::Type::kIntersection;
}

/// @brief Sets proper map_api lane subtype according to the OpenDRIVE definition.
///
/// @param lane the map_api lane.
/// @param lane_subtype the lane subtype (string).
inline void SetLaneSubtype(map_api::Lane& lane, const std::string& lane_subtype)
{
    const auto lane_subtype_str = road_logic_suite::utils::ToLower(lane_subtype);
    if (lane_subtype_str == "shoulder")
    {
        lane.sub_type = map_api::Lane::Subtype::kShoulder;
    }
    else if (lane_subtype_str == "border")
    {
        lane.sub_type = map_api::Lane::Subtype::kBorder;
    }
    else if (lane_subtype_str == "driving")
    {
        lane.sub_type = map_api::Lane::Subtype::kNormal;
    }
    else if (lane_subtype_str == "stop")
    {
        lane.sub_type = map_api::Lane::Subtype::kStop;
    }
    else if (lane_subtype_str == "none")
    {
        lane.sub_type = map_api::Lane::Subtype::kOther;
    }
    else if (lane_subtype_str == "restricted")
    {
        lane.sub_type = map_api::Lane::Subtype::kRestricted;
    }
    else if (lane_subtype_str == "parking")
    {
        lane.sub_type = map_api::Lane::Subtype::kParking;
    }
    else if (lane_subtype_str == "median")
    {
        lane.sub_type = map_api::Lane::Subtype::kOther;
    }
    else if (lane_subtype_str == "biking")
    {
        lane.sub_type = map_api::Lane::Subtype::kBiking;
    }
    else if (lane_subtype_str == "sidewalk")
    {
        lane.sub_type = map_api::Lane::Subtype::kSidewalk;
    }
    else if (lane_subtype_str == "curb")
    {
        lane.sub_type = map_api::Lane::Subtype::kOther;
    }
    else if (lane_subtype_str == "exit")
    {
        lane.sub_type = map_api::Lane::Subtype::kExit;
    }
    else if (lane_subtype_str == "entry")
    {
        lane.sub_type = map_api::Lane::Subtype::kEntry;
    }
    else if (lane_subtype_str == "onramp")
    {
        lane.sub_type = map_api::Lane::Subtype::kOnRamp;
    }
    else if (lane_subtype_str == "offramp")
    {
        lane.sub_type = map_api::Lane::Subtype::kOffRamp;
    }
    else if (lane_subtype_str == "connectingramp")
    {
        lane.sub_type = map_api::Lane::Subtype::kConnectingRamp;
    }
    else
    {
        lane.sub_type = map_api::Lane::Subtype::kUnknown;
    }
}

/// @brief Sets proper map_api logical lane type according to the OpenDRIVE definition.
///
/// @param logical_lane the map_api logical lane.
/// @param lane_subtype the lane subtype (string).
inline void SetLogicalLaneType(map_api::LogicalLane& logical_lane, const std::string& lane_subtype)
{
    const auto lane_subtype_str = road_logic_suite::utils::ToLower(lane_subtype);

    if (lane_subtype_str == "unknown")
    {
        logical_lane.type = map_api::LogicalLane::Type::kUnknown;
    }
    else if (lane_subtype_str == "other")
    {
        logical_lane.type = map_api::LogicalLane::Type::kOther;
    }
    else if (lane_subtype_str == "normal")
    {
        logical_lane.type = map_api::LogicalLane::Type::kNormal;
    }
    else if (lane_subtype_str == "biking")
    {
        logical_lane.type = map_api::LogicalLane::Type::kBiking;
    }
    else if (lane_subtype_str == "sidewalk")
    {
        logical_lane.type = map_api::LogicalLane::Type::kSidewalk;
    }
    else if (lane_subtype_str == "parking")
    {
        logical_lane.type = map_api::LogicalLane::Type::kParking;
    }
    else if (lane_subtype_str == "stop")
    {
        logical_lane.type = map_api::LogicalLane::Type::kStop;
    }
    else if (lane_subtype_str == "restricted")
    {
        logical_lane.type = map_api::LogicalLane::Type::kRestricted;
    }
    else if (lane_subtype_str == "border")
    {
        logical_lane.type = map_api::LogicalLane::Type::kBorder;
    }
    else if (lane_subtype_str == "shoulder")
    {
        logical_lane.type = map_api::LogicalLane::Type::kShoulder;
    }
    else if (lane_subtype_str == "exit")
    {
        logical_lane.type = map_api::LogicalLane::Type::kExit;
    }
    else if (lane_subtype_str == "entry")
    {
        logical_lane.type = map_api::LogicalLane::Type::kEntry;
    }
    else if (lane_subtype_str == "onramp" || lane_subtype_str == "on_ramp")
    {
        logical_lane.type = map_api::LogicalLane::Type::kOnramp;
    }
    else if (lane_subtype_str == "offramp" || lane_subtype_str == "off_ramp")
    {
        logical_lane.type = map_api::LogicalLane::Type::kOfframp;
    }
    else if (lane_subtype_str == "connectingramp" || lane_subtype_str == "connecting_ramp")
    {
        logical_lane.type = map_api::LogicalLane::Type::kConnectingramp;
    }
    else if (lane_subtype_str == "median")
    {
        logical_lane.type = map_api::LogicalLane::Type::kMedian;
    }
    else if (lane_subtype_str == "curb")
    {
        logical_lane.type = map_api::LogicalLane::Type::kCurb;
    }
    else if (lane_subtype_str == "rail")
    {
        logical_lane.type = map_api::LogicalLane::Type::kRail;
    }
    else if (lane_subtype_str == "tram")
    {
        logical_lane.type = map_api::LogicalLane::Type::kTram;
    }
    else
    {
        // Default to unknown if the string doesn't match any known subtype
        logical_lane.type = map_api::LogicalLane::Type::kUnknown;
    }
}

/// @brief Sets proper map_api logical lane moving direction according to the OpenDRIVE definition, only support
/// standard direction currently.
///
/// @param logical_lane the map_api logical lane.
/// @param is_left_side the lane subtype (string).
inline void SetLogicalLaneMovingDirection(map_api::LogicalLane& logical_lane, const bool is_left_side = false)
{
    logical_lane.move_direction = is_left_side ? map_api::LogicalLane::MoveDirection::kDecreasingS
                                               : map_api::LogicalLane::MoveDirection::kIncreasingS;
}

/// @brief Sets proper map_api logical lane start s and end s value according to the OpenDRIVE definition.
/// @param logical_lane the map_api logical lane.
/// @param start_s start value of s coordinate.
/// @param end_s end value of s coordinate.
inline void SetLogicalLaneSValue(map_api::LogicalLane& logical_lane,
                                 const units::length::meter_t start_s,
                                 const units::length::meter_t end_s)
{
    logical_lane.start_s = start_s;
    logical_lane.end_s = end_s;
}

/// @brief Sets proper map_api logical lane's physical lane references according to the OpenDRIVE definition.
/// @param logical_lane the map_api logical lane to be set.
/// @param physical_lane the map_api reference physical lane for the logical lane.
/// @param start_s start value of s coordinate of referenced physical lane.
/// @param end_s end value of s coordinate of referenced physical lane.
inline void SetPhysicalLaneRefs(map_api::LogicalLane& logical_lane,
                                map_api::Lane& physical_lane,
                                const units::length::meter_t start_s,
                                const units::length::meter_t end_s)
{
    logical_lane.physical_lane_references.push_back(map_api::PhysicalLaneReference{physical_lane, start_s, end_s});
}

/// @brief Checks whether a road mark is considered a "double line", i.e. two solid lines next to each other.
/// @param type the road mark type.
/// @return true when the road mark is double lined, false otherwise.
inline bool IsRoadMarkTypeDoubleLined(road_logic_suite::types::RoadMarkType type)
{
    switch (type)
    {
        case road_logic_suite::types::RoadMarkType::UNKNOWN:
        case road_logic_suite::types::RoadMarkType::BOTTS_DOTS:  // https://en.wikipedia.org/wiki/Botts%27_dots
            return false;
        case road_logic_suite::types::RoadMarkType::BROKEN_BROKEN:  // double lines. Left Broken / Right Broken
        case road_logic_suite::types::RoadMarkType::BROKEN_SOLID:   // double lines. Left Broken / Right Solid
            return true;
        case road_logic_suite::types::RoadMarkType::BROKEN:
        case road_logic_suite::types::RoadMarkType::CURB:
        case road_logic_suite::types::RoadMarkType::CUSTOM:
        case road_logic_suite::types::RoadMarkType::EDGE:
        case road_logic_suite::types::RoadMarkType::GRASS:
        case road_logic_suite::types::RoadMarkType::NONE:
            return false;
        case road_logic_suite::types::RoadMarkType::SOLID_BROKEN:  // double lines. Left Solid / Right Broken
        case road_logic_suite::types::RoadMarkType::SOLID_SOLID:   // double lines. Left Solid / Right Solid
            return true;
        case road_logic_suite::types::RoadMarkType::SOLID:
            return false;
        default:
            throw std::runtime_error("Invalid road marking type.");
    }
}

/// @brief Converts road barrier type by type string.
///
/// @param str the type string.
/// @return the road barrier type.
inline map_api::LaneBoundary::Type ConvertRoadBarrierType(const std::string& str)
{
    const auto lower = road_logic_suite::utils::ToLower(str);
    if (lower == "guardrail")
    {
        return map_api::LaneBoundary::Type::kGuardRail;
    }
    return map_api::LaneBoundary::Type::kBarrier;
}

inline std::vector<map_api::RefWrapper<map_api::LaneBoundary>> GetLaneBoundariesRefs(
    const map_api::Map::LaneBoundaries& lane_boundaries)
{
    std::vector<map_api::RefWrapper<map_api::LaneBoundary>> lane_boundaries_refs{};
    for (const auto& lane_boundary : lane_boundaries)
    {
        lane_boundaries_refs.push_back(std::ref(*lane_boundary));
    }

    return lane_boundaries_refs;
}

inline std::vector<map_api::RefWrapper<map_api::LogicalLaneBoundary>> GetLogicalLaneBoundariesRefs(
    const map_api::Map::LogicalLaneBoundaries& lane_boundaries)
{
    std::vector<map_api::RefWrapper<map_api::LogicalLaneBoundary>> lane_boundaries_refs{};
    for (const auto& lane_boundary : lane_boundaries)
    {
        lane_boundaries_refs.push_back(std::ref(*lane_boundary));
    }

    return lane_boundaries_refs;
}

inline std::vector<map_api::RefWrapper<map_api::Lane>> GetLanesRefs(const map_api::Map::Lanes& lanes)
{
    std::vector<map_api::RefWrapper<map_api::Lane>> lanes_refs{};
    for (const auto& lane : lanes)
    {
        lanes_refs.push_back(std::ref(*lane));
    }

    return lanes_refs;
}

inline std::optional<map_api::RefWrapper<map_api::Lane>> FindLaneWithId(const map_api::Map::Lanes& lanes,
                                                                        const map_api::Identifier lane_id)
{
    const auto found_lane_itr =
        std::find_if(std::begin(lanes), std::end(lanes), [lane_id](const std::unique_ptr<map_api::Lane>& current_lane) {
            return current_lane->id == lane_id;
        });

    if (found_lane_itr == std::end(lanes))
    {
        return std::nullopt;
    }

    return std::ref(**found_lane_itr);
}

inline std::optional<map_api::RefWrapper<map_api::LogicalLane>> CreateLogicalLaneRefWithId(
    const map_api::Map::LogicalLanes& logical_lanes,
    const map_api::Identifier logical_lane_id)
{
    const auto found_lane_itr =
        std::find_if(std::begin(logical_lanes),
                     std::end(logical_lanes),
                     [logical_lane_id](const std::unique_ptr<map_api::LogicalLane>& current_lane) {
                         return current_lane->id == logical_lane_id;
                     });

    if (found_lane_itr == std::end(logical_lanes))
    {
        return std::nullopt;
    }

    return std::ref(**found_lane_itr);
}

inline std::optional<map_api::LogicalLaneRelation> CreateLogicalLaneRelationWithId(
    const map_api::Map::LogicalLanes& logical_lanes,
    map_api::LogicalLane& logical_lane,
    const map_api::Identifier logical_lane_id)
{
    auto found_lane_itr = std::find_if(logical_lanes.begin(),
                                       logical_lanes.end(),
                                       [logical_lane_id](const std::unique_ptr<map_api::LogicalLane>& current_lane) {
                                           return current_lane->id == logical_lane_id;
                                       });

    if (found_lane_itr == std::end(logical_lanes))
    {
        return std::nullopt;
    }
    auto& found_lane = **found_lane_itr;

    return map_api::LogicalLaneRelation{
        std::ref(found_lane), logical_lane.start_s, logical_lane.end_s, found_lane.start_s, found_lane.end_s};
}

std::vector<mantle_api::UniqueId> GetMantleMapLaneIdFromOpenDriveLaneIds(
    const road_logic_suite::OpenDriveData& data,
    const std::string& road_id,
    const std::vector<road_logic_suite::types::LaneId>& lane_ids,
    road_logic_suite::map_conversion::MantleIdProvider& id_provider);

std::vector<map_api::RefWrapper<map_api::Lane>> CreateAssignedLanes(
    const map_api::Map& map,
    const std::vector<mantle_api::UniqueId>& mantle_lane_ids);

struct LaneRelationIds
{
    using LanesIds = std::vector<mantle_api::UniqueId>;

    LanesIds antecessor_lane_ids{};
    LanesIds successor_lane_ids{};
    LanesIds left_adjacent_lane_ids{};
    LanesIds right_adjacent_lane_ids{};
};

//  The adjacent lane for logical lane is in definition direction (not driving direction). So a different structure to
//  store the relations for logical lanes is used.
struct LogicalLaneRelationIds
{
    using LanesIds = std::vector<mantle_api::UniqueId>;
    LanesIds left_adjacent_lane_ids{};
    LanesIds right_adjacent_lane_ids{};
};

}  // namespace road_logic_suite::map_conversion::utils

#endif  // ROADLOGICSUITE_MANTLE_MAP_CONVERTERUTILS_H
