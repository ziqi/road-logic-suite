/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_MAP_CONVERTER_H
#define ROADLOGICSUITE_MANTLE_MAP_CONVERTER_H

#include "RoadLogicSuite/Internal/MapConversion/lane_info.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"
#include "RoadLogicSuite/Internal/coordinate_converter.h"
#include "RoadLogicSuite/Internal/i_map_converter.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"
#include "RoadLogicSuite/map_converter_config.h"

#include <MapAPI/i_map_loader.h>

namespace road_logic_suite::map_conversion
{
/// @brief Converts the internal data storage to a mantle map.

class MantleMapConverter : public IMapConverter<map_api::Map>
{
  public:
    MantleMapConverter() = default;
    explicit MantleMapConverter(MapConverterConfig config) : config_(config) {}

    /// @brief Converts the internal data storage to the mantle map format.
    /// @param data_storage the data storage, provided by the road logic suite.
    /// @return the mantle map.
    [[nodiscard]] map_api::Map Convert(const OpenDriveData& data_storage) override;

    /// @brief To be used after conversion. This can be helpful i.e. for tests, as a lanes MantleAPI Id is not
    /// guaranteed to be deterministic on all platforms and standard library implementations.
    /// @remark If the lane has not been created yet, i.e. because Convert() was not called, yet, then it will still
    ///  return a new id, but it may not correspond to a lane in a later converted map.
    /// @param info the lane info.
    /// @return the (reserved) lane identifier.
    mantle_api::UniqueId GetLaneId(const LaneInfo& info);

  private:
    void ConvertRoad(const Road& road, const OpenDriveData& data_storage);
    std::unique_ptr<map_api::LaneBoundary> CreateBoundary(const Road& road,
                                                          const CoordinateConverter& converter,
                                                          const units::length::meter_t& s_start,
                                                          const units::length::meter_t& s_end,
                                                          const units::length::meter_t& t_offset,
                                                          const types::LaneSection& section,
                                                          const types::RoadMark& road_mark,
                                                          const types::Lane::LaneId& lane_id);
    std::vector<map_api::LogicalBoundaryPoint> CreateLogicalBoundaryLine(const CoordinateConverter& converter,
                                                                         const Road& road,
                                                                         const types::LaneSection& section,
                                                                         const types::Lane::LaneId& lane_id,
                                                                         const units::length::meter_t& s_start,
                                                                         const units::length::meter_t& s_end,
                                                                         const units::length::meter_t& t_offset) const;

    std::pair<map_api::Map::Lanes, map_api::Map::LogicalLanes> CreateLanes(const OpenDriveData& data_storage,
                                                                           const CoordinateConverter& converter,
                                                                           const Road& road,
                                                                           const types::LaneSection& section,
                                                                           bool is_left_side);

    void CreateLaneRelations();

    void CreateLaneReferenceFromId(std::vector<map_api::RefWrapper<map_api::Lane>>& lanes,
                                   const std::vector<uint64_t>& lane_ids) const;
    void CreateLogicalLaneReferenceFromId(std::vector<map_api::RefWrapper<map_api::LogicalLane>>& logical_lanes,
                                          const std::vector<uint64_t>& lane_ids) const;
    void CreateLogicalLaneRelationFromId(std::vector<map_api::LogicalLaneRelation>& logical_lanes,
                                         map_api::LogicalLane& logical_lane,
                                         const std::vector<uint64_t>& lane_ids) const;

    std::unique_ptr<map_api::ReferenceLine> CreateReferenceLine(const CoordinateConverter& converter,
                                                                const Road& road,
                                                                const units::length::meter_t& s_start,
                                                                const units::length::meter_t& s_end);

    std::pair<map_api::Map::LaneBoundaries, map_api::Map::LogicalLaneBoundaries> CreateLaneBoundaries(
        const CoordinateConverter& converter,
        const Road& road,
        const types::LaneSection& section,
        map_api::Map::Lanes& physical_lanes,
        map_api::Map::LogicalLanes& logical_lanes,
        const map_api::Map::LaneBoundaries& center_boundaries,
        const map_api::Map::LogicalLaneBoundaries& logical_center_boundaries,
        const std::unique_ptr<map_api::ReferenceLine>& reference_line,
        const bool is_left_side);

    std::pair<map_api::Map::LaneBoundaries, map_api::Map::LogicalLaneBoundaries> CreateLaneBoundariesForLane(
        const CoordinateConverter& coordinate_converter,
        const Road& road,
        const types::LaneSection& section,
        const types::Lane& lane,
        const std::unique_ptr<map_api::ReferenceLine>& reference_line);

    std::pair<map_api::Map::LaneBoundaries, std::vector<map_api::LogicalBoundaryPoint>> CreateLaneBoundariesForRoadMark(
        const CoordinateConverter& converter,
        const Road& road,
        const types::LaneSection& section,
        const types::Lane& lane,
        const types::RangeBasedMap<units::length::meter_t, types::RoadMark>::ConstIterator& road_mark_it);

    void StoreNeighbourLanes(const Road& road,
                             const types::LaneSection& section,
                             const types::Lane& lane,
                             utils::LaneRelationIds& lane_relation_ids);
    void StoreNeighbourLanesForLogicalLane(const Road& road,
                                           const types::LaneSection& section,
                                           const types::Lane& lane,
                                           utils::LogicalLaneRelationIds& lane_relation_ids);

    [[nodiscard]] std::vector<mantle_api::Vec3<units::length::meter_t>> CreateCenterLineGeometry(
        const CoordinateConverter& converter,
        const Road& road,
        const types::LaneSection& section,
        const road_logic_suite::types::Lane::LaneId& lane_id) const;

    MapConverterConfig config_{};
    MantleIdProvider id_provider_{};
    MantleIdProvider id_provider_lane_group_{};
    std::map<mantle_api::UniqueId, utils::LaneRelationIds> id_to_lane_relation_{};
    std::map<mantle_api::UniqueId, utils::LogicalLaneRelationIds> id_to_logical_lane_relation_{};
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> physical_lane_id_to_logical_lane_id_{};
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> logical_lane_id_to_physical_lane_id_{};
    map_api::Map map_{};
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_MANTLE_MAP_CONVERTER_H
