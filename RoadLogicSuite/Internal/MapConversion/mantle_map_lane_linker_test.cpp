/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_lane_linker.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_lane_linker.h"
#include "RoadLogicSuite/Internal/road.h"

#include <MantleAPI/Map/lane_definition.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace
{
using namespace road_logic_suite::map_conversion;
using road_logic_suite::map_conversion::MantleMapLaneLinker;
using IdToLaneRelation = std::map<mantle_api::LaneId, utils::LaneRelationIds>;

auto CreateSimpleRoadWithPredecessorAndSuccessor(int lane_id)
{
    road_logic_suite::Road road{.id = "0"};
    road_logic_suite::types::Lane predecessor_lane{.id = lane_id, .successors = {lane_id}};
    road_logic_suite::types::Lane successor_lane{.id = lane_id, .predecessors = {lane_id}};
    if (lane_id < 0)
    {
        road_logic_suite::types::LaneSection predecessor_lane_section{.s_start = units::length::meter_t(0),
                                                                      .right_lanes = {predecessor_lane}};
        road_logic_suite::types::LaneSection successor_lane_section{.s_start = units::length::meter_t(50),
                                                                    .right_lanes = {successor_lane}};
        road.lane_sections.Insert(predecessor_lane_section.s_start, predecessor_lane_section);
        road.lane_sections.Insert(successor_lane_section.s_start, successor_lane_section);
    }
    else if (lane_id > 0)
    {
        road_logic_suite::types::LaneSection predecessor_lane_section{.s_start = units::length::meter_t(0),
                                                                      .left_lanes = {predecessor_lane}};
        road_logic_suite::types::LaneSection successor_lane_section{.s_start = units::length::meter_t(50),
                                                                    .left_lanes = {successor_lane}};
        road.lane_sections.Insert(predecessor_lane_section.s_start, predecessor_lane_section);
        road.lane_sections.Insert(successor_lane_section.s_start, successor_lane_section);
    }
    return road;
}

TEST(LaneLinkerTest, GivenLaneWithSuccessorOnSameRoad_WhenLinkingLane_ThenSuccessorMustBeSet)
{
    // arrange
    const auto lane_id = -1;
    const auto road = CreateSimpleRoadWithPredecessorAndSuccessor(lane_id);
    const units::length::meter_t predecessor_lane_section_s0(0);
    const auto predecessor_lane_section = road.lane_sections.At(predecessor_lane_section_s0);
    const auto& predecessor_lane = predecessor_lane_section.GetLaneById(lane_id);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks({}, road, predecessor_lane_section, predecessor_lane, id_provider, lane_relation);

    // assert
    ASSERT_EQ(1, lane_relation.successor_lane_ids.size());
    EXPECT_EQ(1, lane_relation.successor_lane_ids[0]);
}

TEST(LaneLinkerTest, GivenLaneWithPredecessorOnSameRoad_WhenLinkingLane_ThenPredecessorMustBeSet)
{
    // arrange
    const auto lane_id = -1;
    const auto road = CreateSimpleRoadWithPredecessorAndSuccessor(lane_id);
    const units::length::meter_t successor_lane_section_s0(50);
    const auto successor_lane_section = road.lane_sections.At(successor_lane_section_s0);
    const auto& successor_lane = successor_lane_section.GetLaneById(lane_id);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks({}, road, successor_lane_section, successor_lane, id_provider, lane_relation);

    // assert
    ASSERT_EQ(1, lane_relation.antecessor_lane_ids.size());
    EXPECT_EQ(1, lane_relation.antecessor_lane_ids[0]);
}

// On a left handed lane (positive id) the predecessors and successors must be switched.
TEST(LaneLinkerTest, GivenLaneWithSuccessorOnSameRoadLeftHanded_WhenLinkingLane_ThenSuccessorMustBeSet)
{
    // arrange
    const auto lane_id = 1;
    const auto road = CreateSimpleRoadWithPredecessorAndSuccessor(lane_id);
    const units::length::meter_t predecessor_lane_section_s0(0);
    const auto predecessor_lane_section = road.lane_sections.At(predecessor_lane_section_s0);
    const auto& predecessor_lane = predecessor_lane_section.GetLaneById(lane_id);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks({}, road, predecessor_lane_section, predecessor_lane, id_provider, lane_relation);

    // assert
    ASSERT_EQ(1, lane_relation.antecessor_lane_ids.size());
    EXPECT_EQ(1, lane_relation.antecessor_lane_ids[0]);
}

// On a left handed lane (positive id) the predecessors and successors must be switched.
TEST(LaneLinkerTest, GivenLaneWithPredecessorOnSameRoadLeftHanded_WhenLinkingLane_ThenPredecessorMustBeSet)
{
    // arrange
    const auto lane_id = 1;
    const auto road = CreateSimpleRoadWithPredecessorAndSuccessor(lane_id);
    const units::length::meter_t successor_lane_section_s0(50);
    const auto successor_lane_section = road.lane_sections.At(successor_lane_section_s0);
    const auto& successor_lane = successor_lane_section.GetLaneById(lane_id);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks({}, road, successor_lane_section, successor_lane, id_provider, lane_relation);

    // assert
    ASSERT_EQ(1, lane_relation.successor_lane_ids.size());
    EXPECT_EQ(1, lane_relation.successor_lane_ids[0]);
}

/**
 * Also see: https://releases.asam.net/OpenDRIVE/1.6.0/ASAM_OpenDRIVE_BS_V1-6-0.html#_road_linkage Fig. 36 upper right
 * @verbatim OpenDrive:
 *        Road "0"                Road "1"
 * ← ╠═════════════════════╣╠═════════════════════╣ → Reference Line
 *   ║    Lane 1           ║║     Lane -1         ║
 *   ╟─────────────────────╢╟─────────────────────╢
 * Note: Driving direction is still to the right
 * @endverbatim
 */
road_logic_suite::OpenDriveData CreateTwoRoadConnectedAtOrigin()
{
    road_logic_suite::types::LaneSection lane_section_road_0{.left_lanes = {{.id = 1, .predecessors = {-1}}}};
    road_logic_suite::Road road_0{.id = "0",
                                  .road_links = {{.target_id = "1",
                                                  .link_type = road_logic_suite::RoadLinkType::kPredecessor,
                                                  .contact_type = road_logic_suite::LinkContactType::kStart}}};
    road_0.lane_sections.Insert(units::length::meter_t(0), lane_section_road_0);

    road_logic_suite::types::LaneSection lane_section_road_1{.right_lanes = {{.id = -1, .predecessors = {1}}}};
    road_logic_suite::Road road_1{.id = "1",
                                  .road_links = {{.target_id = "0",
                                                  .link_type = road_logic_suite::RoadLinkType::kPredecessor,
                                                  .contact_type = road_logic_suite::LinkContactType::kStart}}};
    road_1.lane_sections.Insert(units::length::meter_t(0), lane_section_road_1);

    return {.roads = {{"0", road_0}, {"1", road_1}}};
}

TEST(LaneLinkerTest, GivenLeftLaneWithPredecessorWithStartContactType_WhenLinkingLane_ThenSuccessorMustBeSet)
{
    const road_logic_suite::OpenDriveData data = CreateTwoRoadConnectedAtOrigin();
    const auto road = data.roads.at("0");
    const auto lane_section = road.lane_sections.At(units::length::meter_t(0));
    const auto lane_id = 1;
    const auto& lane = lane_section.GetLaneById(lane_id);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks({}, road, lane_section, lane, id_provider, lane_relation);

    // assert
    const auto successor_id = id_provider.GetOrGenerateLaneId({"1", 0, -1});
    ASSERT_THAT(lane_relation.successor_lane_ids, ::testing::ElementsAre(successor_id));
    ASSERT_THAT(lane_relation.antecessor_lane_ids, ::testing::IsEmpty());
}

TEST(LaneLinkerTest, GivenRightLaneWithPredecessorWithStartContactType_WhenLinkingLane_ThenPredecessorMustBeSet)
{
    const road_logic_suite::OpenDriveData data = CreateTwoRoadConnectedAtOrigin();
    const auto road = data.roads.at("1");
    const auto lane_section = road.lane_sections.At(units::length::meter_t(0));
    const auto lane_id = -1;
    const auto& lane = lane_section.GetLaneById(lane_id);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks({}, road, lane_section, lane, id_provider, lane_relation);

    // assert
    const auto predecessor_id = id_provider.GetOrGenerateLaneId({"0", 0, 1});
    ASSERT_THAT(lane_relation.antecessor_lane_ids, ::testing::ElementsAre(predecessor_id));
    ASSERT_THAT(lane_relation.successor_lane_ids, ::testing::IsEmpty());
}

/**
 * Also see: https://releases.asam.net/OpenDRIVE/1.6.0/ASAM_OpenDRIVE_BS_V1-6-0.html#_road_linkage Fig. 36 lower left
 * @verbatim OpenDrive:
 *        Road "0"                Road "1"
 * ╠═════════════════════╣  ╠═════════════════════╣  Reference Line
 * ║    Lane -1    ->    ║→←║     Lane 1    ->    ║
 * ╟─────────────────────╢  ╟─────────────────────╢
 * Note: Driving direction is still to the right
 * @endverbatim
 */
road_logic_suite::OpenDriveData CreateTwoRoadConnectedHeadOn()
{
    road_logic_suite::types::LaneSection lane_section_road_0{.right_lanes = {{.id = -1, .successors = {1}}}};
    road_logic_suite::Road road_0{.id = "0",
                                  .road_links = {{.target_id = "1",
                                                  .link_type = road_logic_suite::RoadLinkType::kSuccessor,
                                                  .contact_type = road_logic_suite::LinkContactType::kEnd}}};
    road_0.lane_sections.Insert(units::length::meter_t(0), lane_section_road_0);

    road_logic_suite::types::LaneSection lane_section_road_1{.left_lanes = {{.id = 1, .successors = {-1}}}};
    road_logic_suite::Road road_1{.id = "1",
                                  .road_links = {{.target_id = "0",
                                                  .link_type = road_logic_suite::RoadLinkType::kSuccessor,
                                                  .contact_type = road_logic_suite::LinkContactType::kEnd}}};
    road_1.lane_sections.Insert(units::length::meter_t(0), lane_section_road_1);

    return {.roads = {{"0", road_0}, {"1", road_1}}};
}

TEST(LaneLinkerTest, GivenRightLaneWithSuccessorWithEndContactType_WhenLinkingLane_ThenSuccessorMustBeSet)
{
    const road_logic_suite::OpenDriveData data = CreateTwoRoadConnectedHeadOn();
    const auto road = data.roads.at("0");
    const auto lane_section = road.lane_sections.At(units::length::meter_t(0));
    const auto lane_id = -1;
    const auto& lane = lane_section.GetLaneById(-1);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks(data, road, lane_section, lane, id_provider, lane_relation);

    // assert
    const auto successor_id = id_provider.GetOrGenerateLaneId({"1", 0, 1});
    ASSERT_THAT(lane_relation.antecessor_lane_ids, ::testing::IsEmpty());
    ASSERT_THAT(lane_relation.successor_lane_ids, ::testing::ElementsAre(successor_id));
}

TEST(LaneLinkerTest, GivenLeftLaneWithSuccessorWithEndContactType_WhenLinkingLane_ThenPredecessorMustBeSet)
{
    const road_logic_suite::OpenDriveData data = CreateTwoRoadConnectedHeadOn();
    const auto road = data.roads.at("1");
    const auto lane_section = road.lane_sections.At(units::length::meter_t(0));
    const auto lane_id = 1;
    const auto& lane = lane_section.GetLaneById(1);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    // act
    MantleMapLaneLinker::StoreLinks(data, road, lane_section, lane, id_provider, lane_relation);

    // assert
    const auto predecessor_id = id_provider.GetOrGenerateLaneId({"0", 0, -1});
    ASSERT_THAT(lane_relation.successor_lane_ids, ::testing::IsEmpty());
    ASSERT_THAT(lane_relation.antecessor_lane_ids, ::testing::ElementsAre(predecessor_id));
}

road_logic_suite::OpenDriveData CreateRoadWithEmptyPredecessorAndSuccessor()
{
    road_logic_suite::types::LaneSection lane_section_road_0{.right_lanes = {{.id = -1, .successors = {1}}}};
    road_logic_suite::Road road_0{.id = "0",
                                  .road_links = {{.target_id = "1",
                                                  .link_type = road_logic_suite::RoadLinkType::kPredecessor,
                                                  .contact_type = road_logic_suite::LinkContactType::kEnd},
                                                 {.target_id = "2",
                                                  .link_type = road_logic_suite::RoadLinkType::kSuccessor,
                                                  .contact_type = road_logic_suite::LinkContactType::kEnd}}};
    road_0.lane_sections.Insert(units::length::meter_t(0), lane_section_road_0);

    return {.roads = {{"0", road_0}}};
}

TEST(LaneLinkerTest, GivenRoadWithInvalidPredecessorAndSuccessor_WhenLinkingLane_ThenPrintWarning)
{
    const road_logic_suite::OpenDriveData data = CreateRoadWithEmptyPredecessorAndSuccessor();
    const auto road = data.roads.at("0");
    const auto lane_section = road.lane_sections.At(units::length::meter_t(0));
    const auto lane_id = -1;
    const auto& lane = lane_section.GetLaneById(-1);

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};
    id_to_lane_relation.insert({lane_id, {}});

    auto& lane_relation = id_to_lane_relation[lane_id];

    ::testing::internal::CaptureStdout();
    const auto expected_console_output = "Road predecessor with ID 1 not found!\nRoad successor with ID 2 not found!\n";

    // act
    MantleMapLaneLinker::StoreLinks(data, road, lane_section, lane, id_provider, lane_relation);

    // assert
    const auto console_output = ::testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_console_output);
    ASSERT_THAT(lane_relation.successor_lane_ids, ::testing::IsEmpty());
    ASSERT_THAT(lane_relation.antecessor_lane_ids, ::testing::IsEmpty());
}

}  // namespace
