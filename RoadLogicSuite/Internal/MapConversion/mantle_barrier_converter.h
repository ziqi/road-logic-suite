/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_BARRIER_CONVERTER_H
#define ROADLOGICSUITE_MANTLE_BARRIER_CONVERTER_H

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/coordinate_converter.h"
#include "RoadLogicSuite/map_converter_config.h"

#include <MapAPI/lane_boundary.h>
#include <MapAPI/map.h>

namespace road_logic_suite::map_conversion
{

/// @brief Converts barrier-type OpenDRIVE objects to mantle lane boundaries.
/// An OpenDRIVE object will be converted if it is continuous (repeat distance = 0).
class MantleBarrierConverter
{
  public:
    /// @brief Converts the road barrier.
    /// @param road_id the road id this belongs to.
    /// @param barrier the barrier definition.
    /// @param converter the coordinate converter.
    /// @param config the configuration (i.e. sampling rate + downsampling).
    /// @param id_provider the id provider to set up a proper boundary id.
    /// @return the created road barrier.
    static std::unique_ptr<map_api::LaneBoundary> Convert(const std::string& road_id,
                                                          const RoadBarrier& barrier,
                                                          const CoordinateConverter& converter,
                                                          const MapConverterConfig& config,
                                                          MantleIdProvider& id_provider);
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_MANTLE_BARRIER_CONVERTER_H