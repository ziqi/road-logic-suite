/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_ID_PROVIDER_H
#define ROADLOGICSUITE_MANTLE_ID_PROVIDER_H

#include "RoadLogicSuite/Internal/MapConversion/lane_info.h"

#include <MantleAPI/Common/i_identifiable.h>

#include <map>
#include <optional>

namespace road_logic_suite::map_conversion
{
class MantleIdProvider
{
  public:
    /// @param info the lane information.
    /// @return either a newly generated id, or the id of the already converted lane.
    mantle_api::UniqueId GetOrGenerateLaneId(const LaneInfo& info);

    /// @param info the lane information.
    /// @return the ID of the lane if it has been converted, or 'std::nullopt' if the input lane is not converted.
    std::optional<mantle_api::UniqueId> QueryLaneId(const LaneInfo& info);

    /// @return always a newly generated id.
    mantle_api::UniqueId GetNewId();

  private:
    mantle_api::UniqueId internal_id_count_{1};  ///< 0 is reserved for the host vehicle
    std::map<LaneInfo, mantle_api::UniqueId> lane_info_to_mantle_id_{};
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_MANTLE_ID_PROVIDER_H
