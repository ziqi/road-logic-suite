/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "mantle_map_lane_linker.h"

namespace road_logic_suite::map_conversion
{
void MantleMapLaneLinker::StoreLinks(const OpenDriveData& data_storage,
                                     const road_logic_suite::Road& road,
                                     const road_logic_suite::types::LaneSection& lane_section,
                                     const road_logic_suite::types::Lane& lane,
                                     road_logic_suite::map_conversion::MantleIdProvider& id_provider,
                                     road_logic_suite::map_conversion::utils::LaneRelationIds& lane_relation_ids)
{
    MantleMapLaneLinker linker(data_storage, road, lane_section, lane, id_provider, lane_relation_ids);
    linker.HandleOpenDrivePredecessors();
    linker.HandleOpenDriveSuccessors();
}

void MantleMapLaneLinker::HandleOpenDrivePredecessors()
{
    const auto section_it = road_.lane_sections.Find(lane_section_.s_start);
    if (section_it == road_.lane_sections.begin())
    {
        HandleRoadPredecessors();
    }
    else
    {
        const auto prev_section_it = std::prev(section_it);
        const auto section_id = std::distance(road_.lane_sections.begin(), prev_section_it);
        AddPredecessorsTowardsSection(road_.id, section_id);
    }
}

void MantleMapLaneLinker::HandleOpenDriveSuccessors()
{
    const auto section_it = road_.lane_sections.Find(lane_section_.s_start);
    const auto next_section_it = std::next(section_it);
    if (next_section_it == road_.lane_sections.end())
    {
        HandleRoadSuccessors();
    }
    else
    {
        const auto section_id = std::distance(road_.lane_sections.begin(), next_section_it);
        AddSuccessorsTowardsSection(road_.id, section_id);
    }
}

void MantleMapLaneLinker::HandleRoadPredecessors()
{
    for (const auto& road_link : road_.road_links)
    {
        if (road_link.link_type == RoadLinkType::kPredecessor && road_link.contact_type == LinkContactType::kEnd)
        {
            const auto target_road_it = data_storage_.roads.find(road_link.target_id);
            if (target_road_it != data_storage_.roads.end())
            {
                const auto& target_road = target_road_it->second;
                const auto lane_section_id = target_road.lane_sections.FindIndex(target_road.length);
                AddPredecessorsTowardsSection(road_link.target_id, lane_section_id);
            }
            else
            {
                std::cout << "Road predecessor with ID " << road_link.target_id << " not found!" << std::endl;
            }
        }
        else if (road_link.link_type == RoadLinkType::kPredecessor && road_link.contact_type == LinkContactType::kStart)
        {
            AddPredecessorsTowardsSection(road_link.target_id, 0);
        }
    }
}

void MantleMapLaneLinker::HandleRoadSuccessors()
{
    for (const auto& road_link : road_.road_links)
    {
        if (road_link.link_type == RoadLinkType::kSuccessor && road_link.contact_type == LinkContactType::kStart)
        {
            AddSuccessorsTowardsSection(road_link.target_id, 0);
        }
        else if (road_link.link_type == RoadLinkType::kSuccessor && road_link.contact_type == LinkContactType::kEnd)
        {
            const auto target_road_it = data_storage_.roads.find(road_link.target_id);
            if (target_road_it != data_storage_.roads.end())
            {
                const auto& target_road = target_road_it->second;
                const auto lane_section_id = target_road.lane_sections.FindIndex(target_road.length);
                AddSuccessorsTowardsSection(road_link.target_id, lane_section_id);
            }
            else
            {
                std::cout << "Road successor with ID " << road_link.target_id << " not found!" << std::endl;
            }
        }
    }
}

void MantleMapLaneLinker::AddPredecessorsTowardsSection(const std::string& road_id, std::int64_t section_id)
{
    for (const auto& predecessor : lane_.predecessors)
    {
        const LaneInfo lane_info{.road_id = road_id, .lane_section_id = section_id, .lane_id = predecessor};
        if (lane_.id < 0)
        {
            lane_relation_ids_.antecessor_lane_ids.push_back(id_provider_.GetOrGenerateLaneId(lane_info));
        }
        else if (lane_.id > 0)
        {
            lane_relation_ids_.successor_lane_ids.push_back(id_provider_.GetOrGenerateLaneId(lane_info));
        }
    }
}

void MantleMapLaneLinker::AddSuccessorsTowardsSection(const std::string& road_id, std::int64_t section_id)
{
    for (const auto& successor : lane_.successors)
    {
        const LaneInfo lane_info{.road_id = road_id, .lane_section_id = section_id, .lane_id = successor};
        if (lane_.id < 0)
        {
            lane_relation_ids_.successor_lane_ids.push_back(id_provider_.GetOrGenerateLaneId(lane_info));
        }
        else if (lane_.id > 0)
        {
            lane_relation_ids_.antecessor_lane_ids.push_back(id_provider_.GetOrGenerateLaneId(lane_info));
        }
    }
}
}  // namespace road_logic_suite::map_conversion
