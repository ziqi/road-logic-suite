/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_road_object_converter.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"

namespace
{
using namespace road_logic_suite::map_conversion;

map_api::Orientation3d GetOrientation(const road_logic_suite::Object& obj)
{
    return map_api::Orientation3d{obj.hdg, obj.pitch, obj.roll};
}

map_api::Position3d GetPosition(const road_logic_suite::Object& obj)
{
    return map_api::Position3d(obj.position.x, obj.position.y, obj.position.z + obj.height * 0.5);
}

mantle_api::Dimension3 GetDimensions(const road_logic_suite::Object& obj)
{
    units::length::meter_t x_min = std::numeric_limits<units::length::meter_t>::max();
    units::length::meter_t x_max = std::numeric_limits<units::length::meter_t>::min();
    units::length::meter_t y_min = std::numeric_limits<units::length::meter_t>::max();
    units::length::meter_t y_max = std::numeric_limits<units::length::meter_t>::min();
    for (const auto& pt : obj.shape)
    {
        if (pt.x < x_min)
        {
            x_min = pt.x;
        }
        if (pt.x > x_max)
        {
            x_max = pt.x;
        }
        if (pt.y < y_min)
        {
            y_min = pt.y;
        }
        if (pt.y > y_max)
        {
            y_max = pt.y;
        }
    }
    return {.length = x_max - x_min, .width = y_max - y_min, .height = obj.height};
}

std::vector<map_api::Vector3d> CreateBasePolygon(const road_logic_suite::Object& obj)
{
    std::vector<map_api::Vector3d> polygon{};
    polygon.reserve(obj.shape.size());
    for (const auto& pt : obj.shape)
    {
        polygon.push_back(map_api::Vector3d{pt.x, pt.y, {}});
    }
    return polygon;
}

map_api::BaseProperties CreateBaseProperties(const road_logic_suite::Object& obj)
{
    return map_api::BaseProperties{.dimension = GetDimensions(obj),
                                   .position = GetPosition(obj),
                                   .orientation = GetOrientation(obj),
                                   .base_polygon = CreateBasePolygon(obj)};
}

std::unique_ptr<map_api::StationaryObject> ConvertObject(
    const road_logic_suite::OpenDriveData& data,
    const map_api::Map& map,
    const road_logic_suite::Object& object,
    road_logic_suite::map_conversion::MantleIdProvider& id_provider)
{
    auto valid_mantle_lanes =
        utils::GetMantleMapLaneIdFromOpenDriveLaneIds(data, object.road_id, object.valid_lanes, id_provider);
    auto assigned_lanes = utils::CreateAssignedLanes(map, valid_mantle_lanes);
    return std::make_unique<map_api::StationaryObject>(map_api::StationaryObject{
        .id = id_provider.GetNewId(),
        .base = CreateBaseProperties(object),
        .type = road_logic_suite::map_conversion::utils::ConvertStationaryObjectType(object.type),
        .assigned_lanes = std::move(assigned_lanes),
    });
}

}  // namespace

namespace road_logic_suite::map_conversion
{
void MantleRoadObjectConverter::ConvertObjects(const OpenDriveData& data,
                                               map_api::Map& map,
                                               MantleIdProvider& id_provider)
{
    for (const auto& object : data.objects)
    {
        map.stationary_objects.push_back(ConvertObject(data, map, object, id_provider));
    }
}

}  // namespace road_logic_suite::map_conversion
