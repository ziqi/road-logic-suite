/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "lane_info.h"

namespace road_logic_suite::map_conversion
{
bool LaneInfo::operator<(const LaneInfo& other) const
{
    return std::tie(road_id, lane_section_id, lane_id) < std::tie(other.road_id, other.lane_section_id, other.lane_id);
}
}  // namespace road_logic_suite::map_conversion
