/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_MANTLE_ROAD_SIGNAL_CONVERTER_H
#define ROADLOGICSUITE_MANTLE_ROAD_SIGNAL_CONVERTER_H

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/open_drive_data.h"

#include <MapAPI/map.h>

namespace road_logic_suite::map_conversion
{
class MantleRoadSignalConverter
{

  public:
    /// @brief Converts signals from the internal representation to the mantle map format.
    /// @param data internal data representation.
    /// @param map output format.
    /// @param id_provider to update the IDs.
    static void ConvertSignals(const road_logic_suite::OpenDriveData& data,
                               map_api::Map& map,
                               MantleIdProvider& id_provider);
};
}  // namespace road_logic_suite::map_conversion

#endif  // ROADLOGICSUITE_MANTLE_ROAD_SIGNAL_CONVERTER_H