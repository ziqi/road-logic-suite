/*******************************************************************************
 * Copyright (C) 2024-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_junction_converter.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_id_provider.h"
#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace
{
using namespace road_logic_suite::map_conversion;
using namespace road_logic_suite::types;
using IdToLaneRelation = std::map<mantle_api::UniqueId, utils::LaneRelationIds>;

road_logic_suite::RoadLink CreateRoadLink(const std::string& target_id,
                                          road_logic_suite::RoadLinkType link_type,
                                          road_logic_suite::LinkContactType contact_type)
{
    return road_logic_suite::RoadLink{.target_id = target_id, .link_type = link_type, .contact_type = contact_type};
}

road_logic_suite::Road CreateRoadWithSingleLaneSection(const std::string& id,
                                                       const LaneSection& lane_section,
                                                       const std::vector<road_logic_suite::RoadLink>& road_links = {})
{
    road_logic_suite::Road road{
        .id = id,
        .length = units::length::meter_t(10),
        .road_links = road_links,
    };
    road.lane_sections.Insert(units::length::meter_t(0), lane_section);
    return road;
}

road_logic_suite::Road CreateRoadWithMultiLaneSections(const std::string& id,
                                                       const double road_length,
                                                       const std::vector<LaneSection>& lane_sections,
                                                       const std::vector<road_logic_suite::RoadLink>& road_links = {})
{
    road_logic_suite::Road road{
        .id = id,
        .length = units::length::meter_t(road_length),
        .road_links = road_links,
    };
    for (auto& lane_section : lane_sections)
    {
        road.lane_sections.Insert(lane_section.s_start, lane_section);
    }
    return road;
}

TEST(
    JunctionConverterTest,
    GivenDataStorageWithJunctionAndRoadsWithMultiSections_WhenDeduceLaneRelationFromJunctions_ThenLaneRelationsMustBeDeducedCorrectly)
{
    // create dummy junction
    // create incoming road 0
    std::vector<LaneSection> incoming_road_0_lane_sections{
        {
            .s_start = units::length::meter_t(0),
            .left_lanes = {{.id = 1}},
            .right_lanes = {{.id = -1}},
        },
        {
            .s_start = units::length::meter_t(5),
            .left_lanes = {{.id = 1, .predecessors = {1}, .successors = {1}}},
            .right_lanes = {{.id = -1, .predecessors = {-1}, .successors = {-1}}},
        },
        {
            .s_start = units::length::meter_t(10),
            .left_lanes = {{.id = 1, .predecessors = {1}}},
            .right_lanes = {{.id = -1, .predecessors = {-1}}},
        }};
    const auto incoming_road_0 = CreateRoadWithMultiLaneSections("0", 15, incoming_road_0_lane_sections);

    // create connecting road 1
    std::vector<LaneSection> connecting_road_1_lane_sections{{
                                                                 .s_start = units::length::meter_t(0),
                                                                 .right_lanes = {{.id = -1}},
                                                             },
                                                             {
                                                                 .s_start = units::length::meter_t(5),
                                                                 .right_lanes = {{.id = -1, .predecessors = {-1}}},
                                                             }};
    const auto connecting_road_1_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kEnd);
    std::vector<road_logic_suite::RoadLink> connecting_road_1_road_links{connecting_road_1_road_link};
    const auto connecting_road_1 =
        CreateRoadWithMultiLaneSections("1", 10, connecting_road_1_lane_sections, connecting_road_1_road_links);

    // create connecting road 2
    std::vector<LaneSection> connecting_road_2_lane_sections{{
                                                                 .s_start = units::length::meter_t(0),
                                                                 .left_lanes = {{.id = 1, .successors = {1}}},
                                                             },
                                                             {
                                                                 .s_start = units::length::meter_t(5),
                                                                 .left_lanes = {{.id = 1, .predecessors = {-1}}},
                                                             }};
    const auto connecting_road_2_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kEnd);
    std::vector<road_logic_suite::RoadLink> connecting_road_2_road_links{connecting_road_2_road_link};
    const auto connecting_road_2 =
        CreateRoadWithMultiLaneSections("2", 10, connecting_road_2_lane_sections, connecting_road_2_road_links);

    // create connecting road 3
    std::vector<LaneSection> connecting_road_3_lane_sections{{
                                                                 .s_start = units::length::meter_t(0),
                                                                 .right_lanes = {{.id = -1, .predecessors = {1}}},
                                                             },
                                                             {
                                                                 .s_start = units::length::meter_t(5),
                                                                 .right_lanes = {{.id = -1, .predecessors = {-1}}},
                                                             }};
    const auto connecting_road_3_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kStart);
    std::vector<road_logic_suite::RoadLink> connecting_road_3_road_links{connecting_road_3_road_link};
    const auto connecting_road_3 =
        CreateRoadWithMultiLaneSections("3", 10, connecting_road_3_lane_sections, connecting_road_3_road_links);

    // create connecting road 4
    std::vector<LaneSection> connecting_road_4_lane_sections{{
                                                                 .s_start = units::length::meter_t(0),
                                                                 .left_lanes = {{.id = 1, .successors = {1}}},
                                                             },
                                                             {
                                                                 .s_start = units::length::meter_t(5),
                                                                 .left_lanes = {{.id = 1, .predecessors = {1}}},
                                                             }};
    const auto connecting_road_4_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kStart);
    std::vector<road_logic_suite::RoadLink> connecting_road_4_road_links{connecting_road_4_road_link};
    const auto connecting_road_4 =
        CreateRoadWithMultiLaneSections("4", 10, connecting_road_4_lane_sections, connecting_road_4_road_links);

    // create junctions
    JunctionConnection junction_connection_0{.id = "0",
                                             .incoming_road = "0",
                                             .connecting_road = "1",
                                             .junction_lane_links{{
                                                 .from = -1,
                                                 .to = -1,
                                             }}};
    JunctionConnection junction_connection_1{.id = "1",
                                             .incoming_road = "0",
                                             .connecting_road = "2",
                                             .junction_lane_links{{
                                                 .from = -1,
                                                 .to = 1,
                                             }}};
    Junction junction_0{.id = "0", .junction_connections = {junction_connection_0, junction_connection_1}};

    JunctionConnection junction_connection_2{.id = "2",
                                             .incoming_road = "0",
                                             .connecting_road = "3",
                                             .junction_lane_links{{
                                                 .from = 1,
                                                 .to = -1,
                                             }}};
    JunctionConnection junction_connection_3{.id = "3",
                                             .incoming_road = "0",
                                             .connecting_road = "4",
                                             .junction_lane_links{{
                                                 .from = 1,
                                                 .to = 1,
                                             }}};
    Junction junction_1{.id = "1", .junction_connections = {junction_connection_2, junction_connection_3}};

    const road_logic_suite::OpenDriveData data_storage{
        .roads{{"0", incoming_road_0},
               {"1", connecting_road_1},
               {"2", connecting_road_2},
               {"3", connecting_road_3},
               {"4", connecting_road_4}},
        .junctions{{"0", junction_0}, {"1", junction_1}},
    };
    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};

    // act
    MantleJunctionConverter::DeduceLaneRelationFromJunctions(data_storage, id_provider, id_to_lane_relation);

    // assert
    const LaneInfo lane_info_road_0_section_2_lane_minus_1{.road_id = "0", .lane_section_id = 2, .lane_id = -1};
    const LaneInfo lane_info_road_1_lane_minus_1{.road_id = "1", .lane_section_id = 0, .lane_id = -1};
    const LaneInfo lane_info_road_2_lane_1{.road_id = "2", .lane_section_id = 1, .lane_id = 1};

    const LaneInfo lane_info_road_0_section_0_lane_1{.road_id = "0", .lane_section_id = 0, .lane_id = 1};
    const LaneInfo lane_info_road_3_lane_minus_1{.road_id = "3", .lane_section_id = 0, .lane_id = -1};
    const LaneInfo lane_info_road_4_lane_1{.road_id = "4", .lane_section_id = 1, .lane_id = 1};

    EXPECT_THAT(id_to_lane_relation.at(id_provider.GetOrGenerateLaneId(lane_info_road_0_section_2_lane_minus_1))
                    .successor_lane_ids,
                ::testing::ElementsAre(id_provider.GetOrGenerateLaneId(lane_info_road_1_lane_minus_1),
                                       id_provider.GetOrGenerateLaneId(lane_info_road_2_lane_1)));
    EXPECT_THAT(
        id_to_lane_relation.at(id_provider.GetOrGenerateLaneId(lane_info_road_0_section_0_lane_1)).successor_lane_ids,
        ::testing::ElementsAre(id_provider.GetOrGenerateLaneId(lane_info_road_3_lane_minus_1),
                               id_provider.GetOrGenerateLaneId(lane_info_road_4_lane_1)));
}

TEST(JunctionConverterTest,
     GivenDataStorageWithJunction_WhenDeduceLaneRelationFromJunctions_ThenLaneRelationsMustBeDeducedCorrectly)
{
    // create dummy junction
    // create incoming road 0
    const LaneSection incoming_road_0_lane_section{
        .s_start = units::length::meter_t(0),
        .left_lanes = {{.id = 1}},
        .right_lanes = {{.id = -1}},
    };
    const auto incoming_road_0 = CreateRoadWithSingleLaneSection("0", incoming_road_0_lane_section);

    // create connecting road 1
    const LaneSection connecting_road_1_lane_section{.s_start = units::length::meter_t(0),
                                                     .right_lanes = {{.id = -1, .predecessors = {-1}}}};
    const auto connecting_road_1_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kEnd);
    std::vector<road_logic_suite::RoadLink> connecting_road_1_road_links{connecting_road_1_road_link};
    const auto connecting_road_1 =
        CreateRoadWithSingleLaneSection("1", connecting_road_1_lane_section, connecting_road_1_road_links);

    // create connecting road 2
    const LaneSection connecting_road_2_lane_section{.s_start = units::length::meter_t(0),
                                                     .left_lanes = {{.id = 1, .predecessors = {-1}}}};
    const auto connecting_road_2_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kEnd);
    std::vector<road_logic_suite::RoadLink> connecting_road_2_road_links{connecting_road_2_road_link};
    const auto connecting_road_2 =
        CreateRoadWithSingleLaneSection("2", connecting_road_2_lane_section, connecting_road_2_road_links);

    // create connecting road 3
    const LaneSection connecting_road_3_lane_section{.s_start = units::length::meter_t(0),
                                                     .right_lanes = {{.id = -1, .predecessors = {1}}}};
    const auto connecting_road_3_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kStart);
    std::vector<road_logic_suite::RoadLink> connecting_road_3_road_links{connecting_road_3_road_link};
    const auto connecting_road_3 =
        CreateRoadWithSingleLaneSection("3", connecting_road_3_lane_section, connecting_road_3_road_links);

    // create connecting road 4
    const LaneSection connecting_road_4_lane_section{.s_start = units::length::meter_t(0),
                                                     .left_lanes = {{.id = 1, .predecessors = {1}}}};
    const auto connecting_road_4_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kStart);
    std::vector<road_logic_suite::RoadLink> connecting_road_4_road_links{connecting_road_4_road_link};
    const auto connecting_road_4 =
        CreateRoadWithSingleLaneSection("4", connecting_road_4_lane_section, connecting_road_4_road_links);

    // create junctions
    JunctionConnection junction_connection_0{.id = "0",
                                             .incoming_road = "0",
                                             .connecting_road = "1",
                                             .junction_lane_links{{
                                                 .from = -1,
                                                 .to = -1,
                                             }}};
    JunctionConnection junction_connection_1{.id = "1",
                                             .incoming_road = "0",
                                             .connecting_road = "2",
                                             .junction_lane_links{{
                                                 .from = -1,
                                                 .to = 1,
                                             }}};
    Junction junction_0{.id = "0", .junction_connections = {junction_connection_0, junction_connection_1}};

    JunctionConnection junction_connection_2{.id = "2",
                                             .incoming_road = "0",
                                             .connecting_road = "3",
                                             .junction_lane_links{{
                                                 .from = 1,
                                                 .to = -1,
                                             }}};
    JunctionConnection junction_connection_3{.id = "3",
                                             .incoming_road = "0",
                                             .connecting_road = "4",
                                             .junction_lane_links{{
                                                 .from = 1,
                                                 .to = 1,
                                             }}};
    Junction junction_1{.id = "1", .junction_connections = {junction_connection_2, junction_connection_3}};

    const road_logic_suite::OpenDriveData data_storage{
        .roads{{"0", incoming_road_0},
               {"1", connecting_road_1},
               {"2", connecting_road_2},
               {"3", connecting_road_3},
               {"4", connecting_road_4}},
        .junctions{{"0", junction_0}, {"1", junction_1}},
    };
    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};

    // act
    MantleJunctionConverter::DeduceLaneRelationFromJunctions(data_storage, id_provider, id_to_lane_relation);

    // assert
    const LaneInfo lane_info_road_0_lane_minus_1{.road_id = "0", .lane_section_id = 0, .lane_id = -1};
    const LaneInfo lane_info_road_0_lane_1{.road_id = "0", .lane_section_id = 0, .lane_id = 1};
    const LaneInfo lane_info_road_1_lane_minus_1{.road_id = "1", .lane_section_id = 0, .lane_id = -1};
    const LaneInfo lane_info_road_2_lane_1{.road_id = "2", .lane_section_id = 0, .lane_id = 1};
    const LaneInfo lane_info_road_3_lane_minus_1{.road_id = "3", .lane_section_id = 0, .lane_id = -1};
    const LaneInfo lane_info_road_4_lane_1{.road_id = "4", .lane_section_id = 0, .lane_id = 1};

    EXPECT_THAT(
        id_to_lane_relation.at(id_provider.GetOrGenerateLaneId(lane_info_road_0_lane_minus_1)).successor_lane_ids,
        ::testing::ElementsAre(id_provider.GetOrGenerateLaneId(lane_info_road_1_lane_minus_1),
                               id_provider.GetOrGenerateLaneId(lane_info_road_2_lane_1)));
    EXPECT_THAT(id_to_lane_relation.at(id_provider.GetOrGenerateLaneId(lane_info_road_0_lane_1)).successor_lane_ids,
                ::testing::ElementsAre(id_provider.GetOrGenerateLaneId(lane_info_road_3_lane_minus_1),
                                       id_provider.GetOrGenerateLaneId(lane_info_road_4_lane_1)));
}

TEST(JunctionConverterTest,
     GivenDataStorageWithJunctionWithoutLaneLinkElements_WhenDeduceLaneRelationFromJunctions_ThenOutputWarningAndSkip)
{
    // create dummy junction without laneLink elements
    // create incoming road 0
    const LaneSection incoming_road_0_lane_section{
        .s_start = units::length::meter_t(0),
        .left_lanes = {{.id = 1}},
        .right_lanes = {{.id = -1}},
    };
    const auto incoming_road_0 = CreateRoadWithSingleLaneSection("0", incoming_road_0_lane_section);

    // create connecting road 1
    const LaneSection connecting_road_1_lane_section{.s_start = units::length::meter_t(0),
                                                     .right_lanes = {{.id = -1, .predecessors = {-1}}}};
    const auto connecting_road_1_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kEnd);
    std::vector<road_logic_suite::RoadLink> connecting_road_1_road_links{connecting_road_1_road_link};
    const auto connecting_road_1 =
        CreateRoadWithSingleLaneSection("1", connecting_road_1_lane_section, connecting_road_1_road_links);

    // create connecting road 2
    const LaneSection connecting_road_2_lane_section{.s_start = units::length::meter_t(0),
                                                     .left_lanes = {{.id = 1, .predecessors = {-1}}}};
    const auto connecting_road_2_road_link =
        CreateRoadLink("0", road_logic_suite::RoadLinkType::kPredecessor, road_logic_suite::LinkContactType::kEnd);
    std::vector<road_logic_suite::RoadLink> connecting_road_2_road_links{connecting_road_2_road_link};
    const auto connecting_road_2 =
        CreateRoadWithSingleLaneSection("2", connecting_road_2_lane_section, connecting_road_2_road_links);

    // create junctions
    JunctionConnection junction_connection_0{.id = "0", .incoming_road = "0", .connecting_road = "1"};
    JunctionConnection junction_connection_1{.id = "1", .incoming_road = "0", .connecting_road = "2"};
    Junction junction_0{.id = "0", .junction_connections = {junction_connection_0, junction_connection_1}};

    const road_logic_suite::OpenDriveData data_storage{
        .roads{{"0", incoming_road_0}, {"1", connecting_road_1}, {"2", connecting_road_2}},
        .junctions{{"0", junction_0}},
    };

    MantleIdProvider id_provider{};
    IdToLaneRelation id_to_lane_relation{};

    // act
    testing::internal::CaptureStdout();
    MantleJunctionConverter::DeduceLaneRelationFromJunctions(data_storage, id_provider, id_to_lane_relation);

    // assert
    const std::string expected_warning_message =
        "[RoadLogicSuite: Junction Conversion] WARNING: Junction with ID 0 and connection ID 0 has no laneLink "
        "elements. "
        "Lane relations (successor/predecessor) cannot be deduced. Please check the map file!\n"
        "[RoadLogicSuite: Junction Conversion] WARNING: Junction with ID 0 and connection ID 1 has no laneLink "
        "elements. "
        "Lane relations (successor/predecessor) cannot be deduced. Please check the map file!\n";
    const std::string output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expected_warning_message);
}

}  // namespace
