/*******************************************************************************
 * Copyright (C) 2023-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapConversion/mantle_barrier_converter.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter_utils.h"
#include "RoadLogicSuite/Internal/Utils/converter_utils.h"
#include "RoadLogicSuite/Internal/Utils/math_utils.h"

namespace road_logic_suite::map_conversion
{
std::unique_ptr<map_api::LaneBoundary> MantleBarrierConverter::Convert(const std::string& road_id,
                                                                       const RoadBarrier& barrier,
                                                                       const CoordinateConverter& converter,
                                                                       const MapConverterConfig& config,
                                                                       MantleIdProvider& id_provider)
{
    std::vector<map_api::LaneBoundary::BoundaryPoint> points{};
    for (auto s = barrier.s_start; s < barrier.s_end; s += config.sampling_distance)
    {
        const auto s_percent = math_utils::InverseLerp(barrier.s_start, barrier.s_end, s);
        const auto t = math_utils::Lerp(barrier.t_start, barrier.t_end, s_percent);
        const auto z_offset = math_utils::Lerp(barrier.z_offset_start, barrier.z_offset_end, s_percent);
        const auto height = math_utils::Lerp(barrier.height_start, barrier.height_end, s_percent);
        const auto width = math_utils::Lerp(barrier.width_start, barrier.width_end, s_percent);
        const auto position =
            converter.ConvertRoadPositionToInertialCoordinates({.road = road_id, .s_offset = s, .t_offset = t});
        if (position.has_value())
        {
            points.push_back({
                .position = {position.value().x, position.value().y, position.value().z + z_offset},
                .width = width,
                .height = height,
            });
        }
    }
    const auto position = converter.ConvertRoadPositionToInertialCoordinates(
        {.road = road_id, .s_offset = barrier.s_end, .t_offset = barrier.t_end});

    if (position.has_value())
    {
        points.push_back({
            .position = {position.value().x, position.value().y, position.value().z + barrier.z_offset_end},
            .width = barrier.width_end,
            .height = barrier.height_end,
        });
    }

    if (config.downsampling)
    {
        points = DecimatePolyline(points.begin(), points.end(), config.downsampling_epsilon.value());
    }
    return std::make_unique<map_api::LaneBoundary>(map_api::LaneBoundary{
        .id = id_provider.GetNewId(), .boundary_line = points, .type = utils::ConvertRoadBarrierType(barrier.name)});
}
}  // namespace road_logic_suite::map_conversion
