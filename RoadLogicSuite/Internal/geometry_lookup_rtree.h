/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_GEOMETRY_LOOKUP_RTREE_H
#define ROADLOGICSUITE_GEOMETRY_LOOKUP_RTREE_H

#include "RTree.h"
#include "i_geometry_lookup.h"

#include <memory>
#include <vector>

namespace road_logic_suite
{

class GeometryLookupRTree : public IGeometryLookup
{
  public:
    explicit GeometryLookupRTree(std::vector<std::unique_ptr<IGeometry>> const& geometries);

    void OptimizeStorage() final;

    void VisitGeometry(const std::function<bool(const IGeometry*)>& search_callback) const final;

    void VisitGeometry(const Vec2<units::length::meter_t>& search_point,
                       const std::function<bool(const IGeometry*)>& search_callback) const final;

  private:
    RTree<const IGeometry*, double, 2> tree_;
    bool is_using_r_tree_;

    std::vector<std::unique_ptr<IGeometry>> const& geometries_;
};
}  // namespace road_logic_suite

#endif  // ROADLOGICSUITE_GEOMETRY_LOOKUP_RTREE_H
