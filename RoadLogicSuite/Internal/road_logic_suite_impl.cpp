/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/road_logic_suite_impl.h"

namespace road_logic_suite
{
bool RoadLogicSuiteImpl::LoadFile(const std::string& xodr_file)
{
    odr::OpenDriveMap map(xodr_file,
                          false,  // center_map
                          true,   // with_road_objects
                          true,   // with_lateral_profile
                          true,   // with_lane_height
                          false,  // abs_z_for_for_local_road_obj_outline
                          false,  // fix_spiral_edge_cases
                          true);  // with_road_signals
    if (!map.xml_doc.document_element())
    {
        return false;
    }
    LibOpenDriveConverter converter(this->data_storage_);
    converter.Convert(map);

    coordinate_converter_ = std::make_unique<CoordinateConverter>(data_storage_);
    coordinate_converter_->OptimizeStorage();

    return true;
}

const CoordinateConverter& RoadLogicSuiteImpl::GetCoordinateConverter()
{
    return *coordinate_converter_;
}

map_validation::ValidationRunner RoadLogicSuiteImpl::GetMapValidator() const
{
    return map_validation::ValidationRunner(data_storage_);
}
}  // namespace road_logic_suite
