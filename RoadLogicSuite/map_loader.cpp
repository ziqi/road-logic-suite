/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/map_loader.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter.h"
#include "RoadLogicSuite/Internal/road_logic_suite_impl.h"
namespace road_logic_suite
{

MapLoader::MapLoader()
{
    road_logic_suite_impl_ = std::make_unique<RoadLogicSuiteImpl>();
}

MapLoader::~MapLoader() = default;

std::unique_ptr<map_api::Map> MapLoader::LoadMap(const std::string& map_file) const
{
    if (!road_logic_suite_impl_->LoadFile(map_file))
    {
        throw std::runtime_error("RoadLogicSuite load map file " + map_file + " failed.");
    }

    MapConverterConfig converter_config{};
    map_conversion::MantleMapConverter converter(converter_config);
    return std::make_unique<map_api::Map>(std::move(road_logic_suite_impl_->ConvertMap(converter)));
}

}  // namespace road_logic_suite
