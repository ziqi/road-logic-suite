/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
TEST_F(MapConvertIntegrationTestFixture, GivenMapWithMultiSections_WhenConverting_ThenLaneGroupsShouldBeParsedCorrectly)
{
    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_multisections.xodr"));
    const auto expected_lane_group_size = 8;
    const auto expected_lane_group_type = map_api::LaneGroup::Type::kOneWay;
    const auto expected_section_0_right_lanes_size = 2;
    const auto expected_section_0_left_lanes_size = 2;
    const auto expected_section_1_right_lanes_size = 2;
    const auto expected_section_1_left_lanes_size = 2;
    const auto expected_section_2_right_lanes_size = 2;
    const auto expected_section_2_left_lanes_size = 2;
    const auto expected_section_3_right_lanes_size = 3;
    const auto expected_section_3_left_lanes_size = 3;
    const auto expected_section_0_right_lane_boundaries_size = 14;
    const auto expected_section_0_left_lane_boundaries_size = 13;
    const auto expected_section_1_right_lane_boundaries_size = 4;
    const auto expected_section_1_left_lane_boundaries_size = 4;
    const auto expected_section_2_right_lane_boundaries_size = 3;
    const auto expected_section_2_left_lane_boundaries_size = 3;
    const auto expected_section_3_right_lane_boundaries_size = 5;
    const auto expected_section_3_left_lane_boundaries_size = 5;

    ASSERT_EQ(map.lane_groups.size(), expected_lane_group_size);
    EXPECT_EQ(map.lane_groups.at(0)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(1)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(2)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(3)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(4)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(5)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(6)->type, expected_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(7)->type, expected_lane_group_type);

    EXPECT_EQ(map.lane_groups.at(0)->id, 1);
    EXPECT_EQ(map.lane_groups.at(1)->id, 2);
    EXPECT_EQ(map.lane_groups.at(2)->id, 3);
    EXPECT_EQ(map.lane_groups.at(3)->id, 4);
    EXPECT_EQ(map.lane_groups.at(4)->id, 5);
    EXPECT_EQ(map.lane_groups.at(5)->id, 6);
    EXPECT_EQ(map.lane_groups.at(6)->id, 7);
    EXPECT_EQ(map.lane_groups.at(7)->id, 8);

    EXPECT_EQ(map.lane_groups.at(0)->lanes.size(), expected_section_0_right_lanes_size);
    EXPECT_EQ(map.lane_groups.at(1)->lanes.size(), expected_section_0_left_lanes_size);
    EXPECT_EQ(map.lane_groups.at(2)->lanes.size(), expected_section_1_right_lanes_size);
    EXPECT_EQ(map.lane_groups.at(3)->lanes.size(), expected_section_1_left_lanes_size);
    EXPECT_EQ(map.lane_groups.at(4)->lanes.size(), expected_section_2_right_lanes_size);
    EXPECT_EQ(map.lane_groups.at(5)->lanes.size(), expected_section_2_left_lanes_size);
    EXPECT_EQ(map.lane_groups.at(6)->lanes.size(), expected_section_3_right_lanes_size);
    EXPECT_EQ(map.lane_groups.at(7)->lanes.size(), expected_section_3_left_lanes_size);

    EXPECT_EQ(map.lane_groups.at(0)->lane_boundaries.size(), expected_section_0_right_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(1)->lane_boundaries.size(), expected_section_0_left_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(2)->lane_boundaries.size(), expected_section_1_right_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(3)->lane_boundaries.size(), expected_section_1_left_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(4)->lane_boundaries.size(), expected_section_2_right_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(5)->lane_boundaries.size(), expected_section_2_left_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(6)->lane_boundaries.size(), expected_section_3_right_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(7)->lane_boundaries.size(), expected_section_3_left_lane_boundaries_size);
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithJunction_WhenConverting_ThenLaneGroupsShouldBeParsedCorrectly)
{
    const auto& map = GetMap(GetResolvedMapPathInternal("junction_example.xodr"));
    const auto expected_lane_group_size = 3;
    const auto expected_junction_lane_group_type = map_api::LaneGroup::Type::kJunction;
    const auto expected_oneway_lane_group_type = map_api::LaneGroup::Type::kOneWay;

    const auto expected_road_0_left_lanes_size = 3;
    const auto expected_road_1_right_lanes_size = 3;
    const auto expected_road_2_right_lanes_size = 1;

    const auto expected_road_0_left_lane_boundaries_size = 4;
    const auto expected_road_1_right_lane_boundaries_size = 4;
    const auto expected_road_2_right_lane_boundaries_size = 2;

    ASSERT_EQ(map.lane_groups.size(), expected_lane_group_size);
    EXPECT_EQ(map.lane_groups.at(0)->type, expected_junction_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(1)->type, expected_junction_lane_group_type);
    EXPECT_EQ(map.lane_groups.at(2)->type, expected_oneway_lane_group_type);

    EXPECT_EQ(map.lane_groups.at(2)->lanes.size(), expected_road_0_left_lanes_size);
    EXPECT_EQ(map.lane_groups.at(1)->lanes.size(), expected_road_1_right_lanes_size);
    EXPECT_EQ(map.lane_groups.at(0)->lanes.size(), expected_road_2_right_lanes_size);

    EXPECT_EQ(map.lane_groups.at(2)->lane_boundaries.size(), expected_road_0_left_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(1)->lane_boundaries.size(), expected_road_1_right_lane_boundaries_size);
    EXPECT_EQ(map.lane_groups.at(0)->lane_boundaries.size(), expected_road_2_right_lane_boundaries_size);
}
}  // namespace road_logic_suite::test