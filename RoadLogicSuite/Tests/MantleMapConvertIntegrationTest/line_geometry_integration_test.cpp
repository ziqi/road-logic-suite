/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using units::literals::operator""_m;
using ::testing::ElementsAre;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///
///  origin = (-5.0, -10.0)
///  o-----------------------> y: -10.00  (ref line)
///  | x x x (Lane -1) x x x | y: -11.75
///  |-----------------------| y: -13.50
///  | x x x (Lane -2) x x x | y: -15.25
///  |-----------------------| y: -17.00
///  | x x x (Lane -3) x x x | y: -18.75
///  |-----------------------|
///  ^                       ^
/// s(0)                    (6000)
/// x(-5)                   (5995)
///
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithLineGeometry_WhenParsingMap_ThenConversionLaneAndLaneBoundariesAreCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath("Highway6kmStraightRight.xodr"));

    // Lanes
    const auto& lanes = map.lanes;
    ASSERT_EQ(lanes.size(), 3);

    const auto& lane_minus_1 = lanes[0];
    const auto& lane_minus_2 = lanes[1];
    const auto& lane_minus_3 = lanes[2];

    EXPECT_EQ(lane_minus_1->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_minus_1->successor_lanes.size(), 0);
    EXPECT_THAT(lane_minus_1->left_adjacent_lanes, ::testing::IsEmpty());
    EXPECT_THAT(lane_minus_1->right_adjacent_lanes.size(), 1);
    EXPECT_THAT(lane_minus_1->right_adjacent_lanes, ElementsAre(*lane_minus_2));
    EXPECT_THAT(lane_minus_1->centerline.front(), MantlePositionIsNear(MantlePosition{-5.0_m, -11.75_m, 0.0_m}));
    EXPECT_THAT(lane_minus_1->centerline.back(), MantlePositionIsNear(MantlePosition{5995.0_m, -11.75_m, 0.0_m}));

    ASSERT_EQ(lane_minus_1->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lane_minus_1->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lane_minus_2->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_minus_2->successor_lanes.size(), 0);
    EXPECT_THAT(lane_minus_2->left_adjacent_lanes.size(), 1);
    EXPECT_THAT(lane_minus_2->right_adjacent_lanes.size(), 1);
    EXPECT_THAT(lane_minus_2->left_adjacent_lanes, ElementsAre(*lane_minus_1));
    EXPECT_THAT(lane_minus_2->right_adjacent_lanes, ElementsAre(*lane_minus_3));
    EXPECT_THAT(lane_minus_2->centerline.front(), MantlePositionIsNear(MantlePosition{-5.0_m, -15.25_m, 0.0_m}));
    EXPECT_THAT(lane_minus_2->centerline.back(), MantlePositionIsNear(MantlePosition{5995.0_m, -15.25_m, 0.0_m}));

    ASSERT_EQ(lane_minus_2->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lane_minus_2->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lane_minus_3->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_minus_3->successor_lanes.size(), 0);
    EXPECT_THAT(lane_minus_3->left_adjacent_lanes.size(), 1);
    EXPECT_THAT(lane_minus_3->right_adjacent_lanes.size(), 0);
    EXPECT_THAT(lane_minus_3->left_adjacent_lanes, ElementsAre(*lane_minus_2));
    EXPECT_THAT(lane_minus_3->right_adjacent_lanes, ::testing::IsEmpty());
    EXPECT_THAT(lane_minus_3->centerline.front(), MantlePositionIsNear(MantlePosition{-5.0_m, -18.75_m, 0.0_m}));
    EXPECT_THAT(lane_minus_3->centerline.back(), MantlePositionIsNear(MantlePosition{5995.0_m, -18.75_m, 0.0_m}));

    ASSERT_EQ(lane_minus_3->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lane_minus_3->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lane_minus_1->right_lane_boundaries[0].get().id, lane_minus_2->left_lane_boundaries[0].get().id);
    EXPECT_EQ(lane_minus_2->right_lane_boundaries[0].get().id, lane_minus_3->left_lane_boundaries[0].get().id);

    // Lane boundary 0
    EXPECT_EQ(lane_minus_1->left_lane_boundaries[0].get().type, LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_minus_1->left_lane_boundaries[0].get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_minus_1->left_lane_boundaries[0].get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {-5.0_m, -10.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_minus_1->left_lane_boundaries[0].get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {5995.0_m, -10_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 1
    EXPECT_EQ(lane_minus_1->right_lane_boundaries[0].get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(lane_minus_1->right_lane_boundaries[0].get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_minus_1->right_lane_boundaries[0].get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {-5.0_m, -13.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_minus_1->right_lane_boundaries[0].get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {5995.0_m, -13.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 2
    EXPECT_EQ(lane_minus_2->right_lane_boundaries[0].get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(lane_minus_2->right_lane_boundaries[0].get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_minus_2->right_lane_boundaries[0].get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {-5.0_m, -17.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_minus_2->right_lane_boundaries[0].get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {5995.0_m, -17_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 3
    EXPECT_EQ(lane_minus_3->right_lane_boundaries[0].get().type, LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_minus_3->right_lane_boundaries[0].get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_minus_3->right_lane_boundaries[0].get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {-5.0_m, -20.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_minus_3->right_lane_boundaries[0].get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {5995.0_m, -20.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
}

}  // namespace road_logic_suite::test
