/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using ::testing::ElementsAre;
using units::literals::operator""_m;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///  origin = (-5.0, -10.0)
///   |-----------------------| ref line
///   | x x x (Lane -1) x x x |
///   |-----------------------|
///   | x x x (Lane -2) x x x |
///   |-----------------------|
///   | x x x (Lane -2) x x x |
///   |-----------------------|
///   ^                       ^
/// s(0)                    (1000)
/// x(0)
///
/// IDs:
/// |LaneGroup: 9                 |
/// |-----------------------------|
/// |Left LaneBoundary: 1         |
/// |Lane: 3                      |
/// |Right LaneBoundary: 6        |
/// |-----------------------------|
/// |Left LaneBoundary: 6         |
/// |Lane: 4                      |
/// |Right LaneBoundary:7         |
/// |-----------------------------|
/// |Left LaneBoundary: 7         |
/// |Lane: 5                      |
/// |Right LaneBoundary:8         |
/// |-----------------------------|
///
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithSpiralGeometry_WhenParsingMap_ThenConversionLaneAndLaneBoundariesAreCorrect)
{

    const std::string test_case = GetResolvedMapPath("CurvedRoad1kmRightSpiralGeometry.xodr");

    const auto& map = GetMap(test_case);

    ASSERT_EQ(map.lanes.size(), 3);
    const auto& lane_3 = map.lanes[0];
    const auto& lane_4 = map.lanes[1];
    const auto& lane_5 = map.lanes[2];

    const auto& lane_boundary_1 = lane_3->left_lane_boundaries[0];
    const auto& lane_boundary_6 = lane_3->right_lane_boundaries[0];
    const auto& lane_boundary_7 = lane_4->right_lane_boundaries[0];
    const auto& lane_boundary_8 = lane_5->right_lane_boundaries[0];

    // Lane  1
    EXPECT_EQ(lane_3->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_3->successor_lanes.size(), 0);
    EXPECT_THAT(lane_3->left_adjacent_lanes, ::testing::IsEmpty());
    EXPECT_THAT(lane_3->right_adjacent_lanes, ElementsAre(*lane_4));
    EXPECT_THAT(lane_3->centerline.front(), MantlePositionIsNear(MantlePosition{-5.0_m, -11.75_m, 0.0_m}));
    EXPECT_THAT(lane_3->centerline.back(), MantlePositionIsNear(MantlePosition{169.135_m, -249.675_m, 0.0_m}));
    ASSERT_THAT(lane_3->left_lane_boundaries, ElementsAre(lane_boundary_1));
    ASSERT_THAT(lane_3->right_lane_boundaries, ElementsAre(lane_boundary_6));

    // Lane 2
    EXPECT_EQ(lane_4->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_4->successor_lanes.size(), 0);
    EXPECT_THAT(lane_4->left_adjacent_lanes, ElementsAre(*lane_3));
    EXPECT_THAT(lane_4->right_adjacent_lanes, ElementsAre(*lane_5));
    EXPECT_THAT(lane_4->centerline.front(), MantlePositionIsNear(MantlePosition{-5.0_m, -15.25_m, 0.0_m}));
    EXPECT_THAT(lane_4->centerline.back(), MantlePositionIsNear(MantlePosition{171.039_m, -246.738_m, 0.0_m}));
    ASSERT_THAT(lane_4->left_lane_boundaries, ElementsAre(lane_boundary_6));
    ASSERT_THAT(lane_4->right_lane_boundaries, ElementsAre(lane_boundary_7));

    // Lane 3
    EXPECT_EQ(lane_5->antecessor_lanes.size(), 0);
    EXPECT_EQ(lane_5->successor_lanes.size(), 0);
    EXPECT_THAT(lane_5->left_adjacent_lanes, ElementsAre(*lane_4));
    EXPECT_THAT(lane_5->right_adjacent_lanes, ElementsAre());
    EXPECT_THAT(lane_5->centerline.front(), MantlePositionIsNear(MantlePosition{-5.0_m, -18.75_m, 0.0_m}));
    EXPECT_THAT(lane_5->centerline.back(), MantlePositionIsNear(MantlePosition{172.943_m, -243.801_m, 0.0_m}));
    ASSERT_THAT(lane_5->left_lane_boundaries, ElementsAre(lane_boundary_7));
    ASSERT_THAT(lane_5->right_lane_boundaries, ElementsAre(lane_boundary_8));

    // Lane boundary 0
    EXPECT_EQ(lane_boundary_1.get().type, LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_boundary_1.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_1.get().boundary_line.front(),
                LaneBoundaryIsEqual(
                    LaneBoundary::BoundaryPoint{.position = {-5.0_m, -10_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_1.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {168.183_m, -251.143_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 4
    EXPECT_EQ(lane_boundary_6.get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(lane_boundary_6.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_6.get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {-5.0_m, -13.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_6.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {170.087_m, -248.206_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 5
    EXPECT_EQ(lane_boundary_7.get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(lane_boundary_7.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_7.get().boundary_line.front(),
                LaneBoundaryIsEqual(
                    LaneBoundary::BoundaryPoint{.position = {-5.0_m, -17_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_7.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {171.991_m, -245.27_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 6
    EXPECT_EQ(lane_boundary_8.get().type, LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_boundary_8.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_8.get().boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {-5.0_m, -20.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_8.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {173.895_m, -242.333_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
}

}  // namespace road_logic_suite::test
