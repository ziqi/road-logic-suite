/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using ::testing::ElementsAre;
using units::literals::operator""_m;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///  origin = (0.0, 0.0)
///   |-----------------------| ref line
///   | x x x (Lane -1) x x x |
///   |-----------------------|
///   | x x x (Lane -2) x x x |
///   |-----------------------|
///   ^                       ^
/// s(0)                    (100)
/// x(0)
///
/// IDs:
/// |LaneGroup: 9                 |
/// |-----------------------------|
/// |Left LaneBoundary: 1         |
/// |Lane: 3                      |
/// |Right LaneBoundary: 6        |
/// |-----------------------------|
/// |Left LaneBoundary: 6         |
/// |Lane: 4                      |
/// |Right LaneBoundary:7         |
/// |-----------------------------|
///
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithParametricPolynomialGeometry_WhenParsingMap_ThenConversionLaneAndLaneBoundariesAreCorrect)
{

    const std::string test_map = GetResolvedMapPathInternal("road_with_param_poly3_geometry.xodr");

    const auto& map = GetMap(test_map);

    ASSERT_EQ(map.lanes.size(), 2);
    const auto& lane_3 = map.lanes[0];
    const auto& lane_4 = map.lanes[1];

    ASSERT_EQ(lane_3->left_lane_boundaries.size(), 1);
    const auto& lane_boundary_1 = lane_3->left_lane_boundaries[0];
    ASSERT_EQ(lane_3->right_lane_boundaries.size(), 1);
    const auto& lane_boundary_6 = lane_3->right_lane_boundaries[0];
    ASSERT_EQ(lane_4->right_lane_boundaries.size(), 1);
    const auto& lane_boundary_7 = lane_4->right_lane_boundaries[0];

    // Lane 3
    EXPECT_THAT(lane_3->left_adjacent_lanes, ::testing::IsEmpty());
    EXPECT_THAT(lane_3->right_adjacent_lanes, ElementsAre(*lane_4));
    EXPECT_THAT(lane_3->centerline.front(), MantlePositionIsNear(MantlePosition{0.0_m, -1.75_m, 0.0_m}));
    EXPECT_THAT(lane_3->centerline.back(), MantlePositionIsNear(MantlePosition{99.904_m, -3.73766_m, 0.0_m}));
    ASSERT_THAT(lane_3->left_lane_boundaries, ElementsAre(lane_boundary_1));
    ASSERT_THAT(lane_3->right_lane_boundaries, ElementsAre(lane_boundary_6));

    // Lane 4
    EXPECT_THAT(lane_4->left_adjacent_lanes, ElementsAre(*lane_3));
    EXPECT_THAT(lane_4->centerline.front(), MantlePositionIsNear(MantlePosition{0.0_m, -5.25_m, 0.0_m}));
    EXPECT_THAT(lane_4->centerline.back(), MantlePositionIsNear(MantlePosition{99.7647_m, -7.23489_m, 0.0_m}));
    ASSERT_THAT(lane_4->left_lane_boundaries, ElementsAre(lane_boundary_6));
    ASSERT_THAT(lane_4->right_lane_boundaries, ElementsAre(lane_boundary_7));

    // Lane boundary 1
    EXPECT_EQ(lane_boundary_1.get().type, LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(lane_boundary_1.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_1.get().boundary_line.front(),
                LaneBoundaryIsEqual(
                    LaneBoundary::BoundaryPoint{.position = {0.0_m, 0.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_1.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {99.9737_m, -1.98905_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 6
    EXPECT_EQ(lane_boundary_6.get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(lane_boundary_6.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_6.get().boundary_line.front(),
                LaneBoundaryIsEqual(
                    LaneBoundary::BoundaryPoint{.position = {0.0_m, -3.5_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_6.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {99.8344_m, -5.48627_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    // Lane boundary 7
    EXPECT_EQ(lane_boundary_7.get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(lane_boundary_7.get().color, LaneBoundary::Color::kStandard);
    EXPECT_THAT(lane_boundary_7.get().boundary_line.front(),
                LaneBoundaryIsEqual(
                    LaneBoundary::BoundaryPoint{.position = {0.0_m, -7.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(lane_boundary_7.get().boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {99.6951_m, -8.9835_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
}

}  // namespace road_logic_suite::test
