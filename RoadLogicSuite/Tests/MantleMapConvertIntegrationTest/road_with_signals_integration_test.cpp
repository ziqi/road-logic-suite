/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

constexpr double kAbsError = 1e-6;

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithTrafficSigns_WhenParsingMap_ThenConversionOfTrafficSignsAreCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_traffic_signs.xodr"));
    const auto& lanes = map.lanes;

    ASSERT_EQ(lanes.size(), 4);
    const auto& lane_minus_1 = GetLaneWithFrontAt(0, -1.5);
    const auto& lane_minus_2 = GetLaneWithFrontAt(0, -4.5);

    const auto& traffic_signs = map.traffic_signs;
    ASSERT_EQ(traffic_signs.size(), 1);
    const auto& main_sign = traffic_signs[0]->main_sign;
    const auto& supplementary_signs = traffic_signs[0]->supplementary_signs;
    ASSERT_EQ(supplementary_signs.size(), 1);
    const auto& supplementary_sign = supplementary_signs[0];

    const auto expected_main_sign_position_x = 50;
    const auto expected_main_sign_position_y = -6.5;
    const auto expected_main_sign_position_z = 2.205;
    const auto expected_main_sign_rotation_roll = 0.0;
    const auto expected_main_sign_rotation_pitch = 0.0;
    const auto expected_main_sign_rotation_yaw = M_PI;
    const auto expected_main_sign_length = 0.0;
    const auto expected_main_sign_width = 0.61;
    const auto expected_main_sign_height = 0.61;
    const auto expected_main_sign_variability = map_api::TrafficSignVariability::kFixed;
    const auto expected_main_sign_type = map_api::MainSignType::kSpeedLimitBegin;
    const auto expected_main_sign_value = 60.0;
    const auto expected_main_sign_value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
    const auto expected_main_sign_type_str = "274";
    const auto expected_main_sign_subtype_str = "56";
    const auto expected_main_sign_country_str = "DE";
    const auto expected_main_sign_country_revision_str = "2013";

    const auto expected_supplementary_sign_variability = map_api::TrafficSignVariability::kFixed;
    const auto expected_supplementary_sign_type = map_api::SupplementarySignType::kConstrainedTo;
    const auto expected_supplementary_sign_value = 0.0;
    const auto expected_supplementary_sign_value_unit = map_api::TrafficSignValue::Unit::kNoUnit;
    const auto expected_supplementary_sign_type_str = "1010";
    const auto expected_supplementary_sign_subtype_str = "51";
    const auto expected_supplementary_sign_country_str = "DE";
    const auto expected_supplementary_sign_country_revision_str = "2013";

    const auto& main_sign_base = main_sign.base;

    EXPECT_NEAR(main_sign_base.position.x(), expected_main_sign_position_x, kAbsError);
    EXPECT_NEAR(main_sign_base.position.y(), expected_main_sign_position_y, kAbsError);
    EXPECT_NEAR(main_sign_base.position.z(), expected_main_sign_position_z, kAbsError);

    EXPECT_NEAR(main_sign_base.orientation.yaw(), expected_main_sign_rotation_yaw, kAbsError);
    EXPECT_NEAR(main_sign_base.orientation.pitch(), expected_main_sign_rotation_pitch, kAbsError);
    EXPECT_NEAR(main_sign_base.orientation.roll(), expected_main_sign_rotation_roll, kAbsError);

    // Since the main sign constains no length property, the polygon will not be generated.
    ASSERT_EQ(main_sign_base.base_polygon.size(), 0);

    ASSERT_NEAR(main_sign_base.dimension.length(), expected_main_sign_length, kAbsError);
    ASSERT_NEAR(main_sign_base.dimension.width(), expected_main_sign_width, kAbsError);
    ASSERT_NEAR(main_sign_base.dimension.height(), expected_main_sign_height, kAbsError);

    EXPECT_EQ(main_sign.variability, expected_main_sign_variability);
    EXPECT_EQ(main_sign.type, expected_main_sign_type);
    EXPECT_EQ(main_sign.value.value, expected_main_sign_value);
    EXPECT_EQ(main_sign.value.value_unit, expected_main_sign_value_unit);
    EXPECT_EQ(main_sign.vertically_mirrored, false);
    EXPECT_EQ(main_sign.code, expected_main_sign_type_str);
    EXPECT_EQ(main_sign.sub_code, expected_main_sign_subtype_str);
    EXPECT_EQ(main_sign.country, expected_main_sign_country_str);
    EXPECT_EQ(main_sign.country_revision, expected_main_sign_country_revision_str);
    ASSERT_EQ(main_sign.assigned_lanes.size(), 2);
    EXPECT_THAT(main_sign.assigned_lanes, ::testing::ElementsAre(lane_minus_2, lane_minus_1));

    EXPECT_EQ(supplementary_sign.variability, expected_supplementary_sign_variability);
    EXPECT_EQ(supplementary_sign.type, expected_supplementary_sign_type);
    ASSERT_EQ(supplementary_sign.values.size(), 1);
    EXPECT_EQ(supplementary_sign.values[0].value, expected_supplementary_sign_value);
    EXPECT_EQ(supplementary_sign.values[0].value_unit, expected_supplementary_sign_value_unit);
    EXPECT_EQ(supplementary_sign.code, expected_supplementary_sign_type_str);
    EXPECT_EQ(supplementary_sign.sub_code, expected_supplementary_sign_subtype_str);
    EXPECT_EQ(supplementary_sign.country, expected_supplementary_sign_country_str);
    EXPECT_EQ(supplementary_sign.country_revision, expected_supplementary_sign_country_revision_str);
    ASSERT_EQ(supplementary_sign.assigned_lanes.size(), 2);
    EXPECT_THAT(supplementary_sign.assigned_lanes, ::testing::ElementsAre(lane_minus_2, lane_minus_1));
    EXPECT_THAT(supplementary_sign.actors, ::testing::ElementsAre(map_api::SupplementarySignActor::kTrucks));
    ASSERT_EQ(supplementary_sign.arrows.size(), 1);
    EXPECT_THAT(supplementary_sign.arrows[0].direction,
                ::testing::ElementsAre(map_api::SupplementarySignArrow::Direction::kUnknown));
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithRoadMarkings_WhenParsingMap_ThenConversionOfRoadMarkingsAreCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_road_markings.xodr"));
    const auto& lanes = map.lanes;

    ASSERT_EQ(lanes.size(), 4);
    const auto& lane_1 = GetLaneWithFrontAt(100.0, 1.5);
    const auto& lane_2 = GetLaneWithFrontAt(100.0, 4.5);

    const auto& road_markings = map.road_markings;
    ASSERT_EQ(road_markings.size(), 4);
    const auto& road_marking = road_markings[0];

    const auto expected_road_marking_position_x = 50.0;
    const auto expected_road_marking_position_y = 5.25;
    const auto expected_road_marking_position_z = 0.0;
    const auto expected_road_marking_rotation_roll = 0.0;
    const auto expected_road_marking_rotation_pitch = 0.0;
    const auto expected_road_marking_rotation_yaw = 0.0;
    const auto expected_road_marking_length = 10.0;
    const auto expected_road_marking_width = 1.5;
    const auto expected_road_marking_height = 0.0;

    const auto expected_road_marking_type = map_api::RoadMarking::Type::kSymbolicTrafficSign;
    const auto expected_road_marking_main_sign_type = map_api::MainSignType::kNoParking;
    const auto expected_road_marking_type_str = "299";
    const auto expected_road_marking_subtype_str = "";
    const auto expected_road_marking_country_str = "DE";
    const auto expected_road_marking_country_revision_str = "2013";

    const auto& road_marking_base = road_marking->base;

    EXPECT_NEAR(road_marking_base.position.x(), expected_road_marking_position_x, kAbsError);
    EXPECT_NEAR(road_marking_base.position.y(), expected_road_marking_position_y, kAbsError);
    EXPECT_NEAR(road_marking_base.position.z(), expected_road_marking_position_z, kAbsError);

    EXPECT_NEAR(road_marking_base.orientation.yaw(), expected_road_marking_rotation_yaw, kAbsError);
    EXPECT_NEAR(road_marking_base.orientation.pitch(), expected_road_marking_rotation_pitch, kAbsError);
    EXPECT_NEAR(road_marking_base.orientation.roll(), expected_road_marking_rotation_roll, kAbsError);

    ASSERT_NEAR(road_marking_base.dimension.length(), expected_road_marking_length, kAbsError);
    ASSERT_NEAR(road_marking_base.dimension.width(), expected_road_marking_width, kAbsError);
    ASSERT_NEAR(road_marking_base.dimension.height(), expected_road_marking_height, kAbsError);

    EXPECT_EQ(road_marking->type, expected_road_marking_type);
    EXPECT_EQ(road_marking->traffic_main_sign_type, expected_road_marking_main_sign_type);

    EXPECT_EQ(road_marking->code, expected_road_marking_type_str);
    EXPECT_EQ(road_marking->sub_code, expected_road_marking_subtype_str);
    EXPECT_EQ(road_marking->country, expected_road_marking_country_str);
    EXPECT_EQ(road_marking->country_revision, expected_road_marking_country_revision_str);
    ASSERT_EQ(road_marking->assigned_lanes.size(), 2);
    EXPECT_THAT(road_marking->assigned_lanes, ::testing::ElementsAre(lane_1, lane_2));
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithAllSignalTypes_WhenParsingMap_ThenSizeOfEachSignalTypeIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_all_type_signals.xodr"));

    ASSERT_EQ(map.traffic_signs.size(), 1);
    ASSERT_EQ(map.road_markings.size(), 4);
}

}  // namespace road_logic_suite::test
