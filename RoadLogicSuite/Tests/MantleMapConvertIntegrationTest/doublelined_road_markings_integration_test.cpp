/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///
///  origin = (0.0, 0.0)
///
///                  Road "0"
///   |---------------------------------| ---> single road marking.
///   |          x x (Lane  2) x x      |
///   |=================================| ---> doublelined road marking, defined as "solid broken".
///   |          x x (Lane  1) x x      |
///   |=================================> ---> doublelined road marking, defined as "broken solid".
///   |          x x (Lane -1) x x      |
///   |=================================| ---> doublelined road marking, defined as "broken solid".
///   |          x x (Lane -2) x x      |
///   |---------------------------------| ---> single road marking.
///   ^                                 ^
///  s(0)                             s(100)
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithDoublelinedRoadMarkings_WhenParsingMap_ThenConversionOfLaneBoundaryTypeIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_doublelined_road_markings.xodr"), 1.0, 0.01, false);
    const auto& lanes = map.lanes;

    const auto expected_center_line_leftside_boundary_start_position_y = 0.12;
    const auto expected_center_line_rightside_boundary_start_position_y = -0.12;
    const auto expected_center_line_leftside_boundary_type = map_api::LaneBoundary::Type::kDashedLine;
    const auto expected_center_line_right_boundary_type = map_api::LaneBoundary::Type::kSolidLine;

    const auto expected_lane_minus_1_right_inner_boundary_start_position_y = -2.88;
    const auto expected_lane_minus_1_right_outer_boundary_start_position_y = -3.12;
    const auto expected_lane_minus_1_right_inner_boundary_type = map_api::LaneBoundary::Type::kDashedLine;
    const auto expected_lane_minus_1_right_outer_boundary_type = map_api::LaneBoundary::Type::kSolidLine;

    const auto expected_lane_1_right_inner_boundary_start_position_y = 2.88;
    const auto expected_lane_1_right_outer_boundary_start_position_y = 3.12;
    const auto expected_lane_1_right_inner_boundary_type = map_api::LaneBoundary::Type::kSolidLine;
    const auto expected_lane_1_right_outer_boundary_type = map_api::LaneBoundary::Type::kDashedLine;

    ASSERT_EQ(lanes.size(), 4);

    const auto lane_minus_1 = GetLaneWithFrontAt(0, -1.5);
    const auto lane_1 = GetLaneWithFrontAt(100, 1.5);

    // lane minus 1 left boundaries
    ASSERT_EQ(lane_minus_1.left_lane_boundaries.size(), 2);
    const auto lane_minus_1_left_boundary_leftside = lane_minus_1.left_lane_boundaries[0].get();
    const auto lane_minus_1_left_boundary_rightside = lane_minus_1.left_lane_boundaries[1].get();

    EXPECT_EQ(lane_minus_1_left_boundary_leftside.boundary_line.begin()->position.y.value(),
              expected_center_line_leftside_boundary_start_position_y);
    EXPECT_EQ(lane_minus_1_left_boundary_rightside.boundary_line.begin()->position.y.value(),
              expected_center_line_rightside_boundary_start_position_y);
    EXPECT_EQ(lane_minus_1_left_boundary_leftside.type, expected_center_line_leftside_boundary_type);
    EXPECT_EQ(lane_minus_1_left_boundary_rightside.type, expected_center_line_right_boundary_type);

    // lane minus 1 right boundaries
    ASSERT_EQ(lane_minus_1.right_lane_boundaries.size(), 2);
    const auto lane_minus_1_right_inner_boundary = lane_minus_1.right_lane_boundaries[0].get();
    const auto lane_minus_1_right_outer_boundary = lane_minus_1.right_lane_boundaries[1].get();

    EXPECT_EQ(lane_minus_1_right_inner_boundary.boundary_line.begin()->position.y.value(),
              expected_lane_minus_1_right_inner_boundary_start_position_y);
    EXPECT_EQ(lane_minus_1_right_outer_boundary.boundary_line.begin()->position.y.value(),
              expected_lane_minus_1_right_outer_boundary_start_position_y);
    EXPECT_EQ(lane_minus_1_right_inner_boundary.type, expected_lane_minus_1_right_inner_boundary_type);
    EXPECT_EQ(lane_minus_1_right_outer_boundary.type, expected_lane_minus_1_right_outer_boundary_type);

    // lane 1 left boundaries, order of elements in left boundaries(center lines) is changed due to the reverse
    // operation in conversion.
    ASSERT_EQ(lane_1.left_lane_boundaries.size(), 2);
    const auto lane_1_left_boundary_leftside = lane_1.left_lane_boundaries[1].get();
    const auto lane_1_right_boundary_rightside = lane_1.left_lane_boundaries[0].get();
    EXPECT_EQ(lane_1_left_boundary_leftside.boundary_line.begin()->position.y.value(),
              expected_center_line_leftside_boundary_start_position_y);
    EXPECT_EQ(lane_1_right_boundary_rightside.boundary_line.begin()->position.y.value(),
              expected_center_line_rightside_boundary_start_position_y);
    EXPECT_EQ(lane_1_left_boundary_leftside.type, expected_center_line_leftside_boundary_type);
    EXPECT_EQ(lane_1_right_boundary_rightside.type, expected_center_line_right_boundary_type);

    // lane 1 right boundaries
    ASSERT_EQ(lane_1.right_lane_boundaries.size(), 2);
    const auto lane_1_right_inner_boundary = lane_1.right_lane_boundaries[0].get();
    const auto lane_1_right_outer_boundary = lane_1.right_lane_boundaries[1].get();

    EXPECT_EQ(lane_1_right_inner_boundary.boundary_line.begin()->position.y.value(),
              expected_lane_1_right_inner_boundary_start_position_y);
    EXPECT_EQ(lane_1_right_outer_boundary.boundary_line.begin()->position.y.value(),
              expected_lane_1_right_outer_boundary_start_position_y);
    EXPECT_EQ(lane_1_right_inner_boundary.type, expected_lane_1_right_inner_boundary_type);
    EXPECT_EQ(lane_1_right_outer_boundary.type, expected_lane_1_right_outer_boundary_type);
}

}  // namespace road_logic_suite::test
