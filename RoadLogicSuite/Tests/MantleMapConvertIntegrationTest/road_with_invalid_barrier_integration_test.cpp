/*******************************************************************************
 * Copyright (C) 2024-2025, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithInvalidBarrier_WhenParsingMap_ThenConversionOfBarrierIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("simple_road_with_invalid_barrier.xodr"), 1.0, 0.01, false);
    const auto& lanes = map.lanes;

    const auto expected_lane_minus_2_barrier_last_point_x_position = 999 + 9.7037600280e-09;
    ASSERT_EQ(lanes.size(), 4);

    const auto& lane_minus_2 = GetLaneWithFrontAt(0, -4.5);
    EXPECT_EQ(lane_minus_2.left_lane_boundaries.size(), 1);
    EXPECT_EQ(lane_minus_2.right_lane_boundaries.size(), 2);

    // check if last invalid point of the road barrier is discarded: point whose x position is (1000 + 9.7037600280e-09)
    // is discarded, because it extends the road length of 1000.
    const auto lane_minus_2_barrier_last_point_x_position =
        lane_minus_2.right_lane_boundaries[1].get().boundary_line.back().position.x.value();
    EXPECT_EQ(lane_minus_2_barrier_last_point_x_position, expected_lane_minus_2_barrier_last_point_x_position);
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithInvalidBarrier_WhenParsingMap_ThenOutputWarning)
{
    const auto expected_warning_output =
        "[RoadLogicSuite: Barrier Conversion] WARNING: The s_end of the road barrier is larger than the road length "
        "(ID: 0). Points outside the range will be discarded during conversion!\n";

    testing::internal::CaptureStdout();
    EXPECT_NO_THROW(GetMap(GetResolvedMapPathInternal("simple_road_with_invalid_barrier.xodr"), 1.0, 0.01, false));
    std::string console_output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(console_output, expected_warning_output);
}

}  // namespace road_logic_suite::test
