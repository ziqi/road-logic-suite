/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using units::literals::operator""_m;

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithMultiSections_WhenParsingMap_ThenConversionOfRoadmarksIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_multisections.xodr"), 1.0, 0.01, false);

    const auto& lanes = map.lanes;

    // calculation of expected number of points of section 0 lane -1: 142.96(s value) / 1.0(default sampling_distance) +
    // 2(start point + end point) = 144
    const auto expected_num_section_0_lane_minus_1_points = 144;
    const auto expected_num_section_0_lane_minus_1_left_boundary_points = 51;
    const auto expected_num_section_0_lane_minus_1_right_boundary_points = 51;
    // calculation of expected number of points of section 1 lane -1: 471.6(s value) / 1.0(default sampling_distance) +
    // 2(start point + end point) = 473
    const auto expected_num_section_1_lane_minus_1_points = 473;
    const auto expected_num_section_1_lane_minus_1_left_boundary_points = 16;
    const auto expected_num_section_1_lane_minus_1_right_boundary_points = 473;

    const auto expected_num_section_2_lane_minus_1_points = 662;
    const auto expected_num_section_2_lane_minus_1_left_boundary_points = 662;
    const auto expected_num_section_2_lane_minus_1_right_boundary_points = 662;

    const auto expected_num_section_3_lane_minus_1_points = 58;
    const auto expected_num_section_3_lane_minus_1_left_boundary_points = 58;
    const auto expected_num_section_3_lane_minus_1_right_boundary_points = 51;

    ASSERT_EQ(lanes.size(), 18);

    // section 0
    const auto& section_0_lane_minus_1 = GetLaneWithFrontAt(1083.375, 1502.96);

    EXPECT_EQ(section_0_lane_minus_1.left_lane_boundaries.size(), 6);
    EXPECT_EQ(section_0_lane_minus_1.right_lane_boundaries.size(), 7);

    const auto& section_0_lane_minus_1_left_boundary = section_0_lane_minus_1.left_lane_boundaries[0].get();
    const auto& section_0_lane_minus_1_right_boundary = section_0_lane_minus_1.right_lane_boundaries[0].get();

    EXPECT_THAT(section_0_lane_minus_1_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1085.0_m, 1502.96_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_0_lane_minus_1_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1085.0_m, 1452.96_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_0_lane_minus_1_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1081.75_m, 1502.96_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_0_lane_minus_1_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1081.75_m, 1452.96_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    EXPECT_EQ(section_0_lane_minus_1.centerline.size(), expected_num_section_0_lane_minus_1_points);
    EXPECT_EQ(section_0_lane_minus_1_left_boundary.boundary_line.size(),
              expected_num_section_0_lane_minus_1_left_boundary_points);
    EXPECT_EQ(section_0_lane_minus_1_right_boundary.boundary_line.size(),
              expected_num_section_0_lane_minus_1_right_boundary_points);

    // section 1
    const auto& section_1_lane_minus_1 = GetLaneWithFrontAt(1083.375, 1360.0);

    EXPECT_EQ(section_1_lane_minus_1.left_lane_boundaries.size(), 2);
    EXPECT_EQ(section_1_lane_minus_1.right_lane_boundaries.size(), 1);

    const auto& section_1_lane_minus_1_left_boundary = section_1_lane_minus_1.left_lane_boundaries[0].get();
    const auto& section_1_lane_minus_1_right_boundary = section_1_lane_minus_1.right_lane_boundaries[0].get();

    EXPECT_THAT(section_1_lane_minus_1_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1085.0_m, 1360.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_1_lane_minus_1_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1085.0_m, 1345.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_1_lane_minus_1_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1081.75_m, 1360.0_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_1_lane_minus_1_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1267.511_m, 958.684_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    EXPECT_EQ(section_1_lane_minus_1.centerline.size(), expected_num_section_1_lane_minus_1_points);
    EXPECT_EQ(section_1_lane_minus_1_left_boundary.boundary_line.size(),
              expected_num_section_1_lane_minus_1_left_boundary_points);
    EXPECT_EQ(section_1_lane_minus_1_right_boundary.boundary_line.size(),
              expected_num_section_1_lane_minus_1_right_boundary_points);

    // section 2
    const auto& section_2_lane_minus_1 = GetLaneWithFrontAt(1268.42, 960.031);

    EXPECT_EQ(section_2_lane_minus_1.left_lane_boundaries.size(), 1);
    EXPECT_EQ(section_2_lane_minus_1.right_lane_boundaries.size(), 1);

    const auto& section_2_lane_minus_1_left_boundary = section_2_lane_minus_1.left_lane_boundaries[0].get();
    const auto& section_2_lane_minus_1_right_boundary = section_2_lane_minus_1.right_lane_boundaries[0].get();

    EXPECT_THAT(section_2_lane_minus_1_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1269.329_m, 961.378_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_2_lane_minus_1_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1854.95_m, 721.865_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_2_lane_minus_1_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1267.512_m, 958.684_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_2_lane_minus_1_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1852.137_m, 720.236_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));

    EXPECT_EQ(section_2_lane_minus_1.centerline.size(), expected_num_section_2_lane_minus_1_points);
    EXPECT_EQ(section_2_lane_minus_1_left_boundary.boundary_line.size(),
              expected_num_section_2_lane_minus_1_left_boundary_points);
    EXPECT_EQ(section_2_lane_minus_1_right_boundary.boundary_line.size(),
              expected_num_section_2_lane_minus_1_right_boundary_points);

    // section 3
    const auto& section_3_lane_minus_1 = GetLaneWithFrontAt(1853.543, 721.051);

    EXPECT_EQ(section_3_lane_minus_1.left_lane_boundaries.size(), 1);
    EXPECT_EQ(section_3_lane_minus_1.right_lane_boundaries.size(), 2);

    const auto& section_3_lane_minus_1_left_boundary = section_3_lane_minus_1.left_lane_boundaries[0].get();
    const auto& section_3_lane_minus_1_right_boundary = section_3_lane_minus_1.right_lane_boundaries[0].get();

    EXPECT_THAT(section_3_lane_minus_1_left_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1854.949_m, 721.865_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_3_lane_minus_1_left_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1879.105_m, 671.021_m, 0.0_m}, .width = 0.13_m, .height = 0.0_m}));
    EXPECT_THAT(section_3_lane_minus_1_right_boundary.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1852.137_m, 720.236_m, 0.0_m}, .width = 0.25_m, .height = 0.0_m}));
    EXPECT_THAT(section_3_lane_minus_1_right_boundary.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {1873.616_m, 675.539_m, 0.0_m}, .width = 0.25_m, .height = 0.0_m}));

    EXPECT_EQ(section_3_lane_minus_1.centerline.size(), expected_num_section_3_lane_minus_1_points);
    EXPECT_EQ(section_3_lane_minus_1_left_boundary.boundary_line.size(),
              expected_num_section_3_lane_minus_1_left_boundary_points);
    EXPECT_EQ(section_3_lane_minus_1_right_boundary.boundary_line.size(),
              expected_num_section_3_lane_minus_1_right_boundary_points);
}

}  // namespace road_logic_suite::test