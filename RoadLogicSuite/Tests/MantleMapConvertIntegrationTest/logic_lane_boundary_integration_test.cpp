/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///
///  origin = (0.0, 0.0)
///
///                  Road "0"
///   |---------------------------------| ---> single road marking, defined as "solid".
///   |          x x (Lane  2) x x      |
///   |=================================| ---> doublelined road marking, defined as "solid broken".
///   |          x x (Lane  1) x x      |
///   |=================================> ---> doublelined road marking, defined as "broken solid".
///   |          x x (Lane -1) x x      |
///   |=================================| ---> doublelined road marking, defined as "broken solid".
///   |          x x (Lane -2) x x      |
///   |---------------------------------| ---> single road marking, defined as "solid".
///   ^                                 ^
///  s(0)                             s(100)
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapWithDoublelinedRoadMarkings_WhenParsingMap_ThenConversionOfLogicLaneBoundaryIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_doublelined_road_markings.xodr"), 1.0, 0.01, false);
    const auto& logical_lane_boundaries = map.logical_lane_boundaries;
    const auto& reference_lines = map.reference_lines;
    ASSERT_EQ(reference_lines.size(), 1);
    const auto& reference_line = reference_lines[0];

    const auto expected_lane_minus_1_left_boundary_start_position_x = 0.0;
    const auto expected_lane_minus_1_left_boundary_start_position_y = 0.0;
    const auto expected_lane_minus_1_left_boundary_end_position_x = 100.0;
    const auto expected_lane_minus_1_left_boundary_end_position_y = 0.0;
    const auto expected_lane_minus_1_left_boundary_passing_rule =
        map_api::LogicalLaneBoundary::PassingRule::kDecreasingT;

    const auto expected_lane_mimus_1_right_boundary_start_position_x = 0.0;
    const auto expected_lane_mimus_1_right_boundary_start_position_y = -3.0;
    const auto expected_lane_mimus_1_right_boundary_end_position_x = 100.0;
    const auto expected_lane_mimus_1_right_boundary_end_position_y = -3.0;
    const auto expected_lane_mimus_1_right_boundary_passing_rule =
        map_api::LogicalLaneBoundary::PassingRule::kDecreasingT;

    const auto expected_lane_mimus_2_right_boundary_start_position_x = 0.0;
    const auto expected_lane_mimus_2_right_boundary_start_position_y = -6.0;
    const auto expected_lane_mimus_2_right_boundary_end_position_x = 100.0;
    const auto expected_lane_mimus_2_right_boundary_end_position_y = -6.0;
    const auto expected_lane_mimus_2_right_boundary_passing_rule =
        map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed;

    const auto expected_lane_1_left_boundary_start_position_x = 0.0;
    const auto expected_lane_1_left_boundary_start_position_y = 0.0;
    const auto expected_lane_1_left_boundary_end_position_x = 100.0;
    const auto expected_lane_1_left_boundary_end_position_y = 0.0;
    const auto expected_lane_1_left_boundary_passing_rule = map_api::LogicalLaneBoundary::PassingRule::kDecreasingT;

    const auto expected_lane_1_right_boundary_start_position_x = 100.0;
    const auto expected_lane_1_right_boundary_start_position_y = 3.0;
    const auto expected_lane_1_right_boundary_end_position_x = 0.0;
    const auto expected_lane_1_right_boundary_end_position_y = 3.0;
    const auto expected_lane_1_right_boundary_passing_rule = map_api::LogicalLaneBoundary::PassingRule::kDecreasingT;

    const auto expected_lane_2_right_boundary_start_position_x = 100.0;
    const auto expected_lane_2_right_boundary_start_position_y = 6.0;
    const auto expected_lane_2_right_boundary_end_position_x = 0.0;
    const auto expected_lane_2_right_boundary_end_position_y = 6.0;
    const auto expected_lane_2_right_boundary_passing_rule = map_api::LogicalLaneBoundary::PassingRule::kNoneAllowed;

    ASSERT_EQ(logical_lane_boundaries.size(), 6);
    const auto lane_minus_1_left_logic_boundary = logical_lane_boundaries[2].get();
    const auto lane_mimus_1_right_logic_boundary = logical_lane_boundaries[0].get();
    const auto lane_mimus_2_right_logic_boundary = logical_lane_boundaries[1].get();
    const auto lane_1_left_logic_boundary = logical_lane_boundaries[5].get();
    const auto lane_1_right_logic_boundary = logical_lane_boundaries[3].get();
    const auto lane_2_right_logic_boundary = logical_lane_boundaries[4].get();

    EXPECT_EQ(lane_minus_1_left_logic_boundary->boundary_line.begin()->position.x.value(),
              expected_lane_minus_1_left_boundary_start_position_x);
    EXPECT_EQ(lane_minus_1_left_logic_boundary->boundary_line.begin()->position.y.value(),
              expected_lane_minus_1_left_boundary_start_position_y);
    EXPECT_EQ(lane_minus_1_left_logic_boundary->boundary_line.rbegin()->position.x.value(),
              expected_lane_minus_1_left_boundary_end_position_x);
    EXPECT_EQ(lane_minus_1_left_logic_boundary->boundary_line.rbegin()->position.y.value(),
              expected_lane_minus_1_left_boundary_end_position_y);
    EXPECT_EQ(lane_minus_1_left_logic_boundary->reference_line, reference_line.get());
    EXPECT_EQ(lane_minus_1_left_logic_boundary->passing_rule, expected_lane_minus_1_left_boundary_passing_rule);
    ASSERT_EQ(lane_minus_1_left_logic_boundary->physical_boundaries.size(), 2);

    EXPECT_EQ(lane_mimus_1_right_logic_boundary->boundary_line.begin()->position.x.value(),
              expected_lane_mimus_1_right_boundary_start_position_x);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary->boundary_line.begin()->position.y.value(),
              expected_lane_mimus_1_right_boundary_start_position_y);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary->boundary_line.rbegin()->position.x.value(),
              expected_lane_mimus_1_right_boundary_end_position_x);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary->boundary_line.rbegin()->position.y.value(),
              expected_lane_mimus_1_right_boundary_end_position_y);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary->reference_line, reference_line.get());
    EXPECT_EQ(lane_mimus_1_right_logic_boundary->passing_rule, expected_lane_mimus_1_right_boundary_passing_rule);
    ASSERT_EQ(lane_mimus_1_right_logic_boundary->physical_boundaries.size(), 2);

    EXPECT_EQ(lane_mimus_2_right_logic_boundary->boundary_line.begin()->position.x.value(),
              expected_lane_mimus_2_right_boundary_start_position_x);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary->boundary_line.begin()->position.y.value(),
              expected_lane_mimus_2_right_boundary_start_position_y);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary->boundary_line.rbegin()->position.x.value(),
              expected_lane_mimus_2_right_boundary_end_position_x);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary->boundary_line.rbegin()->position.y.value(),
              expected_lane_mimus_2_right_boundary_end_position_y);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary->reference_line, reference_line.get());
    EXPECT_EQ(lane_mimus_2_right_logic_boundary->passing_rule, expected_lane_mimus_2_right_boundary_passing_rule);
    ASSERT_EQ(lane_mimus_2_right_logic_boundary->physical_boundaries.size(), 1);

    EXPECT_EQ(lane_1_left_logic_boundary->boundary_line.begin()->position.x.value(),
              expected_lane_1_left_boundary_start_position_x);
    EXPECT_EQ(lane_1_left_logic_boundary->boundary_line.begin()->position.y.value(),
              expected_lane_1_left_boundary_start_position_y);
    EXPECT_EQ(lane_1_left_logic_boundary->boundary_line.rbegin()->position.x.value(),
              expected_lane_1_left_boundary_end_position_x);
    EXPECT_EQ(lane_1_left_logic_boundary->boundary_line.rbegin()->position.y.value(),
              expected_lane_1_left_boundary_end_position_y);
    EXPECT_EQ(lane_1_left_logic_boundary->reference_line, reference_line.get());
    EXPECT_EQ(lane_1_left_logic_boundary->passing_rule, expected_lane_1_left_boundary_passing_rule);
    ASSERT_EQ(lane_1_left_logic_boundary->physical_boundaries.size(), 2);

    EXPECT_EQ(lane_1_right_logic_boundary->boundary_line.begin()->position.x.value(),
              expected_lane_1_right_boundary_start_position_x);
    EXPECT_EQ(lane_1_right_logic_boundary->boundary_line.begin()->position.y.value(),
              expected_lane_1_right_boundary_start_position_y);
    EXPECT_EQ(lane_1_right_logic_boundary->boundary_line.rbegin()->position.x.value(),
              expected_lane_1_right_boundary_end_position_x);
    EXPECT_EQ(lane_1_right_logic_boundary->boundary_line.rbegin()->position.y.value(),
              expected_lane_1_right_boundary_end_position_y);
    EXPECT_EQ(lane_1_right_logic_boundary->reference_line, reference_line.get());
    EXPECT_EQ(lane_1_right_logic_boundary->passing_rule, expected_lane_1_right_boundary_passing_rule);
    ASSERT_EQ(lane_1_right_logic_boundary->physical_boundaries.size(), 2);

    EXPECT_EQ(lane_2_right_logic_boundary->boundary_line.begin()->position.x.value(),
              expected_lane_2_right_boundary_start_position_x);
    EXPECT_EQ(lane_2_right_logic_boundary->boundary_line.begin()->position.y.value(),
              expected_lane_2_right_boundary_start_position_y);
    EXPECT_EQ(lane_2_right_logic_boundary->boundary_line.rbegin()->position.x.value(),
              expected_lane_2_right_boundary_end_position_x);
    EXPECT_EQ(lane_2_right_logic_boundary->boundary_line.rbegin()->position.y.value(),
              expected_lane_2_right_boundary_end_position_y);
    EXPECT_EQ(lane_2_right_logic_boundary->reference_line, reference_line.get());
    EXPECT_EQ(lane_2_right_logic_boundary->passing_rule, expected_lane_2_right_boundary_passing_rule);
    ASSERT_EQ(lane_2_right_logic_boundary->physical_boundaries.size(), 1);
}

}  // namespace road_logic_suite::test
