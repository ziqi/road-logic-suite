/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithMultisections_WhenParsingMap_ThenCountOfReferenceLinesIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("road_with_multisections.xodr"));
    const auto& reference_lanes = map.reference_lines;

    ASSERT_EQ(reference_lanes.size(), 4);
}

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithSimpleRoad_WhenParsingMap_ThenConversionOfReferenceLineIsCorrect)
{
    const auto expected_ploy_line_points_count = 1001;
    const auto expected_first_point_s_position = 0;
    const auto expected_last_point_s_position = 1000;
    const auto expected_first_point_x_position = 0;
    const auto expected_first_point_y_position = 0;
    const auto expected_last_point_x_position = 1000;
    const auto expected_last_point_y_position = 0;
    const auto expected_first_point_t_axis_yaw = M_PI_2;
    const auto expected_last_point_t_axis_yaw = M_PI_2;

    const auto& map = GetMap(GetResolvedMapPathInternal("simple_road.xodr"));
    const auto& reference_lanes = map.reference_lines;

    ASSERT_EQ(reference_lanes.size(), 1);

    const auto poly_line = reference_lanes.at(0)->poly_line;

    ASSERT_EQ(poly_line.size(), expected_ploy_line_points_count);

    EXPECT_EQ(poly_line.begin()->s_position.value(), expected_first_point_s_position);
    EXPECT_EQ(poly_line.rbegin()->s_position.value(), expected_last_point_s_position);
    EXPECT_EQ(poly_line.begin()->world_position.x.value(), expected_first_point_x_position);
    EXPECT_EQ(poly_line.begin()->world_position.y.value(), expected_first_point_y_position);
    EXPECT_EQ(poly_line.rbegin()->world_position.x.value(), expected_last_point_x_position);
    EXPECT_EQ(poly_line.rbegin()->world_position.y.value(), expected_last_point_y_position);
    EXPECT_EQ(poly_line.begin()->t_axis_yaw.value(), expected_first_point_t_axis_yaw);
    EXPECT_EQ(poly_line.rbegin()->t_axis_yaw.value(), expected_last_point_t_axis_yaw);
}

}  // namespace road_logic_suite::test
