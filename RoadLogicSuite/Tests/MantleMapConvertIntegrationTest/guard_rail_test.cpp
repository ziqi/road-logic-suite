/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using units::literals::operator""_m;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///
///  origin = (-5.0, -10.0)
/// IDs:
/// |LaneGroup: 9                   |
/// |-----------------------------  |
/// |Left LaneBoundary: 1           |
/// |Lane: 3                        |
/// |Right LaneBoundary: 6          |
/// |-----------------------------  |
/// |Left LaneBoundary: 6           |
/// |Lane: 4                        |
/// |Right LaneBoundary:7           |
/// |-----------------------------  |
/// |Left LaneBoundary: 7           |
/// |Lane: 5                        |
/// |Right LaneBoundary:8           |
/// |Right LaneBoundary:9 GuardRail |
/// |-----------------------------  |
///
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithGuardRail_WhenParsingMap_ThenConversionOfRoadsIsCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath("Highway1kmGuardRailRight.xodr"));

    const auto& lanes = map.lanes;

    ASSERT_EQ(lanes.size(), 3);
    EXPECT_EQ(lanes[0]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[0]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[1]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[1]->right_lane_boundaries.size(), 1);

    EXPECT_EQ(lanes[2]->left_lane_boundaries.size(), 1);
    EXPECT_EQ(lanes[2]->right_lane_boundaries.size(), 2);

    const auto& lane_boundary_guard_rail = lanes[2]->right_lane_boundaries[1].get();
    EXPECT_EQ(lane_boundary_guard_rail.type, LaneBoundary::Type::kGuardRail);

    EXPECT_THAT(lane_boundary_guard_rail.boundary_line.front(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {295.0_m, -19.75_m, 0.0_m}, .width = 0.5_m, .height = 0.9_m}));
    EXPECT_THAT(lane_boundary_guard_rail.boundary_line.back(),
                LaneBoundaryIsEqual(LaneBoundary::BoundaryPoint{
                    .position = {395.0_m, -19.75_m, 0.0_m}, .width = 0.5_m, .height = 0.9_m}));
}

}  // namespace road_logic_suite::test
