/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

#include <gmock/gmock.h>

namespace road_logic_suite::test
{
using units::literals::operator""_m;

///////////////////////////////////////////////////////////
/// @verbatim
/// OpenDrive Map
///
///  origin = (103.0, 120.0)
///
///                                          Road "2"
///                                         /      /
///          Road "0"       Road "1"       /      / (Lane -1)
///   |<=================|===============>\      /
///   | x x (Lane 1) x x | (Lane -1)       \    /
///   |------------------|------------------\---
///   | x x (Lane 2) x x | (Lane -2)    \    \
///   |------------------|-----------    \    \
///   | x x (Lane 3) x x | (Lane -3) \    \    \
///   |------------------|--------    \    \    \
///                               \    \    \    \
///                                \    \    \    \
///   ^                  ^
///  s(103)             s(0)
/// @endverbatim

TEST_F(MapConvertIntegrationTestFixture, GivenMapWithJunction_WhenParsingMap_ThenConversionOfLogicalLanesIsCorrect)
{

    const auto& map = GetMap(GetResolvedMapPathInternal("junction_example.xodr"), 1.0, 0.01, false);

    const auto& logical_lanes = map.logical_lanes;

    ASSERT_EQ(logical_lanes.size(), 7);
    const auto& road_2_lane_minus_1_logical_lane = logical_lanes[0];
    const auto& road_1_lane_minus_1_logical_lane = logical_lanes[1];
    const auto& road_1_lane_minus_2_logical_lane = logical_lanes[2];
    const auto& road_1_lane_minus_3_logical_lane = logical_lanes[3];
    const auto& road_0_lane_1_logical_lane = logical_lanes[4];
    const auto& road_0_lane_2_logical_lane = logical_lanes[5];
    const auto& road_0_lane_3_logical_lane = logical_lanes[6];

    const auto& reference_lines = map.reference_lines;
    ASSERT_EQ(reference_lines.size(), 3);
    const auto& road_2_reference_line = reference_lines[0];
    const auto& road_1_reference_line = reference_lines[1];
    const auto& road_0_reference_line = reference_lines[2];

    const auto road_2_lane_minus_1 = GetLaneWithFrontAt(103.0, 118.125);
    const auto road_1_lane_minus_2 = GetLaneWithFrontAt(103.0, 116.075);
    const auto road_0_lane_2 = GetLaneWithFrontAt(0.0, 116.075);

    // road 2
    ASSERT_EQ(road_2_lane_minus_1_logical_lane->left_boundaries.size(), 1);
    ASSERT_EQ(road_2_lane_minus_1_logical_lane->right_boundaries.size(), 1);

    const auto& road_2_lane_minus_1_logical_lane_left_boundary =
        road_2_lane_minus_1_logical_lane->left_boundaries.at(0).get();
    const auto& road_2_lane_minus_1_logical_lane_right_boundary =
        road_2_lane_minus_1_logical_lane->right_boundaries.at(0).get();

    EXPECT_THAT(road_2_lane_minus_1_logical_lane_left_boundary.boundary_line.front(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {103.0_m, 120.0_m, 0.0_m}, .s_position = 0.0_m, .t_position = 0.0_m}));
    EXPECT_THAT(road_2_lane_minus_1_logical_lane_left_boundary.boundary_line.back(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {120.0_m, 137.0_m, 0.0_m}, .s_position = 29.9257_m, .t_position = 0.0_m}));
    EXPECT_THAT(road_2_lane_minus_1_logical_lane_right_boundary.boundary_line.front(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {103.0_m, 116.25_m, 0.0_m}, .s_position = 0.0_m, .t_position = -3.75_m}));
    EXPECT_THAT(road_2_lane_minus_1_logical_lane_right_boundary.boundary_line.back(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {123.75_m, 137.0_m, 0.0_m}, .s_position = 29.9257_m, .t_position = -3.75_m}));

    EXPECT_EQ(road_2_lane_minus_1_logical_lane->reference_line, road_2_reference_line.get());

    EXPECT_EQ(road_2_lane_minus_1_logical_lane->right_adjacent_lanes.size(), 0);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane->left_adjacent_lanes.size(), 0);

    ASSERT_EQ(road_2_lane_minus_1_logical_lane->predecessor_lanes.size(), 1);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane->predecessor_lanes[0].get().id, road_0_lane_1_logical_lane->id);
    ASSERT_THAT(road_2_lane_minus_1_logical_lane->successor_lanes, ::testing::IsEmpty());

    ASSERT_EQ(road_2_lane_minus_1_logical_lane->physical_lane_references.size(), 1);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane->physical_lane_references.front().physical_lane, road_2_lane_minus_1);

    EXPECT_EQ(road_2_lane_minus_1_logical_lane->type, map_api::LogicalLane::Type::kUnknown);
    EXPECT_EQ(road_2_lane_minus_1_logical_lane->move_direction, map_api::LogicalLane::MoveDirection::kIncreasingS);

    // road 1
    ASSERT_EQ(road_1_lane_minus_2_logical_lane->left_boundaries.size(), 1);
    ASSERT_EQ(road_1_lane_minus_2_logical_lane->right_boundaries.size(), 1);

    const auto& road_1_lane_minus_2_logical_lane_left_boundary =
        road_1_lane_minus_2_logical_lane->left_boundaries.at(0).get();
    const auto& road_1_lane_minus_2_logical_lane_right_boundary =
        road_1_lane_minus_2_logical_lane->right_boundaries.at(0).get();

    EXPECT_THAT(road_1_lane_minus_2_logical_lane_left_boundary.boundary_line.front(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {103.0_m, 116.25_m, 0.0_m}, .s_position = 0.0_m, .t_position = -3.75_m}));
    EXPECT_THAT(road_1_lane_minus_2_logical_lane_left_boundary.boundary_line.back(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {116.25_m, 103.0_m, 0.0_m}, .s_position = 28.435_m, .t_position = -3.75_m}));
    EXPECT_THAT(road_1_lane_minus_2_logical_lane_right_boundary.boundary_line.front(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {103.0_m, 115.9_m, 0.0_m}, .s_position = 0.0_m, .t_position = -4.1_m}));
    EXPECT_THAT(road_1_lane_minus_2_logical_lane_right_boundary.boundary_line.back(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {115.9_m, 103.0_m, 0.0_m}, .s_position = 28.435_m, .t_position = -4.1_m}));

    EXPECT_EQ(road_1_lane_minus_2_logical_lane->reference_line, road_1_reference_line.get());

    ASSERT_EQ(road_1_lane_minus_2_logical_lane->right_adjacent_lanes.size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->right_adjacent_lanes[0].other_lane.id,
              road_1_lane_minus_3_logical_lane->id);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->right_adjacent_lanes[0].start_s, 0_m);
    EXPECT_NEAR(road_1_lane_minus_2_logical_lane->right_adjacent_lanes[0].end_s.value(), 28.435, 1e-3);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->right_adjacent_lanes[0].start_s_other, 0_m);
    EXPECT_NEAR(road_1_lane_minus_2_logical_lane->right_adjacent_lanes[0].end_s_other.value(), 28.435, 1e-3);

    ASSERT_EQ(road_1_lane_minus_2_logical_lane->left_adjacent_lanes.size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->left_adjacent_lanes[0].other_lane.id,
              road_1_lane_minus_1_logical_lane->id);

    ASSERT_EQ(road_1_lane_minus_2_logical_lane->predecessor_lanes.size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->predecessor_lanes[0].get().id, road_0_lane_2_logical_lane->id);
    ASSERT_THAT(road_1_lane_minus_2_logical_lane->successor_lanes, ::testing::IsEmpty());

    ASSERT_EQ(road_1_lane_minus_2_logical_lane->physical_lane_references.size(), 1);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->physical_lane_references.front().physical_lane, road_1_lane_minus_2);

    EXPECT_EQ(road_1_lane_minus_2_logical_lane->type, map_api::LogicalLane::Type::kBorder);
    EXPECT_EQ(road_1_lane_minus_2_logical_lane->move_direction, map_api::LogicalLane::MoveDirection::kIncreasingS);

    // road 0
    ASSERT_EQ(road_0_lane_2_logical_lane->left_boundaries.size(), 1);
    ASSERT_EQ(road_0_lane_2_logical_lane->right_boundaries.size(), 1);

    const auto& road_0_lane_2_logical_lane_left_boundary = road_0_lane_2_logical_lane->left_boundaries.at(0).get();
    const auto& road_0_lane_2_logical_lane_right_boundary = road_0_lane_2_logical_lane->right_boundaries.at(0).get();

    EXPECT_THAT(road_0_lane_2_logical_lane_left_boundary.boundary_line.front(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {0.0_m, 116.25_m, 0.0_m}, .s_position = 103.0_m, .t_position = 3.75_m}));
    EXPECT_THAT(road_0_lane_2_logical_lane_left_boundary.boundary_line.back(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {103.0_m, 116.25_m, 0.0_m}, .s_position = 0_m, .t_position = 3.75_m}));
    EXPECT_THAT(road_0_lane_2_logical_lane_right_boundary.boundary_line.front(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {0.0_m, 115.9_m, 0.0_m}, .s_position = 103.0_m, .t_position = 4.1_m}));
    EXPECT_THAT(road_0_lane_2_logical_lane_right_boundary.boundary_line.back(),
                LogicalLaneBoundaryIsEqual(map_api::LogicalBoundaryPoint{
                    .position = {103.0_m, 115.9_m, 0.0_m}, .s_position = 0_m, .t_position = 4.1_m}));

    EXPECT_EQ(road_0_lane_2_logical_lane->reference_line, road_0_reference_line.get());

    ASSERT_EQ(road_0_lane_2_logical_lane->right_adjacent_lanes.size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane->right_adjacent_lanes[0].other_lane.id, road_0_lane_1_logical_lane->id);
    ASSERT_EQ(road_0_lane_2_logical_lane->left_adjacent_lanes.size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane->left_adjacent_lanes[0].other_lane.id, road_0_lane_3_logical_lane->id);

    ASSERT_THAT(road_0_lane_2_logical_lane->predecessor_lanes, ::testing::IsEmpty());
    ASSERT_EQ(road_0_lane_2_logical_lane->successor_lanes.size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane->successor_lanes[0].get().id, road_1_lane_minus_2_logical_lane->id);

    ASSERT_EQ(road_0_lane_2_logical_lane->physical_lane_references.size(), 1);
    EXPECT_EQ(road_0_lane_2_logical_lane->physical_lane_references.front().physical_lane, road_0_lane_2);

    EXPECT_EQ(road_0_lane_2_logical_lane->type, map_api::LogicalLane::Type::kBorder);
    EXPECT_EQ(road_0_lane_2_logical_lane->move_direction, map_api::LogicalLane::MoveDirection::kDecreasingS);
}

}  // namespace road_logic_suite::test
