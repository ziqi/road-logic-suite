/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023-2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Tests/MantleMapConvertIntegrationTestFixture/mantle_map_convert_integration_test_fixture.h"

namespace road_logic_suite::test
{
using map_api::Identifier;
using map_api::Lane;
using map_api::LaneBoundary;
using map_api::Map;
using namespace units::literals;

constexpr double kAbsError = 1e-6;

TEST_F(MapConvertIntegrationTestFixture,
       GivenMapsWithAndWithoutGeoReference_WhenParsingMap_ThenGeoReferenceIsCorrectlyParsed)
{

    std::vector<std::tuple<std::string, std::string>> test_cases{
        {"3kmStraight.xodr",
         "+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=-194000 +y_0=-5346000 +datum=WGS84 +units=m +no_defs"}};

    for (const auto& [map_name, expected_geo_reference] : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name));

        EXPECT_EQ(map.projection_string, expected_geo_reference);
    }
}

/// @note disabled because lane groups are not supported in IMAP
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMap_WhenCreateMapFromFile_ThenLaneGroupsCountIsAsExpected)
{
    std::vector<std::pair<std::string, int>> test_cases{};

    for (const auto& [map_name, expected_number_of_lane_groups] : test_cases)
    {
        [[maybe_unused]] const auto& map = GetMap(GetResolvedMapPath(map_name));

        // EXPECT_EQ(map.GetLaneGroups().size(), expected_number_of_lane_groups) << map_name;
    }
}

TEST_F(MapConvertIntegrationTestFixture, GivenValidOdrMap_WhenCreateMapFromFile_ThenLanesCountIsAsExpected)
{
    std::vector<std::pair<std::string, std::uint8_t>> test_cases{{"3kmStraight.xodr", 2},
                                                                 {"3750mStraightMultipleRoads.xodr", 8}};

    for (const auto& [map_name, expected_number_of_lanes] : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name));
        const auto num_lanes = map.lanes.size();

        EXPECT_EQ(num_lanes, expected_number_of_lanes) << map_name;
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidMapWithLaneWithTwoRoadmarks_WhenCreateMapFromFile_ThenBothRoadmarksAreParsed)
{
    const auto& map = GetMap(GetResolvedMapPath(""));

    ASSERT_EQ(map.lanes.size(), 1);
    ASSERT_EQ(map.lane_boundaries.size(), 3);

    const auto& lane = map.lanes.at(0);
    ASSERT_EQ(lane->left_lane_boundaries.size(), 1);
    ASSERT_EQ(lane->right_lane_boundaries.size(), 2);

    const auto first_right_lane_boundary = lane->right_lane_boundaries.at(0);
    const auto second_right_lane_boundary = lane->right_lane_boundaries.at(1);

    EXPECT_EQ(first_right_lane_boundary.get().type, LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(second_right_lane_boundary.get().type, LaneBoundary::Type::kSolidLine);
}

TEST_F(MapConvertIntegrationTestFixture,
       GivenValidOdrMap_WhenCreateMapFromFile_ThenCorrectLeftRightAdjacencyCountIsAsExpected)
{
    // map, left, right
    std::vector<std::tuple<std::string, std::size_t, std::size_t>> test_cases{
        {"3kmStraight.xodr", 1, 1}, {"3750mStraightMultipleRoads.xodr", 8, 4}};

    for (const auto& [map_name, expected_left_lanes_count, expected_right_lane_count] : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name));

        std::size_t left_adjacent_lanes = 0;
        std::size_t right_adjacent_lanes = 0;

        for (const auto& lane : map.lanes)
        {
            left_adjacent_lanes += lane->left_adjacent_lanes.size();
            right_adjacent_lanes += lane->right_adjacent_lanes.size();
        }

        EXPECT_EQ(left_adjacent_lanes, expected_left_lanes_count) << map_name;
        EXPECT_EQ(right_adjacent_lanes, expected_right_lane_count) << map_name;
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapsAndNoDownsampling_WhenCreateMapFromFile_ThenCenterLineSampleCountIsAsExpected)
{
    const std::vector<std::pair<std::string, int>> test_cases{};

    double lane_marking_distance_in_m = 1;
    double lane_marking_downsampling_epsilon = 0.01;
    bool lane_marking_downsampling = false;

    for (const auto& [map_name, expected_centerline_points] : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name),
                                 lane_marking_distance_in_m,
                                 lane_marking_downsampling_epsilon,
                                 lane_marking_downsampling);
        const auto& lanes = map.lanes;
        ASSERT_EQ(1, lanes.size()) << map_name;

        const auto& imap_lane = lanes.front();
        const auto& centerline_sample_count = imap_lane->centerline.size();
        EXPECT_EQ(centerline_sample_count, expected_centerline_points) << map_name;
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapsWithDownsampling_WhenCreateMapFromFile_ThenCenterLineSampleCountIsAsExpected)
{
    const std::vector<std::pair<std::string, int>> test_cases{};

    double lane_marking_distance_in_m = 0.01;
    double lane_marking_downsampling_epsilon = 1.0;
    bool lane_marking_downsampling = true;

    for (const auto& [map_name, expected_centerline_points] : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name),
                                 lane_marking_distance_in_m,
                                 lane_marking_downsampling_epsilon,
                                 lane_marking_downsampling);
        const auto& lanes = map.lanes;
        ASSERT_EQ(1, lanes.size()) << map_name;

        const auto& lane = lanes.front();
        const auto& centerline_sample_count = lane->centerline.size();
        EXPECT_EQ(centerline_sample_count, expected_centerline_points) << map_name;
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenOdrMapWithJunctionAndLaneSplit_WhenCreateMapFromFile_ThenCenterLineHasEnoughSamples)
{
    const std::vector<std::string> map_names{};

    double lane_marking_distance_in_m = 0.4;
    double lane_marking_downsampling_epsilon = 1.0;
    bool lane_marking_downsampling = false;

    for (const auto& map_name : map_names)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name),
                                 lane_marking_distance_in_m,
                                 lane_marking_downsampling_epsilon,
                                 lane_marking_downsampling);
        const auto& lanes = map.lanes;
        ASSERT_GT(lanes.size(), 1);

        for (const auto& lane : lanes)
        {
            const auto& centerline_sample_count = lane->centerline.size();
            EXPECT_GT(centerline_sample_count, 2);
        }
    }
}

TEST_F(MapConvertIntegrationTestFixture, GivenValidOdrMap_WhenCreateMapFromFile_ThenCenterLineDirectionIsAsExpected)
{
    //.second is length of lanes in the map
    std::vector<std::string> test_cases{{"3750mStraightMultipleRoads.xodr"}};

    for (const auto& test_case : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(test_case));

        for (const auto& lane : map.lanes)
        {
            for (std::uint64_t i = 1; i < lane->centerline.size(); ++i)
            {
                if (lane->centerline[0].y < 0_m)
                {
                    EXPECT_GT(lane->centerline[i].x, lane->centerline[i - 1].x);
                }
                else
                {
                    EXPECT_LT(lane->centerline[i].x, lane->centerline[i - 1].x);
                }
            }
        }
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrJunctionMap_WhenCreateMapFromFile_ThenJunctionCountIsAsExpected)
{
    std::vector<std::tuple<std::string, int>> test_cases{};

    for (const auto& test_case : test_cases)
    {
        const auto& map_name = std::get<0>(test_case);
        const auto& expected_intersections = std::get<1>(test_case);

        const auto& map = GetMap(GetResolvedMapPath(map_name));
        const auto num_intersections = std::count_if(map.lanes.begin(), map.lanes.end(), [](const auto& lane) {
            return lane->type == Lane::Type::kIntersection;
        });
        EXPECT_EQ(num_intersections, expected_intersections);
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrJunctionMap_WhenCreateMapFromFile_ThenPredecessorSuccessorCountIsAsExpected)
{
    std::vector<std::tuple<std::string, int, int>> test_cases{};

    for (const auto& test_case : test_cases)
    {
        const auto& map_name = std::get<0>(test_case);
        const auto& expected_antecessor = std::get<1>(test_case);
        const auto& expected_successors = std::get<2>(test_case);

        const auto& map = GetMap(GetResolvedMapPath(map_name));

        std::uint64_t num_antecessor = 0;
        std::uint64_t num_successors = 0;
        for (const auto& lane : map.lanes)
        {
            if (lane->type != Lane::Type::kIntersection)
            {
                continue;
            }

            num_antecessor += lane->antecessor_lanes.size();
            num_successors += lane->successor_lanes.size();
        }

        EXPECT_EQ(num_antecessor, expected_antecessor);
        EXPECT_EQ(num_successors, expected_successors);
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMap_WhenCreateMapFromFile_ThenNumberOfBoundariesIsAsExpected)
{
    std::vector<std::pair<std::string, std::size_t>> test_cases{};

    for (const auto& [map_name, expected_number_of_boundaries] : test_cases)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name));

        std::size_t num_boundaries = 0;
        for (const auto& lane : map.lanes)
        {
            num_boundaries += lane->left_lane_boundaries.size() + lane->right_lane_boundaries.size() +
                              lane->free_lane_boundaries.size();
        }

        EXPECT_EQ(num_boundaries, expected_number_of_boundaries) << map_name;
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapWithDoubleLaneBoundary_WhenCreateMapFromFile_ThenBoundaryPositionsAreCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath(""));

    ASSERT_EQ(map.lanes.size(), 1);

    const auto& lane = map.lanes[0];
    ASSERT_EQ(lane->right_lane_boundaries.size(), 2);
    ASSERT_EQ(lane->left_lane_boundaries.size(), 1);

    const auto& dashed_right_boundary = lane->right_lane_boundaries[0];
    ASSERT_GT(dashed_right_boundary.get().boundary_line.size(), 0);
    const auto& dashed_lane_boundary_point = dashed_right_boundary.get().boundary_line[0].position;

    const auto& solid_right_boundary = lane->right_lane_boundaries[1];
    ASSERT_GT(solid_right_boundary.get().boundary_line.size(), 0);
    const auto& solid_lane_boundary_point = solid_right_boundary.get().boundary_line[0].position;

    const auto& left_lane_boundary = lane->left_lane_boundaries[0];
    ASSERT_GT(left_lane_boundary.get().boundary_line.size(), 0);
    const auto& left_lane_boundary_point = left_lane_boundary.get().boundary_line[0].position;

    EXPECT_NEAR(left_lane_boundary_point.x(), 0.0, 0.002);
    EXPECT_NEAR(dashed_lane_boundary_point.x(), 0.0, 0.002);
    EXPECT_NEAR(solid_lane_boundary_point.x(), 0.0, 0.002);

    // odr center lane is at 0.0
    EXPECT_NEAR(left_lane_boundary_point.y(), 0.0, 0.002);
    // odr boundary is at -3.2, see TakeRoadMarkSample() for explanation of result
    EXPECT_NEAR(dashed_lane_boundary_point.y(), -3.08, 0.002);
    EXPECT_NEAR(solid_lane_boundary_point.y(), -3.32, 0.002);
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_DoubleRightLaneBoundaryLongerThan1km_WhenCreateMapFromFile_ThenBoundaryAssignmentsAreCorrect)
{
    std::size_t expected_right_lane_boundaries_count{2};
    std::size_t expected_left_lane_boundaries_count{1};

    const auto& map = GetMap(GetResolvedMapPath(""));

    ASSERT_EQ(map.lanes.size(), 1);
    const auto& lane = map.lanes[0];

    ASSERT_EQ(lane->left_lane_boundaries.size(), expected_left_lane_boundaries_count);
    ASSERT_EQ(lane->right_lane_boundaries.size(), expected_right_lane_boundaries_count);
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_DoubleCenterLaneBoundaryLongerThan1km_CreateMapFromFile_BoundaryAssignmentsAreCorrect)
{
    std::size_t expected_right_lane_boundaries_count{1};
    std::size_t expected_left_lane_boundaries_count{2};

    const auto& map = GetMap(GetResolvedMapPath(""));

    ASSERT_EQ(map.lanes.size(), 1);
    const auto& lane = map.lanes[0];

    ASSERT_EQ(lane->left_lane_boundaries.size(), expected_left_lane_boundaries_count);
    ASSERT_EQ(lane->right_lane_boundaries.size(), expected_right_lane_boundaries_count);
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapWithDoubleLaneBoundary_WhenCreateMapFromFile_ThenBoundaryAssignmentsAreCorrect)
{
    const auto& map = GetMap(GetResolvedMapPath(""));

    ASSERT_EQ(map.lanes.size(), 2);
    std::size_t expected_right_lane_boundaries_count{1};
    std::size_t expected_left_lane_boundaries_count{2};

    const auto test_boundary_assignment =
        [&map, expected_left_lane_boundaries_count, expected_right_lane_boundaries_count](const Lane& lane) {
            ASSERT_EQ(lane.left_lane_boundaries.size(), expected_left_lane_boundaries_count);
            ASSERT_EQ(lane.right_lane_boundaries.size(), expected_right_lane_boundaries_count);
        };

    // road direction, right (negative) lanes
    test_boundary_assignment(*map.lanes[0]);
    // against road direction, left (positive) lanes
    test_boundary_assignment(*map.lanes[1]);
}

/// @note disabled because IMAP has no mirrored_from property in lane boundaries
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapWithDoubleLaneBoundary_WhenCreateMapFromFile_ThenMirroredFromIsCorrectlySet)
{
    EXPECT_TRUE(true);
}

TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapAndOdrPosition_WhenGetWorldPoseFromOdrPosition_ThenCorrectPoseIsReturned)
{
    EXPECT_TRUE(true);
}

/// @note  disabled because no coordinate converter in IMAP interface
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMapAndOpenDriveLanePosition_WhenGetWorldPosition_ThenExpectedWorldPositionIsReturned)
{
    EXPECT_TRUE(true);
}

/// @note  disabled because no coordinate converter in IMAP interface
TEST_F(MapConvertIntegrationTestFixture, DISABLED_GivenValidOdrMapAndNdsPositon_WhenGetWorldPosition_ThenAssertionHit)
{
    EXPECT_TRUE(true);
}

/// @note disabled because the used maps do not exist
TEST_F(
    MapConvertIntegrationTestFixture,
    DISABLED_GivenMapWithLaneOffsetNotStartingAtZero_WhenCreateMapFromFile_ThenGetCenterLaneOffsetAtSOffsetDoesntThrow)
{
    EXPECT_NO_THROW(GetMap(GetResolvedMapPath("")));
}

TEST_F(MapConvertIntegrationTestFixture, GivenValidOdrMap_WhenCreateMapFromFile_ThenAllLanesHaveBoundaries)
{
    const auto& map = GetMap(GetResolvedMapPath("SingleLaneCircle1kmRadius.xodr"));
    for (const auto& lane : map.lanes)
    {
        EXPECT_FALSE(lane->left_lane_boundaries.empty());
        EXPECT_FALSE(lane->right_lane_boundaries.empty());
    }
}

/// @note disabled because the used maps do not exist
TEST_F(MapConvertIntegrationTestFixture,
       DISABLED_GivenValidOdrMap_WhenCreateMapFromFile_ThenNoGapsBetweenSucceedingLanes)
{
    std::vector<std::string> maps{};

    for (const auto& map_name : maps)
    {
        const auto& map = GetMap(GetResolvedMapPath(map_name));

        for (const auto& lane : map.lanes)
        {
            for (const auto& successor_lane : lane->successor_lanes)
            {
                const auto& centerline = lane->centerline;
                EXPECT_NEAR(centerline.back().x.value(), successor_lane.get().centerline.front().x.value(), kAbsError)
                    << ToString(map);
                EXPECT_NEAR(centerline.back().y.value(), successor_lane.get().centerline.front().y.value(), kAbsError)
                    << ToString(map);
                EXPECT_NEAR(centerline.back().z.value(), successor_lane.get().centerline.front().z.value(), kAbsError)
                    << ToString(map);
            }
        }
    }
}

TEST_F(MapConvertIntegrationTestFixture,
       GivenTwoRoadMarksWhichAreLessThan15mmApart_WhenCreateMapFromFile_ThenInvalidLaneBoundaryIsRemoved)
{
    constexpr std::size_t expected_lane_boundary_size{6};

    const auto& map = GetMap(GetResolvedMapPath("InvalidRoadMarkDistance.xodr"));

    ASSERT_EQ(map.lanes.size(), 2);

    uint64_t total_boundaries = 0;
    for (const auto& lane : map.lanes)
    {
        total_boundaries +=
            lane->left_lane_boundaries.size() + lane->right_lane_boundaries.size() + lane->free_lane_boundaries.size();
    }

    EXPECT_EQ(total_boundaries, expected_lane_boundary_size);
}

// possible further tests, depending on our implementation:
// SpecialBoundariesInMap_CorrectNumberOfBoundaryPoints

// #############################################################################################################
// The following Tests are universally applicable to all maps without knowledge about the map. The List of
// tested maps is kAllMaps. Any Map can be added to it, the more the merrier.
// #############################################################################################################
// NOLINTNEXTLINE(cert-err58-cpp)
const std::vector<std::string> kAllMaps = {{"3kmStraight.xodr"},
                                           {"3750mStraightMultipleRoads.xodr"},
                                           {"RoadWithNonDrivingLanes.xodr"},
                                           {"CurvedRoad2km_LateralProfile.xodr"},
                                           {"SingleLaneCircle1kmRadius.xodr"}};

// NOLINTNEXTLINE(fuchsia-multiple-inheritance) - accepted: Baseclass used in different testing use-cases
class AllMapsOdrToMapConverterTests : public MapConvertIntegrationTestFixture,
                                      public testing::WithParamInterface<std::string>
{
  public:
    const Map& GetMap() { return MapConvertIntegrationTestFixture::GetMap(GetResolvedMapPath(GetParam())); }
};

TEST_P(AllMapsOdrToMapConverterTests, GivenValidOdrMap_WhenCreateMapFromFile_ThenAllIdsAreUnique)
{
    const auto& map = GetMap();

    // first gather them all
    std::map<Identifier, int> unique_id_occurrences{};
    for (const auto& traffic_sign : map.traffic_signs)
    {
        unique_id_occurrences[traffic_sign->id]++;
    }
    for (const auto& traffic_light : map.traffic_lights)
    {
        unique_id_occurrences[traffic_light->id]++;
    }
    for (const auto& stationary_object : map.stationary_objects)
    {
        unique_id_occurrences[stationary_object->id]++;
    }
    for (const auto& lane : map.lanes)
    {
        unique_id_occurrences[lane->id]++;
    }

    const auto expect_unique_identifier = [unique_id_occurrences](const Identifier& object_id,
                                                                  const std::string& object_type) {
        EXPECT_LE(unique_id_occurrences.at(object_id), 1)
            << "Found non-unique id " + std::to_string(object_id) + " for object of type " + object_type;
    };

    for (const auto& traffic_sign : map.traffic_signs)
    {
        expect_unique_identifier(traffic_sign->id, "traffic sign");
    }
    for (const auto& traffic_light : map.traffic_lights)
    {
        expect_unique_identifier(traffic_light->id, "traffic light");
    }
    for (const auto& stationary_object : map.stationary_objects)
    {
        expect_unique_identifier(stationary_object->id, "stationary object");
    }
    for (const auto& lane : map.lanes)
    {
        expect_unique_identifier(lane->id, "lane");
    }
}

/// @note disabled because there are no lane groups in IMAP
TEST_P(AllMapsOdrToMapConverterTests, DISABLED_GivenValidOdrMap_WhenCreateMapFromFile_ThenNoEmptyLaneGroups)
{
    EXPECT_TRUE(true);
}

TEST_P(AllMapsOdrToMapConverterTests, GivenValidOdrMap_WhenCreateMapFromFile_ThenAtLeastTwoPointsInCenterLines)
{
    const auto& map = GetMap();

    for (const auto& lane : map.lanes)
    {
        EXPECT_GE(lane->centerline.size(), 2);
    }
}

TEST_P(AllMapsOdrToMapConverterTests, GivenValidOdrMap_WhenCreateMapFromFile_ThenAllLaneBoundariesHaveAtLeastTwoPoints)
{
    const auto& map = GetMap();

    for (const auto& lane : map.lanes)
    {
        for (const auto& lane_boundary : lane->left_lane_boundaries)
        {
            EXPECT_GE(lane_boundary.get().boundary_line.size(), 2) << "on boundary with id " << lane_boundary.get().id;
        }

        for (const auto& lane_boundary : lane->right_lane_boundaries)
        {
            EXPECT_GE(lane_boundary.get().boundary_line.size(), 2) << "on boundary with id " << lane_boundary.get().id;
        }
    }
}

INSTANTIATE_TEST_CASE_P(AllMapTests, AllMapsOdrToMapConverterTests, testing::ValuesIn(kAllMaps));

}  // namespace road_logic_suite::test