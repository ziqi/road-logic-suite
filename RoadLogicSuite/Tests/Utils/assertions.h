/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_ASSERTIONS_H
#define ROADLOGICSUITE_ASSERTIONS_H

#include "RoadLogicSuite/Internal/Types/vec2.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

/**
 * This expectation wraps around the google test EXPECT_NEAR to support the units package.
 */
#define EXPECT_NEAR_UNITS(val1, val2, abs_error) EXPECT_NEAR(val1(), val2(), abs_error)

namespace road_logic_suite::test
{

static constexpr double kEpsilon = 1e-3;

void ExpectNearPosition(mantle_api::Vec3<units::length::meter_t> pos1,
                        mantle_api::Vec3<units::length::meter_t> pos2,
                        double abs_error = kEpsilon)
{
    EXPECT_NEAR(pos1.x(), pos2.x(), abs_error);
    EXPECT_NEAR(pos1.y(), pos2.y(), abs_error);
    EXPECT_NEAR(pos1.z(), pos2.z(), abs_error);
}

MATCHER_P(IsAlmostEqual, expected_value, "")
{
    return std::fabs((arg - expected_value).value()) <= kEpsilon;
}

MATCHER_P(InertialPositionIsNear, expected_value, "")
{
    using namespace testing;
    return ExplainMatchResult(
        AllOf(Field("x", &mantle_api::Vec3<units::length::meter_t>::x, IsAlmostEqual(expected_value.x)),
              Field("y", &mantle_api::Vec3<units::length::meter_t>::y, IsAlmostEqual(expected_value.y)),
              Field("z", &mantle_api::Vec3<units::length::meter_t>::z, IsAlmostEqual(expected_value.z))),
        arg,
        result_listener);
}

MATCHER_P(Vec3IsNear, expected_value, "")
{

    using namespace testing;
    return ExplainMatchResult(
        AllOf(Field("x", &mantle_api::Vec3<units::length::meter_t>::x, IsAlmostEqual(expected_value.x)),
              Field("y", &mantle_api::Vec3<units::length::meter_t>::y, IsAlmostEqual(expected_value.y)),
              Field("z", &mantle_api::Vec3<units::length::meter_t>::z, IsAlmostEqual(expected_value.z))),
        arg,
        result_listener);
}

MATCHER_P(LanePositionIsNear, expected_value, "Lane-Position are not equal")
{
    using namespace testing;
    return ExplainMatchResult(
        AllOf(Field("road", &mantle_api::OpenDriveLanePosition::road, expected_value.road),
              Field("lane", &mantle_api::OpenDriveLanePosition::lane, expected_value.lane),
              Field("s", &mantle_api::OpenDriveLanePosition::s_offset, IsAlmostEqual(expected_value.s_offset)),
              Field("t", &mantle_api::OpenDriveLanePosition::t_offset, IsAlmostEqual(expected_value.t_offset))),
        arg,
        result_listener);
}

std::vector<std::array<double, 2>> ConvertToDouble(
    const std::vector<road_logic_suite::Vec2<units::length::meter_t>>& in)
{
    std::vector<std::array<double, 2>> out{};
    for (const auto& in_elem : in)
    {
        std::array<double, 2> out_elem{in_elem.x(), in_elem.y()};
        out.push_back(out_elem);
    }
    return out;
}

void AssertShapeEquality(const std::vector<std::array<double, 2>>& actual_shape,
                         const std::vector<std::array<double, 2>>& expected_shape,
                         double abs_error = kEpsilon)
{
    ASSERT_EQ(actual_shape.size(), expected_shape.size());
    for (std::size_t i = 0; i < actual_shape.size(); i++)
    {
        ASSERT_NEAR(actual_shape[i][0], expected_shape[i][0], abs_error);
        ASSERT_NEAR(actual_shape[i][1], expected_shape[i][1], abs_error);
    }
}

}  // namespace road_logic_suite::test

#endif  // ROADLOGICSUITE_ASSERTIONS_H
