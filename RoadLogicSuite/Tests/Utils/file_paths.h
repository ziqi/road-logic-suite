/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef ROADLOGICSUITE_FILE_PATHS_H
#define ROADLOGICSUITE_FILE_PATHS_H

#include <stdexcept>

namespace road_logic_suite::test::utils
{

/**
 * Checks if a file exists and tries to resolve the file path via bazel "Runfile"-information if needed.
 * Bazel puts file information in a file called "*.exe.runfiles_manifest".
 *
 * Throws a std::runtime_error if the path does not exist and cannot be resolved.
 *
 * @param path
 * @return an existing path or throws an exception
 */
std::string Resolve(const std::string& path);

}  // namespace road_logic_suite::test::utils

#endif  // ROADLOGICSUITE_FILE_PATHS_H
