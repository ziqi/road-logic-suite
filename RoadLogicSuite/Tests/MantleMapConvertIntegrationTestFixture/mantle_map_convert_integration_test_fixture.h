/*******************************************************************************
 * Copyright (C) 2023-2024, ANSYS, Inc.
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/Internal/MapValidation/validation_runner.h"
#include "RoadLogicSuite/Tests/Utils/assertions.h"
#include "RoadLogicSuite/Tests/Utils/file_paths.h"
#include "RoadLogicSuite/road_logic_suite.h"

#include <MapAPI/lane.h>
#include <MapAPI/lane_boundary.h>
#include <MapAPI/map.h>
#include <gtest/gtest.h>

namespace road_logic_suite::test
{

std::string GetResolvedMapPath(const std::string& map_path)
{
    const std::string external_test_data_folder = "./ThirdParty/ExternalTestData/Maps/";

    return test::utils::Resolve(external_test_data_folder + map_path);
}

std::string GetResolvedMapPathInternal(const std::string& map_path)
{

    const std::string internal_test_data_folder = "./RoadLogicSuite/Tests/Data/Maps/";

    return test::utils::Resolve(internal_test_data_folder + map_path);
}

/// @brief  integration test mapping to IMap OpenDrive unit tests
class MapConvertIntegrationTestFixture : public ::testing::Test
{
  public:
    /// @brief fixture function to get IMap using RoadLogicSuite
    /// @param map_name name of the input Opendrive map
    /// @param lane_marking_distance_in_m OpenDriveProperties will be added later
    /// @param lane_marking_downsampling_epsilon OpenDriveProperties will be added later
    /// @param lane_marking_downsampling OpenDriveProperties will be added later
    /// @return IMap
    map_api::Map& GetMap(const std::string& map_path,
                         [[maybe_unused]] const double lane_marking_distance_in_m = 0.4,
                         const double lane_marking_downsampling_epsilon = 0.01,
                         const bool lane_marking_downsampling = true)
    {
        // TODO: Is sampling_distance the same as lane_marking_distance_in_m?
        MapConverterConfig converter_config{
            .downsampling = lane_marking_downsampling,
            .downsampling_epsilon = units::length::meter_t{lane_marking_downsampling_epsilon}};
        RoadLogicSuite suite_{converter_config};
        map_ = suite_.LoadMap(map_path);
        return *map_;
    }

    /**
     * Returns a lane that whose start point is at the given position.
     * @param x inertial x position.
     * @param y inertial y position.
     * @return a lane whose first point is at that given position or the method fails with an exception if no lane can
     * be found.
     */
    map_api::Lane& GetLaneWithFrontAt(const double x, const double y) const
    {
        const auto candidate = std::find_if(map_->lanes.begin(), map_->lanes.end(), [x, y](const auto& lane) {
            return std::abs(lane->centerline.front().x.value() - x) < kEpsilon &&
                   std::abs(lane->centerline.front().y.value() - y) < kEpsilon;
        });

        if (candidate != map_->lanes.end())
        {
            return **candidate;
        }
        const auto msg = "Did not find lane @ x=" + std::to_string(x) + ",y=" + std::to_string(y) + "!\n";
        throw std::runtime_error(msg);
    }

    std::string ToString(const map_api::Map& map) const { return ""; }

    std::unique_ptr<map_api::Map> map_{};
};

using MantlePosition = mantle_api::Vec3<units::length::meter_t>;
MATCHER_P(MantlePositionIsNear, expected_value, "")
{
    using namespace testing;
    return ExplainMatchResult(AllOf(Field("x", &MantlePosition::x, IsAlmostEqual(expected_value.x)),
                                    Field("y", &MantlePosition::y, IsAlmostEqual(expected_value.y)),
                                    Field("z", &MantlePosition::z, IsAlmostEqual(expected_value.z))),
                              arg,
                              result_listener);
}

MATCHER_P(LaneBoundaryIsEqual, expected_value, "")
{
    using namespace testing;
    return ExplainMatchResult(
        AllOf(Field("position",
                    &map_api::LaneBoundary::BoundaryPoint::position,
                    MantlePositionIsNear(expected_value.position)),
              Field("width", &map_api::LaneBoundary::BoundaryPoint::width, Eq(expected_value.width)),
              Field("height", &map_api::LaneBoundary::BoundaryPoint::height, Eq(expected_value.height))),
        arg,
        result_listener);
}

MATCHER_P(LogicalLaneBoundaryIsEqual, expected_value, "")
{
    using namespace testing;
    return ExplainMatchResult(
        AllOf(
            Field("position", &map_api::LogicalBoundaryPoint::position, MantlePositionIsNear(expected_value.position)),
            Field("s_position", &map_api::LogicalBoundaryPoint::s_position, IsAlmostEqual(expected_value.s_position)),
            Field("t_position", &map_api::LogicalBoundaryPoint::t_position, IsAlmostEqual(expected_value.t_position))),
        arg,
        result_listener);
}

}  // namespace road_logic_suite::test
