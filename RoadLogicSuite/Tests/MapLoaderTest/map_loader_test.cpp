/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/map_loader.h"

#include "RoadLogicSuite/Tests/Utils/assertions.h"
#include "RoadLogicSuite/Tests/Utils/file_paths.h"

#include <gtest/gtest.h>

namespace road_logic_suite::test
{

TEST(MapLoaderTest, GivenValidOpenDriveMap_WhenLoadMap_ThenDefaultMapReturned)
{
    const auto filepath = utils::Resolve("RoadLogicSuite/Tests/Data/Maps/simple_road.xodr");

    MapLoader map_loader;
    auto map = map_loader.LoadMap(filepath);
    EXPECT_EQ(map->lanes.size(), 4);
    EXPECT_EQ(map->lane_boundaries.size(), 6);
}

}  // namespace road_logic_suite::test
