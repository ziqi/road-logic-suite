/*******************************************************************************
 * Copyright (C) 2023, ANSYS, Inc.
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadLogicSuite/road_logic_suite.h"

#include "RoadLogicSuite/Internal/MapConversion/mantle_map_converter.h"
#include "RoadLogicSuite/Internal/road_logic_suite_impl.h"

#include <MantleAPI/Common/vector.h>

namespace road_logic_suite
{

RoadLogicSuite::RoadLogicSuite() : impl_(std::make_unique<RoadLogicSuiteImpl>()) {}

RoadLogicSuite::RoadLogicSuite(MapConverterConfig& converter_config)
    : impl_(std::make_unique<RoadLogicSuiteImpl>()), converter_config_(converter_config)
{
}

RoadLogicSuite::~RoadLogicSuite() = default;

std::unique_ptr<map_api::Map> RoadLogicSuite::LoadMap(const std::string& map_file) const
{
    if (!impl_->LoadFile(map_file))
    {
        throw std::runtime_error("RoadLogicSuite load map file " + map_file + " failed.");
    }

    map_conversion::MantleMapConverter converter(converter_config_);
    return std::make_unique<map_api::Map>(std::move(impl_->ConvertMap(converter)));
}

std::unique_ptr<map_validation::ValidationRunner> RoadLogicSuite::GetMapValidator() const
{
    return std::make_unique<map_validation::ValidationRunner>(std::move(impl_->GetMapValidator()));
}

std::optional<mantle_api::OpenDriveLanePosition> RoadLogicSuite::ConvertInertialToLaneCoordinates(
    const mantle_api::Vec3<units::length::meter_t>& query) const
{

    auto result = impl_->GetCoordinateConverter().ConvertInertialToLaneCoordinates(
        mantle_api::Vec3<units::length::meter_t>{query.x, query.y, query.z});

    return result.has_value() ? std::make_optional(mantle_api::OpenDriveLanePosition{
                                    .road = result.value().road,
                                    .lane = static_cast<std::int32_t>(result.value().lane),
                                    .s_offset = result.value().s_offset,
                                    .t_offset = result.value().t_offset})
                              : std::nullopt;
}

std::optional<mantle_api::Vec3<units::length::meter_t>> RoadLogicSuite::ConvertLaneToInertialCoordinates(
    const mantle_api::OpenDriveLanePosition& query) const
{
    mantle_api::OpenDriveLanePosition query_lane{.road = std::string(query.road),
                                                 .lane = query.lane,
                                                 .s_offset = query.s_offset,  //
                                                 .t_offset = query.t_offset};

    auto result = impl_->GetCoordinateConverter().ConvertLaneToInertialCoordinates(query_lane);

    return result.has_value() ? std::make_optional(mantle_api::Vec3<units::length::meter_t>{
                                    result.value().x, result.value().y, result.value().z})
                              : std::nullopt;
}

std::optional<mantle_api::Vec3<units::length::meter_t>> RoadLogicSuite::ConvertRoadPositionToInertialCoordinates(
    const mantle_api::OpenDriveRoadPosition& query) const
{
    return impl_->GetCoordinateConverter().ConvertRoadPositionToInertialCoordinates(query);
}

std::optional<units::angle::radian_t> RoadLogicSuite::GetRoadPositionHeading(const std::string& road_id,
                                                                             const units::length::meter_t& s) const
{
    return impl_->GetCoordinateConverter().GetRoadPositionHeading(road_id, s);
}

}  // namespace road_logic_suite
