# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

git_tag=$(git tag --points-at HEAD)

MYDIR="$(dirname "$(readlink -f $0)")"
REPODIR=$(realpath "${MYDIR}/..")

CONTAINER_NAME="rls_release_build"

CONTAINER_FOLDER_PATH="/mnt/repo"

DOCKER_IMAGE="rlstester1/road-logic-suite-dev:latest"

docker pull "$DOCKER_IMAGE"
docker run -d -i -t --name "$CONTAINER_NAME" -v "$REPODIR":"$CONTAINER_FOLDER_PATH" "$DOCKER_IMAGE" /bin/bash

docker exec -it -w "$CONTAINER_FOLDER_PATH" "$CONTAINER_NAME" bazel build --config=rls_release //Packaging/...
docker exec -it -w "$CONTAINER_FOLDER_PATH" "$CONTAINER_NAME" mkdir -p "artifacts"
docker exec -it -w "$CONTAINER_FOLDER_PATH" "$CONTAINER_NAME" cp -r bazel-bin/Packaging/tar-road-logic-suite-lib.tar.gz "${CONTAINER_FOLDER_PATH}/artifacts/road-logic-suite-${git_tag}-linux-x86_64.tar.gz"

docker stop "$CONTAINER_NAME"
docker remove "$CONTAINER_NAME"
