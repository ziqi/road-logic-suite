# *******************************************************************************
# Copyright (C) 2023-2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -e

echo "Run Small Checks ..."
MYDIR="$(dirname "$(readlink -f $0)")"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

"${MYDIR}"/check_and_fix/check_and_fix.sh 1
