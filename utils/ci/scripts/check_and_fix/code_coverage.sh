# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u
source "$(dirname "${BASH_SOURCE[0]}")/../helper_functions/bash_helper.sh"

genhtml_location=$(which genhtml)

OUTDIR=bazel-coverage

function main()
{
    pre_run

    code_coverage

}

function pre_run()
{
    if [[ -z ${genhtml_location} ]]; then
        log::warn "genhtml was not found on your system, can not check code-covrage"
        exit 1
    fi
}

function code_coverage()
{

    log::run "Running bazel code coverage for RoadLogicSuite"

    bazel coverage --config=rls --local_test_jobs=1 --combined_report=lcov --instrumentation_filter="-//RoadLogicSuite/Tests[:/],-//tools[:/]" \
        --nocache_test_results --noshow_progress --ui_event_filters=-info,-stderr \
        //RoadLogicSuite/...

    genhtml --show-details --legend --title "Coverage Report for AnsysRoadLogicSuite" --output-directory $OUTDIR bazel-out/_coverage/_coverage_report.dat

    ls -l $OUTDIR/index.html
    echo "Code coverage html report is saved at: $OUTDIR/index.html"

}

main
