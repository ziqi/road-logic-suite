# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u
if [[ -z "${CURRENT_DIRECTORY+x}" ]]; then
    readonly CURRENT_DIRECTORY=$(readlink -f "$(dirname "$0")")
fi
source "$(dirname "${BASH_SOURCE[0]}")/../helper_functions/bash_helper.sh"

perform_dry_run="${1-0}"
overall_errors=0
title="Repo Check and Fix"

function main()
{
    pre_run

    check_and_fix

    post_run

    return ${overall_errors}

}

function pre_run()
{
    if [[ ! -e utils/ci/scripts/check_and_fix/check_and_fix.sh ]]; then
        echo "This tool must be run from the repo directory." >&2
        exit 1
    fi
}

function check_and_fix()
{
    "${CURRENT_DIRECTORY}"/internal/check_clang_format.sh ${perform_dry_run}
    overall_errors=$((overall_errors + $?))

    "${CURRENT_DIRECTORY}"/internal/check_bazel_buildifier.sh ${perform_dry_run}
    overall_errors=$((overall_errors + $?))

    "${CURRENT_DIRECTORY}"/internal/check_shell_formatting.sh ${perform_dry_run}
    overall_errors=$((overall_errors + $?))

    # todo: current check copyright does not work. To be fixed
    # "${CURRENT_DIRECTORY}"/internal/check_copyrights.sh ${perform_dry_run}
    # overall_errors=$((overall_errors + $?))

}

function post_run()
{
    echo
    echo "====================================================="
    log::summary "${title}" "${overall_errors}" "${perform_dry_run}"
    echo "====================================================="
}

main "${@}"
