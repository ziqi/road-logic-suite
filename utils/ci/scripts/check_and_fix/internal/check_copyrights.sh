# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u

source "$(dirname "${BASH_SOURCE[0]}")/../../helper_functions/bash_helper.sh"

perform_dry_run="${1-0}"
failed=0

# Regex to parse copyright text for open source files.
# This regex consider variable year in its content (e.g. 2023).
copy_right_regex="\/\*{79}\*Copyright\(C\)([0-9]{4})(-[0-9]{4})?, ANSYS, Inc.
 \*
 \* This program and the accompanying materials are made
 \* available under the terms of the Eclipse Public License 2.0
 \* which is available at https://www.eclipse.org/legal/epl-2.0/
 \*
 \* SPDX-License-Identifier: EPL-2.0
 \*{79}\/"
copy_right_regex="$(echo -e "${copy_right_regex}" | tr -d '[:space:]')"

function main()
{
    title="Check copyrights"

    log::run "${title}"

    check_copyrights

    log::summary "${title}" "${failed}" "${perform_dry_run}"
    return "${failed}"
}

function check_copyrights()
{
    checked_filed_cnt=0

    check_copyright_format_on_repo

}

function check_copyright_format_on_repo()
{
    local SOURCE_DIR="RoadLogicSuite/"
    local file_extensions='.*\(\.c\|\.C\|\.cc\|\.cpp\|\.cxx\|\.h\|\.H\|\.hh\|\.hpp\|\.hxx\|\.cl\)$'

    result=$({
        find "${SOURCE_DIR}" -regex "${file_extensions}"
    } 2>&1)

    readarray -t matched_files <<<"${result}"
    for matched_file in "${matched_files[@]}"; do
        check_copyright_format_on_file "${matched_file}"
        ((failed = failed + $?))
    done
}

function check_copyright_format_on_file()
{
    ((checked_filed_cnt = checked_filed_cnt + 1))

    local parsed=$(head -9 "$1") # parsed copy right text
    parsed="$(echo -e "${parsed}" | tr -d '[:space:]')"

    if [[ "$parsed" =~ $copy_right_regex ]]; then
        return 0
    else
        log::sub_failure "Copyright line is invalid in $1: Expected '${copy_right_regex}' but got '${parsed}'"
    fi

    return 1
}

main "${@}"
