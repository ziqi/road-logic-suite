# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

echo "Prepare ..."

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "${BASEDIR}/.cache")

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Wipe old files if exist
rm -rf "${CACHEDIR}" "${BASEDIR}/artifacts"

# Debug prints
echo "Path of MYDIR: ${MYDIR}"
echo "Path of BASEDIR: ${BASEDIR}"
echo "Path of CACHEDIR: ${CACHEDIR}"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

echo "Environment variables:"
env

echo "User name:"
whoami

git --version
bazel --version
clang-tidy --version
buildifier --version

whereis git
whereis bazel
whereis clang-tidy
whereis buildifier

echo "Checkout git submodules recursively ..."
git submodule update --init --recursive
