# *******************************************************************************
# Copyright (C) 2023, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -u

workspace="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." >/dev/null 2>&1 && pwd)"
log_dir=${workspace}

gtest_xml_log_dir=${workspace}/gtest-output
build_config=""
bazel_result=0
bazel_log_file="bazel_output.log"
bazel_test_results="test_results.log"
bazel_test_results_page="test_results.html"

bazel_logging="2>> ${bazel_log_file} 1>> ${bazel_test_results}"

overall_test_result_xml=${bazel_test_results_page%.html}.summary

function main()
{
    echo "Clean-up test result"

    cleanup_gtest_xml

    echo "Generate test html summary"

    bazel_test

    echo "Generate test html summary"
    generate_test_html_summary
}

#
# Bazel test with the current set parameters
#
function bazel_test()
{
    cd "${workspace}"

    echo ""
    echo "Writing test results to ${log_dir}/${bazel_test_results}"
    echo "Test results for --config=${build_config}" >>"${log_dir}"/"${bazel_test_results}"

    echo "Writing test logs (non cached only) to ${gtest_xml_log_dir}"

    bazel_cmd="bazel test --config=rls //RoadLogicSuite/...  ${bazel_logging}"
    echo ""
    echo "Running bazel command: ${bazel_cmd}"
    echo ""
    eval "${bazel_cmd}"
    bazel_result=$(("$bazel_result" + $?))
}

#
# Cleanup gtest xml folder
#
function cleanup_gtest_xml()
{
    mkdir -pv "${gtest_xml_log_dir}"
    rm -vf "${gtest_xml_log_dir}"/*.xml
    rm -vf "${gtest_xml_log_dir}"/*.log
    rm -vf "${gtest_xml_log_dir}"/${overall_test_result_xml}
}

#
# Copies gtest results out of the bazel sandbox
#
function gather_gtest_results()
{

    bazel_test_logs_root=$(bazel info --config=rls bazel-testlogs)

    local bazel_testlogs_dir="${bazel_test_logs_root}/RoadLogicSuite/"

    echo "Testlogsdir: ${bazel_testlogs_dir}"
    # Change to sandbox
    if [ -d "${bazel_testlogs_dir}" ]; then
        cd "${bazel_testlogs_dir}"
        test_results=$(find . -name "test.xml")

        for xml_result in ${test_results}; do
            local parent_dir_name=$(dirname "${xml_result}")
            parent_dir_name=${parent_dir_name:2}     # remove first two character
            parent_dir_name=${parent_dir_name//\//_} # replace all / with _ to garantee uniqueness

            local source_gtest_xml=${xml_result}
            local source_gtest_log="${xml_result::-4}.log"
            local target_gtest_xml="${gtest_xml_log_dir}/${parent_dir_name}.xml"
            local target_gtest_html="${gtest_xml_log_dir}/${parent_dir_name}.html"

            echo "Copy gtest results of ${parent_dir_name}"
            cp "${source_gtest_xml}" "${target_gtest_xml}"
            # open up logfile and create an html side out of it
            echo "${source_gtest_log}" | aha >"${target_gtest_html}"

            local search_string="<testsuites"
            local replace_string="<testsuites\ testlog=\"gtest-output\/$(basename "${target_gtest_html}")\""

            sed -i "s/${search_string}/${replace_string}/g" "${target_gtest_xml}"

        done
    else
        error "Could not find bazel-testlogs. Looks like bazel test was a failure."
    fi
}

#
# Generate html summary out of the gtest results
#
function generate_test_html_summary()
{
    gather_gtest_results

    # Html page generation
    if [ -d "${gtest_xml_log_dir}" ]; then
        echo "Generating test results html page..."
        echo '<?xml version="1.0" encoding="UTF-8"?>' >"${gtest_xml_log_dir}"/${overall_test_result_xml}
        echo '<main>' >>"${gtest_xml_log_dir}"/${overall_test_result_xml}
        for f in "${gtest_xml_log_dir}"/*.xml; do
            sed 1d "${f}" >>"${gtest_xml_log_dir}"/${overall_test_result_xml}
        done
        echo '</main>' >>"${gtest_xml_log_dir}"/${overall_test_result_xml}
        xsltproc "${workspace}"/utils/ci/scripts/utility/gtest2html.xslt "${gtest_xml_log_dir}"/${overall_test_result_xml} >"${log_dir}"/"${bazel_test_results_page}"
        bazel_result=$((bazel_result + $?))
    else
        # Tests were probably cached
        echo "No xml logs have been generated. Skipping html page generation..."
        echo "This is not the log you are looking for." >"${log_dir}"/"${bazel_test_results_page}"
        echo "" >>"${log_dir}"/"${bazel_test_results_page}"
        echo "No GTest xml log has been generated. Nothing to display." >>"${log_dir}"/"${bazel_test_results_page}"
    fi
    echo ""
}

main "$@"

exit 0
