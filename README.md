# RoadLogicSuite

<!-- TOC -->
- [RoadLogicSuite](#roadlogicsuite)
  - [Overview](#overview)
  - [Development Environment Setup](#development-environment-setup)
  - [Using RoadLogicSuite](#using-roadlogicsuite)
  - [Loading Maps](#loading-maps)
  - [Validating Maps](#validating-maps)
    - [Adding new logical data types for Validation](#adding-new-logical-data-types-for-validation)
    - [Adding new Validators](#adding-new-validators)
  - [Converting Coordinates](#converting-coordinates)
    - [Inertial Coordinates \<-\> Road Position Coordinates](#inertial-coordinates---road-position-coordinates)
    - [Inertial Coordinates \<-\> Lane Coordinates](#inertial-coordinates---lane-coordinates)
    - [Converting to IMAP](#converting-to-imap)
  - [Feature Matrix](#feature-matrix)
    - [Additional Features](#additional-features)
    - [Coordinate Conversion](#coordinate-conversion)
<!-- TOC -->

## Overview

The RoadLogicSuite is a C++ library designed to parse OpenDrive files, supporting version 1.8 and backward compatibility.

## Development Environment Setup

See [Development Environment Setup](./Documentation/development_environment_setup.md)

## Using RoadLogicSuite

![External Interface Overview](./Documentation/img/RoadLogicSuite_ExternalInterface.png)

The `RoadLogicSuite` is the main entry point into the `RoadLogicSuite`. All functionality can be reached using this
class.

## Loading Maps

```c++
RoadLogicSuite suite{};
suite.LoadFile("path/to/my/map.xodr");
```

Loading files work with both absolute paths and relative paths from the current working directory.

![LoadMap_SequenceDiagram](./Documentation/img/RoadLogicSuite_LoadMap.png)

## Validating Maps

---

⚠️ The map validation runner is implemented, but only one example validation exists.

---

So far, there does not seem to be a use case for different error severities or a sophisticated error report, i.e. as an
HTML file, so validating the maps is being kept as simple as possible.

```c++
ValidationRunner runner = suite.GetMapValidator();
std::vector<MapError> errors = runner.Validate();
if (!errors.empty()) {
    // some errors happened!
}
```

In the first iteration, the `MapError` will only consist of the error message itself, however it can always be extended
with additional information at a later point in time.

![ValidateMap_SequenceDiagram](./Documentation/img/RoadLogicSuite_ValidateMap.png)

### Adding new logical data types for Validation

The validator interface consists of a templated, pure virtual class. `IValidator<TObject>`, where `TObject` is a logical
data type under test. This can be anything from a `Road` to a `Geometry` or even advanced structures like `Junctions`.
In order to implement a new logical data type, you have to do the following steps:

1. Add a new method into the `AbstractValidatorConfig` class in accordance to the ones, that already exist. The
   signature looks approximately like
   this: `std::vector<std::unique_ptr<validators::IValidator<TObject>>> Get{TObject}Validators` where `TObject` is
   substituted with the appropiate data type. The return value of the base class should be empty to ensure compatibility
   with tests.
2. In `ValidationRunner`, make sure to fetch all validators, then fetch all `TObject` and then run each validator for
   each object.

Check out the `RoadValidators` already implemented as an example.

### Adding new Validators

Creating new validators is straight-forward:

1. Create a new class that inherits from `IValidator` with the appropriate template parameter.
2. In the `DefaultValidationConfig`, add it to the correct method that returns values for your object.

Check out the `RoadLengthValidator`, which serves as an example.

## Converting Coordinates

---

⚠️ Converting coordinates is only partially implemented, yet. Please check the [Feature Matrix](#feature-matrix) for
more information.

---

The current coordinate systems are defined as follows:

* Inertial Coordinate System: A cartesian coordinate system in three dimensions (x,y,z). Usually, this refers to a world
  coordinate system.
* Road Coordinate System: A coordinate system that contains of the two dimensions `s` and `t`. `s` runs along
  the reference line of a road, where the minimum value is `s0` and the maximum value is `s0+length`. `t` lies at a
  right angle towards the current direction of `s`. On the left-hand side it is defined as positive.
* Lane Coordinate System: This coordinate system is not part of the OpenDRIVE standard. However, it is very convenient
  for other applications to have. It defines the position relative to the lane of a road. `s` is defined just like for
  the road Coordinate System of a road, while `t` is defined as the offset to the center of the lane.

For more information about the math used for converting between different coordinate systems, please check out
[this document](./Documentation/Coordinate_Conversion_Math.md).

![CoordinateConversion_SequenceDiagram](./Documentation/img/RoadLogicSuite_CoordinateConversion.png)

### Inertial Coordinates <-> Road Position Coordinates

In the current implementation, only points on the map that can be described as a valid road position are allowed.
This means, that for a position on the map to be valid, there needs to be a reference line geometry which can describe
the same point with valid `s` and `t` values, where `s` must be between `s0` and `s0+length` of the geometry.

```c++
// from inertial to road position
Vec3<meter_t> inertial_coordinates(500_m, 240_m, 0_m);
OpenDriveRoadPosition road_position = suite.ConvertInertialToRoadCoordinates(inertial_coordinates);

// from road position to inertial
OpenDriveRoadPosition road_position{.road = "34", .s_offset = 50_m, .t_offset = 0_m};
Vec3<meter_t> inertial_coordinates = suite.ConvertRoadToInertialCoordinates(road_position);
```

### Inertial Coordinates <-> Lane Coordinates

In the current implementation, only points on the map that can be described as a valid lane position are allowed.
This means, that for a position to be valid, there needs to be a lane geometry which can describe the same point with
valid `s` and `t` values, where `s` must be between `s0` and `s0+length` of the geometry.

```c++
// from inertial to lane
Vec3<meter_t> inertial_coordinates(500_m, 240_m, 0_m);
OpenDriveLanePosition lane_position = suite.ConvertInertialToLaneCoordinates(inertial_coordinates);

// from lane to inertial
OpenDriveLanePosition lane_position{.road = "34", .lane = -2 .s_offset = 50_m, .t_offset = 0_m};
Vec3<meter_t> inertial_coordinates = suite.ConvertLaneToInertialCoordinates(lane_position);

```
### Converting to IMAP
⚠️ The IMAP converter is only partially implemented, yet. Please check the [Feature Matrix](#feature-matrix) for more
information.
```
RoadLogicSuite suite{};
suite.LoadFile("Path/To/file.xodr");
// You can optionally provide a configuration struct.
auto map = suite.ConvertMap();
```

![IMapConversion_SequenceDiagram](./Documentation/img/RoadLogicSuite_IMapConversion.png)

## Feature Matrix

This feature matrix gives a quick overview about which features have already been implemented in the RoadLogicSuite.

🔴: Not implemented

🟡: Partially implemented (see comment)

🟢: Fully implemented

```
Feature
│
├── Reference Line Geometries
│   ├── Line Geometry ········································ 🟢
│   ├── Arc Geometry ········································· 🟢   
│   ├── Spiral Geometry ······································ 🟢
│   └── Parametric Cubic Curve ······························· 🟢 
│   
├── Roads
│   ├── Attributes ··········································· 🟢
│   ├── Road Linkage ········································· 🟢
│   │   ├── Predecessor ······································ 🟢
│   │   └── Successor ········································ 🟢
│   ├── Road Type ············································ 🔴
│   │   └── Speed Limits ····································· 🔴
│   ├── Elevation Profile ···································· 🟢
│   │   └── Elevation ········································ 🟢
│   ├── Lateral Profile ······································ 🟡
│   │   ├── Superelevation ··································· 🟢
│   │   ├── Shape ············································ 🔴
│   │   └── Cross Section Surface ···························· 🔴
│   └── Road CRG Surface ····································· 🔴
│   
├── Lanes
│   ├── Lane Section ········································· 🟢
│   │   ├── Lane Groups ······································ 🟢
│   │   └── Lane ············································· 🟢
│   │       ├── Lane Linkage ································· 🟢
│   │       │   ├── Predecessor ······························ 🟢
│   │       │   └── Successor ································ 🟢
│   │       ├── Lane Geometry ································ 🟢
│   │       │   ├── Width ···································· 🟢
│   │       │   ├── Border ··································· 🟢
│   │       │   └── Height ··································· 🟢
│   │       ├── Type ········································· 🟡 (Todo: need some updates for OpenDrive v1.8.0)
│   │       ├── Material ····································· 🔴
│   │       ├── Speed Limits ································· 🔴
│   │       ├── Lane Access ·································· 🔴
│   │       └── Road Markings ································ 🟡
│   │           ├── Attributes ······························· 🟢
│   │           ├── Road Markings Lines ······················ 🔴
│   │           ├── Types ···································· 🔴
│   │           ├── Irregular Types ·························· 🔴
│   │           └── Offsets ·································· 🔴
│   ├── Lane Offset ·········································· 🟢
│   └── Specific Lane Rules ·································· 🔴
│
├── Junctions
│   ├── Junctions Types ······································ 🟡
│   │   ├── Common Junctions ································· 🟢
│   │   ├── Crossings ········································ 🟢
│   │   ├── Direct Junctions ································· 🔴
│   │   └── Virtual Junctions ································ 🔴
│   ├── Junction Connection ·································· 🟢
│   │   ├── Incoming Roads ··································· 🟢
│   │   ├── Connecting Roads ································· 🟢
│   │   └── Lane Links ······································· 🟢
│   ├── Cross Paths ·········································· 🟢
│   │   ├── Start Lane Link ·································· 🟢
│   │   └── End Lane Link ···································· 🟢
│   ├── Junction Reference Line ······························ 🔴
│   ├── Junction Boundary ···································· 🔴
│   ├── Junction Elevation ··································· 🔴
│   ├── Junction Surface ····································· 🔴
│   └── Junction Groups ······································ 🔴
│
├── Objects
│   ├── Object Attributes ···································· 🟡 (Missing: dynamic, orientation, perpToRoad, validLength)
│   ├── Repeating Objects ···································· 🟢
│   ├── Object Outline ······································· 🔴
│   │   ├── Corner Road ······································ 🔴
│   │   └── Corner Local ····································· 🔴
│   ├── Object Skeleton ······································ 🔴
│   ├── Object Material ······································ 🔴
│   ├── Lane Validity ········································ 🟢
│   ├── Access Rules to Parking Spaces ······················· 🔴
│   ├── Object Marking ······································· 🔴
│   ├── Object Borders ······································· 🔴
│   ├── Object Reference ····································· 🔴
│   ├── Tunnels ·············································· 🔴
│   ├── Bridges ·············································· 🔴
│   └── Object CRG Surface ··································· 🔴
│
└── Signals
    ├── Signal Types ········································· 🟡
    │   ├── Traffic Signs ···································· 🟢
    │   ├── Road Markings ···································· 🟢
    │   └── Traffic Lights ··································· 🔴
    ├── Signal Attributes ···································· 🟢
    ├── Lane Validity ········································ 🟢
    ├── Signal Dependency ···································· 🟢
    ├── Signal Reference ····································· 🔴
    ├── Signals that Apply to Multiple Roads ················· 🔴
    ├── Signal Controllers ··································· 🔴
    ├── Signal Boards ········································ 🔴
    └── Signal semantics ····································· 🔴

``` 

### Additional Features

This matrix is for additional features that are not part of any standard but convenient or useful.

| Additional Feature      | Status | Comments                                                                                                                     |
|-------------------------|:------:|------------------------------------------------------------------------------------------------------------------------------|
| Lane Coordinate Systems |   🟢   | This coordinate system is similar to the road coordinates, but have the offset t(s) from the reference line.       |
| Multiple OpenDRIVE maps |   🔴   | Still very abstract. There need to be discussions about the assumptions, that can be made about the map, i.e. valid linkage. |
| Logical Lane            |   🟢   | A logical lane is a road component that doesn't depend on road markings. When OSI is generated from OpenDRIVE, LogicalLanes correspond directly to OpenDRIVE lanes.       |
| Logical Lane Boundary   |   🟢   | Boundary line of a LogicalLane. Similar to a LaneBoundary, but with a reference line and ST positions.       |


### Coordinate Conversion

| Geometry                | inertial -> road position/lane | road postion/lane -> inertial |
|-------------------------|:-------------------------------:|:-------------------------------:|
| Line                    |               🟢                |               🟢                |
| Arc                     |               🟢                |               🟢                |
| Spiral                  |               🟢                |               🟢                |
| Cubic Parametric Spline |               🟢                |               🟢                |

### Known Constrains

1. The s_end of road barrier(@s + @length) should less than or equal to the s length of corresponding road, that is:  
@s + @length <= road s length  
In cases where the constrains are not met, points outside the range will be discarded during conversion.  

2. The downsampling will not work if the sOffset value of an roadmark in a lane is less than the s length of the same lane.  
   The road in the following block contains a roadmark whose sOffset equals to the s length of the lane, in this case the roadmark is a point, the downsampling for this roadmark will be skipped when it is set to true.

```xml
<road name="Road" length="100" id="0" junction="-1">
    ...
    <lanes>
        <laneSection s="0.0">
            ...
            <right>
                <lane id="-1" type="driving" level="true">
                    ...
                    <roadMark sOffset="0.0" .../>
                    <roadMark sOffset="100.0" .../>
                </lane>
        </laneSection>
      </lanes>
</road>
```
